package cz.cvut.openedx;

import cz.cvut.openedx.helpers.CSVReader;
import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.objects.SiteLiteObject;
import cz.cvut.openedx.pages.EdXHP;
import cz.cvut.openedx.pages.EdXSignUp2Lite;
import cz.cvut.openedx.pages.EdXSignUp3;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SignUpLiteTest {
    static RemoteWebDriver driver;
    static EdXHP edXHP = new EdXHP(driver);

    static String BASE_URL = "https://www.edunext.co/";

    @Parameterized.Parameters
    public static Collection params() throws IOException {
        return CSVReader.readCSV("src/test/resources/signupdata-lite.csv", ",");
    }

    private SiteLiteObject so;

    public SignUpLiteTest(String type, String purpose, String userCount, String feature1, String feature2, String feature3, String timeframe, String strategy,String name, String url, String lang) throws ParseException {
        so = new SiteLiteObject(type, purpose, userCount, feature1, feature2, feature3, timeframe, strategy,name, url, lang);
    }

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);

        PageFactory.initElements(driver, edXHP);

        Dimension dimension = new Dimension(1440, 900);
        driver.manage().window().setSize(dimension);
    }

    @Test
    public void step0_pageLoaded() {
        driver.get("https://manage.edunext.co/embed/register/#site-about");

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.presenceOfElementLocated(By.className("lite")));

        assertTrue("Not throwing an error!", driver.findElement(By.className("lite")).isDisplayed());
    }

    private void step1CommonCompletion(WebElement button) {
        ClickHelper.advancedClick(driver, button);

        WebElement nextBtn = driver.findElement(By.id("finish-site-about"));

        nextBtn.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-initiative"), "class", "active"));
    }

    private void returnStep() {
        WebElement homeBtn = driver.findElement(By.id("navigation-site-about")).findElement(By.className("register-navigation__option-button"));

        homeBtn.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-about"), "class", "active"));
    }

    @Test
    public void step1_chooseTest() {
        WebElement liteBtn = driver.findElement(By.className("lite"));

        step1CommonCompletion(liteBtn);

        assertTrue("Not throwing an error!", driver.findElement(By.id("navigation-site-initiative")).getAttribute("class").contains("active")); //todo replace with sthing more specific
    }

    private void step2Input1(EdXSignUp2Lite edXSignUp2Lite, String setting) {
        edXSignUp2Lite.clickWhatTypeOfOrganizationIsBehind1DropDownListField();
        WebElement choice = driver.findElementByXPath("//span[contains(text(), \"" + setting + "\")]");
        choice.click();
    }

    private void step2Input2(EdXSignUp2Lite edXSignUp2Lite, String setting) {
        edXSignUp2Lite.clickWhatIsThePurposeOfYour1DropDownListField();
        WebElement choice = driver.findElementByXPath("//span[contains(text(), \"" + setting + "\")]");
        choice.click();
    }

    private void step2Input3(EdXSignUp2Lite edXSignUp2Lite, String setting) {
        edXSignUp2Lite.setHowManyMonthlyActiveUsersAreTextField(setting);
    }

    private void step2Input4(EdXSignUp2Lite edXSignUp2Lite, String setting) {
        edXSignUp2Lite.clickWhatIsTheTimeframeForYour1DropDownListField();
        WebElement choice = driver.findElementByXPath("//span[contains(text(), \"" + setting + "\")]");
        choice.click();
    }

    private void step2Input5(EdXSignUp2Lite edXSignUp2Lite, String setting) {
        edXSignUp2Lite.clickHowAreYouPlanningToProduce2DropDownListField();
        WebElement choice = driver.findElementByXPath("//span[contains(text(), \"" + setting + "\")]");
        choice.click();
    }

    private void step2checkbox(EdXSignUp2Lite edXSignUp2Lite, String id) {
        WebElement choice = driver.findElementByXPath("//label[@for=\"" + id + "\"]");
        choice.click();
    }

    @Test
    public void step2_fillInfo() throws IOException {
        EdXSignUp2Lite edXSignUp2Lite = new EdXSignUp2Lite(driver);
        PageFactory.initElements(driver, edXSignUp2Lite);

        edXSignUp2Lite.verifyPageLoaded();

        File signupFile = new File("src/test/resources/signupdata-lite.csv"); //todo use getClass().getResource(), but "src/test/resources//signupdata-lite.csv" and "/signupdata-lite.csv" don't work

        String[] line = CSVReader.readFirstCSVLine(signupFile.toString(), ",");

        step2Input1(edXSignUp2Lite, so.type);
        step2Input2(edXSignUp2Lite, so.purpose);
        step2Input3(edXSignUp2Lite, so.userCount);
        step2Input4(edXSignUp2Lite, so.timeframe);
        step2Input5(edXSignUp2Lite, so.strategy);
        step2checkbox(edXSignUp2Lite, so.feature1);
        step2checkbox(edXSignUp2Lite, so.feature2);
        step2checkbox(edXSignUp2Lite, so.feature3);

        edXSignUp2Lite.clickNext2Button();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-info"), "class", "active"));

        assertTrue(driver.findElement(By.id("navigation-site-info")).getAttribute("class").contains("active"));
    }

    @Test
    public void step3_micrositeSetup() throws IOException {
        EdXSignUp3 edXSignUp3 = new EdXSignUp3(driver);
        PageFactory.initElements(driver, edXSignUp3);

        edXSignUp3.verifyPageLoaded();

        edXSignUp3.setInitiativeNameTextField(so.name);
        edXSignUp3.setUrlForYourOpenEdxSiteTextField(so.url);
        edXSignUp3.clickLanguage1DropDownListField();
        WebElement choice = driver.findElementByXPath("//span[contains(text(), \"" + so.lang + "\")]");
        choice.click();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        /* WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.className("invalid"))); */

        //ClickHelper.advancedClick(driver, driver.findElementById("finish-site-info"));

        edXSignUp3.clickNext2Button();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-account"), "class", "active"));

        assertTrue(driver.findElement(By.id("navigation-account")).getAttribute("class").contains("active"));
    }

    @Test
    public void step4_returnTest() {
        returnStep();

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.presenceOfElementLocated(By.className("lite")));

        assertTrue("Not throwing an error!", driver.findElement(By.className("lite")).isDisplayed());
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
