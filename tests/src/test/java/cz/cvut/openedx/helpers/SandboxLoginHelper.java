package cz.cvut.openedx.helpers;

import cz.cvut.openedx.pages.EdXSandboxSignIn;
import cz.cvut.openedx.pages.EdXSignIn;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SandboxLoginHelper {
    static String BASE_URL = "https://sandbox.edunext.io/";

    public static void logIn(WebDriver driver) {
        driver.get(BASE_URL + "login");
        EdXSandboxSignIn edXSignIn = new EdXSandboxSignIn(driver);

        PageFactory.initElements(driver, edXSignIn);

        edXSignIn
                .verifyPageLoaded()
                .setEmailEmailField("author@example.com")
                .setPasswordPasswordField("author");

        WebDriverWait wait1 = new WebDriverWait(driver, 120);
        wait1.until(ExpectedConditions.elementToBeClickable(By.id("submit")));

        edXSignIn.clickLogIntoMySandboxAccountButton();

        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.titleContains("Dashboard"));
    }
}
