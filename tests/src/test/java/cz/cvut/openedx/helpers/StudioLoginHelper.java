package cz.cvut.openedx.helpers;

import cz.cvut.openedx.pages.EdXSignIn;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudioLoginHelper {
    static String BASE_URL = "https://studio.edunext.co/";

    public static void logIn(WebDriver driver) {
        driver.get(BASE_URL + "signin");
        EdXSignIn edXSignIn = new EdXSignIn(driver);

        PageFactory.initElements(driver, edXSignIn);

        edXSignIn
                .verifyPageLoaded()
                .setEmailEmailField("author@example.com")
                .setPassword1CheckboxField("author");

        WebDriverWait wait1 = new WebDriverWait(driver, 120);
        wait1.until(ExpectedConditions.elementToBeClickable(By.id("submit")));

        edXSignIn.clickSignInToStudioButton();

        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("course-index-tabs")));
    }
}
