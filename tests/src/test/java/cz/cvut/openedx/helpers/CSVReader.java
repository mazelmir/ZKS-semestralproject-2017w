package cz.cvut.openedx.helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CSVReader {

    public static String[] readFirstCSVLine(String filename, String separator) throws IOException, FileNotFoundException {

        BufferedReader reader = new BufferedReader(new FileReader(filename));

        String cur = reader.readLine();
        String[] line = cur.split(separator);

        reader.close();
        return line;
    }

    public static ArrayList<String[]> readCSV(String filename, String separator) throws IOException, FileNotFoundException {

        ArrayList<String[]> lines = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(filename));

        String cur = reader.readLine();
        while (cur != null) {
            String[] line = cur.split(separator);
            lines.add(line);
            cur = reader.readLine();
        }

        reader.close();
        return lines;
    }
}
