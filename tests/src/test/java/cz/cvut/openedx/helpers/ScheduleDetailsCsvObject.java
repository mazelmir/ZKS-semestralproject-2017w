package cz.cvut.openedx.helpers;

public class ScheduleDetailsCsvObject {
    public enum LicenseType {
        AllRightsReserved,
        CCBY,
        CCBYNC,
        CCBYND,
        CCBYSA,
        CCBYNCND,
        CCBYNCSA
    } //todo this is not the actual thing
    public enum EntranceExam {
        NoExam,
        WithinBounds,
        OutsideBounds,
        NonInteger
    }
    public enum EnrollmentTime {
        Full,
        StartOnly,
        NoTime,
        StartAfterCourse,
        EndBeforeStart,
        InvalidTime
    }
    public enum CourseTime {
        Full,
        StartOnly,
        EndBeforeStart,
        NoStart,
        InvalidTime
    }
    public enum Pacing {
        Instructor,
        Self
    }

    public LicenseType lt;
    public EntranceExam exam;
    public EnrollmentTime enrollmentTime;
    public CourseTime courseTime;
    public Pacing pacing;

    public ScheduleDetailsCsvObject(
            LicenseType lt,
            EntranceExam exam,
            EnrollmentTime enrollmentTime,
            CourseTime courseTime,
            Pacing pacing) {
        this.lt = lt;
        this.exam = exam;
        this.enrollmentTime = enrollmentTime;
        this.courseTime = courseTime;
        this.pacing = pacing;
    }
}
