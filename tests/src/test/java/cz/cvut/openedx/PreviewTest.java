package cz.cvut.openedx;

import com.google.common.collect.Iterables;
import cz.cvut.openedx.helpers.CSVReader;
import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.helpers.SandboxLoginHelper;
import cz.cvut.openedx.helpers.StudioLoginHelper;
import cz.cvut.openedx.objects.PreviewQuestionsObject;
import cz.cvut.openedx.objects.SiteLiteObject;
import cz.cvut.openedx.pages.EdXCOutline;
import cz.cvut.openedx.pages.EdXPreviewQuestions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PreviewTest {
    static RemoteWebDriver driver;

    static String BASE_URL = "https://sandbox.edunext.io/";

    @Parameterized.Parameters
    public static Collection params() throws IOException {
        return CSVReader.readCSV("src/test/resources/previewdata.csv", ",");
    }

    private PreviewQuestionsObject previewQuestionsObject;

    public PreviewTest(String color, String furniture, String i1, String i2, String i3, String i4, String answer) {
        previewQuestionsObject = new PreviewQuestionsObject(color, furniture, i1, i2, i3, i4, answer);
    }

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);

        SandboxLoginHelper.logIn(driver);
    }

    @Test
    public void subsection_CreateTest() {
        driver.get(BASE_URL + "courses/course-v1:sandbox+sandbox+02/courseware/interactive_demonstrations/basic_questions/");

        EdXPreviewQuestions edXPreview = new EdXPreviewQuestions(driver);
        PageFactory.initElements(driver, edXPreview);

        edXPreview.verifyPageLoaded();

        edXPreview.setAWindow1CheckboxField(previewQuestionsObject.color);

        switch (previewQuestionsObject.furniture) {
            case "table":
                edXPreview.setATableRadioButtonField();
                break;
            case "desk":
                edXPreview.setADeskRadioButtonField();
                break;
            case "bookshelf":
                edXPreview.setABookshelfRadioButtonField();
                break;
            case "chair":
                edXPreview.setAChairCorrectRadioButtonField();
        }

        if (previewQuestionsObject.i1.equals("1"))
            edXPreview.setAPianoCheckboxField();
        else
            edXPreview.unsetAPianoCheckboxField();

        if (previewQuestionsObject.i2.equals("1"))
            edXPreview.setATreeCheckboxField();
        else
            edXPreview.unsetATreeCheckboxField();

        if (previewQuestionsObject.i3.equals("1"))
            edXPreview.setAGuitarCheckboxField();
        else
            edXPreview.unsetAGuitarCheckboxField();

        if (previewQuestionsObject.i4.equals("1"))
            edXPreview.setAWindow2CheckboxField();
        else
            edXPreview.unsetAWindow2CheckboxField();

        edXPreview.clickSubmitsubmitYourAnswerButton();

        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("notification-submit")));

        WebElement notificationMessage = driver.findElementByXPath("//div[contains(@class, 'notification-submit')]//span[contains(@class, 'notification-message')]");

        assertTrue("Not throwing an error!", notificationMessage.getText().contains(previewQuestionsObject.answer + "/3"));
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
