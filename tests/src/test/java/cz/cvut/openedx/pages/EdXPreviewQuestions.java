package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;

import cz.cvut.openedx.helpers.ClickHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXPreviewQuestions {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(name = "input_a0effb954cca4759994f1ac9e9434bf4_3_1")
    @CacheLookup
    private List<WebElement> aBookshelf;

    private final String aBookshelfValue = "choice_3";

    @FindBy(name = "input_a0effb954cca4759994f1ac9e9434bf4_3_1")
    @CacheLookup
    private List<WebElement> aChairCorrect;

    private final String aChairCorrectValue = "choice_2";

    @FindBy(name = "input_a0effb954cca4759994f1ac9e9434bf4_3_1")
    @CacheLookup
    private List<WebElement> aDesk;

    private final String aDeskValue = "choice_1";

    @FindBy(id = "input_a0effb954cca4759994f1ac9e9434bf4_4_1_choice_2")
    @CacheLookup
    private WebElement aGuitar;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/fe04a5c503d14561a0fbf9178f31ea3a/9a18898895f546379beae596d5ae3905/']")
    @CacheLookup
    private WebElement aLookAhead;

    @FindBy(id = "input_a0effb954cca4759994f1ac9e9434bf4_4_1_choice_0")
    @CacheLookup
    private WebElement aPiano;

    @FindBy(name = "input_a0effb954cca4759994f1ac9e9434bf4_3_1")
    @CacheLookup
    private List<WebElement> aTable;

    private final String aTableValue = "choice_0";

    @FindBy(id = "input_a0effb954cca4759994f1ac9e9434bf4_4_1_choice_1")
    @CacheLookup
    private WebElement aTree;

    @FindBy(id = "input_a0effb954cca4759994f1ac9e9434bf4_2_1")
    @CacheLookup
    private WebElement aWindow1;

    @FindBy(id = "input_a0effb954cca4759994f1ac9e9434bf4_4_1_choice_3")
    @CacheLookup
    private WebElement aWindow2;

    @FindBy(id = "about-exams-and-certificates-parent")
    @CacheLookup
    private WebElement aboutExamsAndCertificates;

    @FindBy(css = "a[href='/account/settings']")
    @CacheLookup
    private WebElement accountSettings;

    @FindBy(name = "submit")
    @CacheLookup
    private WebElement addComment1;

    @FindBy(name = "submit")
    @CacheLookup
    private WebElement addComment2;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/interactive_demonstrations/814f6d938861427ba8d18de503089a2a/']")
    @CacheLookup
    private WebElement analyseDeSituationsDaffaireExam;

    @FindBy(css = "button.bookmarks-list-button.is-inactive")
    @CacheLookup
    private WebElement bookmarks;

    @FindBy(css = "button.dismiss.cancel-button")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "button.btn.btn-link.bookmark-button")
    @CacheLookup
    private WebElement clickToAddBookmarkThis;

    @FindBy(css = "button.close-modal")
    @CacheLookup
    private WebElement close;

    @FindBy(id = "a0effb954cca4759994f1ac9e9434bf4_xqa_entry")
    @CacheLookup
    private WebElement comment1;

    @FindBy(id = "67c26b1e826e47aaa29757f62bcd1ad0_xqa_entry")
    @CacheLookup
    private WebElement comment2;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware']")
    @CacheLookup
    private WebElement courseCurrentLocation;

    @FindBy(id = "course-search-input")
    @CacheLookup
    private WebElement courseSearch;

    @FindBy(css = "a[href='/courses']")
    @CacheLookup
    private WebElement courses;

    @FindBy(css = "a[href='https://creativecommons.org/licenses/by-nc-nd/4.0/']")
    @CacheLookup
    private WebElement creativeCommonsLicensedContentWith;

    @FindBy(css = "a.username")
    @CacheLookup
    private WebElement dashboardForSandboxauthor;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/edx_introduction/']")
    @CacheLookup
    private WebElement demoCourseOverview;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/discussion/forum/']")
    @CacheLookup
    private WebElement discussion;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/1414ffd5143b4b508f739b563ab468b7/workflow/']")
    @CacheLookup
    private WebElement edxExamsExamThisContent;

    @FindBy(id = "example-week-2-get-interactive-parent")
    @CacheLookup
    private WebElement exampleWeek2GetInteractive;

    @FindBy(id = "example-week-3-be-social-parent")
    @CacheLookup
    private WebElement exampleWeek3BeSocial;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/ad7c0eed954d48e9b491bcdcc0718610/']")
    @CacheLookup
    private WebElement faq;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/fe04a5c503d14561a0fbf9178f31ea3a/b13e355c9acf4290a8b6b092cd3ed41d/']")
    @CacheLookup
    private WebElement formATeam;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/graded_interactions/c97584b2f8b74c799b99426ccf1bb2b6/']")
    @CacheLookup
    private WebElement googleDocs;

    @FindBy(css = "button.utility-control.utility-control-button.action-toggle-notes.is-active")
    @CacheLookup
    private WebElement hideNotes;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/info']")
    @CacheLookup
    private WebElement home;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/graded_interactions/175e76c4951144a29d46211361266e0e/']")
    @CacheLookup
    private WebElement homeworkEssays;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/social_integration/dbe8fc027bcb4fe9afb744d2e8415855/']")
    @CacheLookup
    private WebElement homeworkFindYourStudy;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/graded_interactions/graded_simulations/']")
    @CacheLookup
    private WebElement homeworkLabsAndDemos;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/interactive_demonstrations/basic_questions/']")
    @CacheLookup
    private WebElement homeworkQuestionStylesCurrent;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/instructor']")
    @CacheLookup
    private WebElement instructor;

    @FindBy(id = "introduction-parent")
    @CacheLookup
    private WebElement introduction;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/graded_interactions/simulations/']")
    @CacheLookup
    private WebElement lesson2LetsGet;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/social_integration/48ecb924d7fe4b66a230137626bfa93e/']")
    @CacheLookup
    private WebElement lesson3BeSocial;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/interactive_demonstrations/19a30717eff543078a5d94ae9d6c18a5/']")
    @CacheLookup
    private WebElement managementDesOprations;

    @FindBy(css = "a.dropdown")
    @CacheLookup
    private WebElement moreOptionsDropdown;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/social_integration/6ab9c442501d472c8ed200e367b4edfa/']")
    @CacheLookup
    private WebElement moreWaysToConnect;

    @FindBy(css = "a[href='/u/sandbox-author']")
    @CacheLookup
    private WebElement myProfile;

    @FindBy(css = "#sequence_basic_questions div:nth-of-type(1) button:nth-of-type(2)")
    @CacheLookup
    private WebElement next1;

    @FindBy(css = "#sequence_basic_questions nav.sequence-bottom button:nth-of-type(2)")
    @CacheLookup
    private WebElement next2;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/edxnotes/']")
    @CacheLookup
    private WebElement notes;

    @FindBy(css = "button.dismiss.ok-button")
    @CacheLookup
    private WebElement ok;

    @FindBy(id = "confirm_open_button")
    @CacheLookup
    private WebElement open;

    private final String pageLoadedText = "While the multiple choice question types below are somewhat standard, explore the other question types in the sequence above, like the formula builder- try them all out";

    private final String pageUrl = "/courses/course-v1:sandbox+sandbox+02/courseware/interactive_demonstrations/basic_questions/";

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/90e7ce6c141946069323a6dddb935b5e/']")
    @CacheLookup
    private WebElement part1Example;

    @FindBy(css = "#sequence_basic_questions div:nth-of-type(1) button:nth-of-type(1)")
    @CacheLookup
    private WebElement previous1;

    @FindBy(css = "#sequence_basic_questions nav.sequence-bottom button:nth-of-type(1)")
    @CacheLookup
    private WebElement previous2;

    @FindBy(id = "tab_3")
    @CacheLookup
    private WebElement problemChemicalEquations;

    @FindBy(id = "tab_1")
    @CacheLookup
    private WebElement problemDragAndDrop;

    @FindBy(id = "tab_6")
    @CacheLookup
    private WebElement problemInstructorProgrammedResponses;

    @FindBy(id = "tab_2")
    @CacheLookup
    private WebElement problemMathematicalExpressions;

    @FindBy(id = "tab_0")
    @CacheLookup
    private WebElement problemMultipleChoiceQuestions;

    @FindBy(id = "tab_4")
    @CacheLookup
    private WebElement problemNumericalInput;

    @FindBy(id = "tab_5")
    @CacheLookup
    private WebElement problemTextInput;

    @FindBy(css = "button.btn-link.staff-debug-reset")
    @CacheLookup
    private WebElement resetLearnersAttemptsToZero;

    @FindBy(css = "#problem_a0effb954cca4759994f1ac9e9434bf4 div:nth-of-type(2) div:nth-of-type(3) div.notification-btn-wrapper button.btn.btn-default.btn-small.notification-btn.review-btn.sr")
    @CacheLookup
    private WebElement review1;

    @FindBy(css = "#problem_a0effb954cca4759994f1ac9e9434bf4 div:nth-of-type(2) div:nth-of-type(4) div.notification-btn-wrapper button.btn.btn-default.btn-small.notification-btn.review-btn.sr")
    @CacheLookup
    private WebElement review2;

    @FindBy(css = "button.search-button")
    @CacheLookup
    private WebElement search;

    @FindBy(id = "section-parent")
    @CacheLookup
    private WebElement section1;

    @FindBy(id = "section-parent")
    @CacheLookup
    private WebElement section2;

    @FindBy(id = "section-parent")
    @CacheLookup
    private WebElement section3;

    @FindBy(id = "section-parent")
    @CacheLookup
    private WebElement section4;

    @FindBy(id = "section-parent")
    @CacheLookup
    private WebElement section5;

    @FindBy(id = "section-parent")
    @CacheLookup
    private WebElement section6;

    @FindBy(id = "session-1-getting-started-parent")
    @CacheLookup
    private WebElement session1GettingStarted;

    @FindBy(name = "submit")
    @CacheLookup
    private WebElement setPreviewMode;

    @FindBy(css = "button.discussion-show.btn")
    @CacheLookup
    private WebElement showDiscussion;

    @FindBy(css = "a[href='/logout']")
    @CacheLookup
    private WebElement signOut;

    @FindBy(css = "a.nav-skip")
    @CacheLookup
    private WebElement skipToMainContent;

    @FindBy(id = "a0effb954cca4759994f1ac9e9434bf4_trig")
    @CacheLookup
    private WebElement staffDebugInfo1;

    @FindBy(id = "67c26b1e826e47aaa29757f62bcd1ad0_trig")
    @CacheLookup
    private WebElement staffDebugInfo2;

    @FindBy(id = "a0effb954cca4759994f1ac9e9434bf4_history_trig")
    @CacheLookup
    private WebElement submissionHistory;

    @FindBy(css = "button.submit.btn-brand")
    @CacheLookup
    private WebElement submitsubmitYourAnswer;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/social_integration/3cd8951d4a0e437093ea888456fc9009/']")
    @CacheLookup
    private WebElement subseccin;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/8677f0addd9043e086d08003c0a3c3ff/']")
    @CacheLookup
    private WebElement subsection1;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/aff7a240c262415cafe319eaaad38443/']")
    @CacheLookup
    private WebElement subsection2;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/53364f99d80c45c6a9e7a8a1eff11a66/']")
    @CacheLookup
    private WebElement subsection3;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/d6242f3119214b06b1c75e96ee04e0bd/']")
    @CacheLookup
    private WebElement subsection4;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/275f9f8408ca4857ad0f21fa903e8061/']")
    @CacheLookup
    private WebElement subsection5;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/84082d116c084cccbc9fff5d4cdb1d7a/']")
    @CacheLookup
    private WebElement subsection6;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/1414ffd5143b4b508f739b563ab468b7/653a814a7d974062885efdc069dbd6ac/']")
    @CacheLookup
    private WebElement subsection7;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/e4233cd820b94b4ca2e181f2d5f9b723/e53e1a3a8ed24699b25bab6f406b3638/']")
    @CacheLookup
    private WebElement subsection8;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/d8a6192ade314473a78242dfeedfbf5b/25d4334e1c30406d972a33145c896216/']")
    @CacheLookup
    private WebElement subsectionExamThisContentIs;

    @FindBy(id = "a0effb954cca4759994f1ac9e9434bf4_xqa_tag")
    @CacheLookup
    private WebElement tag1;

    @FindBy(id = "67c26b1e826e47aaa29757f62bcd1ad0_xqa_tag")
    @CacheLookup
    private WebElement tag2;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/teams/']")
    @CacheLookup
    private WebElement teams;

    @FindBy(css = "a[href='#tos']")
    @CacheLookup
    private WebElement terms;

    @FindBy(id = "a0effb954cca4759994f1ac9e9434bf4_history_student_username")
    @CacheLookup
    private WebElement user1;

    @FindBy(id = "67c26b1e826e47aaa29757f62bcd1ad0_history_student_username")
    @CacheLookup
    private WebElement user2;

    @FindBy(id = "sd_fu_a0effb954cca4759994f1ac9e9434bf4")
    @CacheLookup
    private WebElement username1;

    @FindBy(id = "sd_fu_67c26b1e826e47aaa29757f62bcd1ad0")
    @CacheLookup
    private WebElement username2;

    @FindBy(id = "action-preview-username")
    @CacheLookup
    private WebElement usernameOrEmail;

    @FindBy(name = "submit")
    @CacheLookup
    private WebElement viewHistory1;

    @FindBy(name = "submit")
    @CacheLookup
    private WebElement viewHistory2;

    @FindBy(id = "action-preview-select")
    @CacheLookup
    private WebElement viewThisCourseAs;

    @FindBy(css = "a[href='//studio.edunext.co/container/block-v1:sandbox+sandbox+02+type@vertical+block@54bb9b142c6c4c22afc62bcb628f0e68']")
    @CacheLookup
    private WebElement viewUnitInStudio;

    @FindBy(id = "week-0-parent")
    @CacheLookup
    private WebElement week0;

    @FindBy(css = "a[href='/courses/course-v1:sandbox+sandbox+02/courseware/fe04a5c503d14561a0fbf9178f31ea3a/7a4c408314824c86b7198590f609069e/']")
    @CacheLookup
    private WebElement welcome;

    public EdXPreviewQuestions() {
    }

    public EdXPreviewQuestions(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXPreviewQuestions(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXPreviewQuestions(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on A Look Ahead Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickALookAheadLink() {
        aLookAhead.click();
        return this;
    }

    /**
     * Click on About Exams And Certificates Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickAboutExamsAndCertificatesLink() {
        aboutExamsAndCertificates.click();
        return this;
    }

    /**
     * Click on Account Settings Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickAccountSettingsLink() {
        accountSettings.click();
        return this;
    }

    /**
     * Click on Add Comment Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickAddComment1Button() {
        addComment1.click();
        return this;
    }

    /**
     * Click on Add Comment Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickAddComment2Button() {
        addComment2.click();
        return this;
    }

    /**
     * Click on Analyse De Situations Daffaire Exam This Content Is Graded Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickAnalyseDeSituationsDaffaireExamLink() {
        analyseDeSituationsDaffaireExam.click();
        return this;
    }

    /**
     * Click on Bookmarks Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickBookmarksButton() {
        bookmarks.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Click To Add Bookmark This Page Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickClickToAddBookmarkThisButton() {
        clickToAddBookmarkThis.click();
        return this;
    }

    /**
     * Click on Close Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickCloseButton() {
        close.click();
        return this;
    }

    /**
     * Click on Course Current Location Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickCourseCurrentLocationLink() {
        courseCurrentLocation.click();
        return this;
    }

    /**
     * Click on Courses Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickCoursesLink() {
        courses.click();
        return this;
    }

    /**
     * Click on Creative Commons Licensed Content With Terms As Follow Attribution Noncommercial No Derivatives Some Rights Reserved Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickCreativeCommonsLicensedContentWithLink() {
        creativeCommonsLicensedContentWith.click();
        return this;
    }

    /**
     * Click on Dashboard For Sandboxauthor Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickDashboardForSandboxauthorLink() {
        dashboardForSandboxauthor.click();
        return this;
    }

    /**
     * Click on Demo Course Overview Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickDemoCourseOverviewLink() {
        demoCourseOverview.click();
        return this;
    }

    /**
     * Click on Discussion Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickDiscussionLink() {
        discussion.click();
        return this;
    }

    /**
     * Click on Edx Exams Exam This Content Is Graded Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickEdxExamsExamThisContentLink() {
        edxExamsExamThisContent.click();
        return this;
    }

    /**
     * Click on Example Week 2 Get Interactive Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickExampleWeek2GetInteractiveLink() {
        exampleWeek2GetInteractive.click();
        return this;
    }

    /**
     * Click on Example Week 3 Be Social Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickExampleWeek3BeSocialLink() {
        exampleWeek3BeSocial.click();
        return this;
    }

    /**
     * Click on Faq Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickFaqLink() {
        faq.click();
        return this;
    }

    /**
     * Click on Form A Team Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickFormATeamLink() {
        formATeam.click();
        return this;
    }

    /**
     * Click on Google Docs Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickGoogleDocsLink() {
        googleDocs.click();
        return this;
    }

    /**
     * Click on Hide Notes Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickHideNotesButton() {
        hideNotes.click();
        return this;
    }

    /**
     * Click on Home Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickHomeLink() {
        home.click();
        return this;
    }

    /**
     * Click on Homework Essays Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickHomeworkEssaysLink() {
        homeworkEssays.click();
        return this;
    }

    /**
     * Click on Homework Find Your Study Buddy Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickHomeworkFindYourStudyLink() {
        homeworkFindYourStudy.click();
        return this;
    }

    /**
     * Click on Homework Labs And Demos Homework This Content Is Graded Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickHomeworkLabsAndDemosLink() {
        homeworkLabsAndDemos.click();
        return this;
    }

    /**
     * Click on Homework Question Styles Current Section Homework This Content Is Graded Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickHomeworkQuestionStylesCurrentLink() {
        homeworkQuestionStylesCurrent.click();
        return this;
    }

    /**
     * Click on Instructor Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickInstructorLink() {
        instructor.click();
        return this;
    }

    /**
     * Click on Introduction Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickIntroductionLink() {
        introduction.click();
        return this;
    }

    /**
     * Click on Lesson 2 Lets Get Interactive Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickLesson2LetsGetLink() {
        lesson2LetsGet.click();
        return this;
    }

    /**
     * Click on Lesson 3 Be Social Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickLesson3BeSocialLink() {
        lesson3BeSocial.click();
        return this;
    }

    /**
     * Click on Management Des Oprations Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickManagementDesOprationsLink() {
        managementDesOprations.click();
        return this;
    }

    /**
     * Click on More Options Dropdown Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickMoreOptionsDropdownLink() {
        moreOptionsDropdown.click();
        return this;
    }

    /**
     * Click on More Ways To Connect Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickMoreWaysToConnectLink() {
        moreWaysToConnect.click();
        return this;
    }

    /**
     * Click on My Profile Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickMyProfileLink() {
        myProfile.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickNext1Button() {
        next1.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickNext2Button() {
        next2.click();
        return this;
    }

    /**
     * Click on Notes Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickNotesLink() {
        notes.click();
        return this;
    }

    /**
     * Click on Ok Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickOkButton() {
        ok.click();
        return this;
    }

    /**
     * Click on Open Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickOpenLink() {
        open.click();
        return this;
    }

    /**
     * Click on Part 1 Example Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickPart1ExampleLink() {
        part1Example.click();
        return this;
    }

    /**
     * Click on Previous Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickPrevious1Button() {
        previous1.click();
        return this;
    }

    /**
     * Click on Previous Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickPrevious2Button() {
        previous2.click();
        return this;
    }

    /**
     * Click on Problem Chemical Equations Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickProblemChemicalEquationsButton() {
        problemChemicalEquations.click();
        return this;
    }

    /**
     * Click on Problem Drag And Drop Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickProblemDragAndDropButton() {
        problemDragAndDrop.click();
        return this;
    }

    /**
     * Click on Problem Instructor Programmed Responses Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickProblemInstructorProgrammedResponsesButton() {
        problemInstructorProgrammedResponses.click();
        return this;
    }

    /**
     * Click on Problem Mathematical Expressions Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickProblemMathematicalExpressionsButton() {
        problemMathematicalExpressions.click();
        return this;
    }

    /**
     * Click on Problem Multiple Choice Questions Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickProblemMultipleChoiceQuestionsButton() {
        problemMultipleChoiceQuestions.click();
        return this;
    }

    /**
     * Click on Problem Numerical Input Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickProblemNumericalInputButton() {
        problemNumericalInput.click();
        return this;
    }

    /**
     * Click on Problem Text Input Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickProblemTextInputButton() {
        problemTextInput.click();
        return this;
    }

    /**
     * Click on Reset Learners Attempts To Zero Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickResetLearnersAttemptsToZeroButton() {
        resetLearnersAttemptsToZero.click();
        return this;
    }

    /**
     * Click on Review Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickReview1Button() {
        review1.click();
        return this;
    }

    /**
     * Click on Review Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickReview2Button() {
        review2.click();
        return this;
    }

    /**
     * Click on Search Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSearchButton() {
        search.click();
        return this;
    }

    /**
     * Click on Section Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSection1Link() {
        section1.click();
        return this;
    }

    /**
     * Click on Section Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSection2Link() {
        section2.click();
        return this;
    }

    /**
     * Click on Section Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSection3Link() {
        section3.click();
        return this;
    }

    /**
     * Click on Section Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSection4Link() {
        section4.click();
        return this;
    }

    /**
     * Click on Section Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSection5Link() {
        section5.click();
        return this;
    }

    /**
     * Click on Section Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSection6Link() {
        section6.click();
        return this;
    }

    /**
     * Click on Session 1 Getting Started Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSession1GettingStartedLink() {
        session1GettingStarted.click();
        return this;
    }

    /**
     * Click on Set Preview Mode Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSetPreviewModeButton() {
        setPreviewMode.click();
        return this;
    }

    /**
     * Click on Show Discussion Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickShowDiscussionButton() {
        showDiscussion.click();
        return this;
    }

    /**
     * Click on Sign Out Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSignOutLink() {
        signOut.click();
        return this;
    }

    /**
     * Click on Skip To Main Content Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSkipToMainContentLink() {
        skipToMainContent.click();
        return this;
    }

    /**
     * Click on Staff Debug Info Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickStaffDebugInfo1Link() {
        staffDebugInfo1.click();
        return this;
    }

    /**
     * Click on Staff Debug Info Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickStaffDebugInfo2Link() {
        staffDebugInfo2.click();
        return this;
    }

    /**
     * Click on Submission History Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubmissionHistoryLink() {
        submissionHistory.click();
        return this;
    }

    /**
     * Click on Submitsubmit Your Answer Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubmitsubmitYourAnswerButton() {
        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.elementToBeClickable(submitsubmitYourAnswer));
        //ClickHelper.advancedClick((RemoteWebDriver) driver, submitsubmitYourAnswer);
        submitsubmitYourAnswer.click();
        return this;
    }

    /**
     * Click on Subseccin Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubseccinLink() {
        subseccin.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection1Link() {
        subsection1.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection2Link() {
        subsection2.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection3Link() {
        subsection3.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection4Link() {
        subsection4.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection5Link() {
        subsection5.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection6Link() {
        subsection6.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection7Link() {
        subsection7.click();
        return this;
    }

    /**
     * Click on Subsection Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsection8Link() {
        subsection8.click();
        return this;
    }

    /**
     * Click on Subsection Exam This Content Is Graded Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickSubsectionExamThisContentIsLink() {
        subsectionExamThisContentIs.click();
        return this;
    }

    /**
     * Click on Teams Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickTeamsLink() {
        teams.click();
        return this;
    }

    /**
     * Click on Terms Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickTermsLink() {
        terms.click();
        return this;
    }

    /**
     * Click on View History Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickViewHistory1Button() {
        viewHistory1.click();
        return this;
    }

    /**
     * Click on View History Button.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickViewHistory2Button() {
        viewHistory2.click();
        return this;
    }

    /**
     * Click on View Unit In Studio Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickViewUnitInStudioLink() {
        viewUnitInStudio.click();
        return this;
    }

    /**
     * Click on Week 0 Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickWeek0Link() {
        week0.click();
        return this;
    }

    /**
     * Click on Welcome Link.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions clickWelcomeLink() {
        welcome.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions fill() {
        setViewThisCourseAsDropDownListField();
        setUsernameOrEmailTextField();
        setCourseSearchTextField();
        setAWindow1CheckboxField();
        setATableRadioButtonField();
        setADeskRadioButtonField();
        setAChairCorrectRadioButtonField();
        setABookshelfRadioButtonField();
        setAPianoCheckboxField();
        setATreeCheckboxField();
        setAGuitarCheckboxField();
        setAWindow2CheckboxField();
        setComment1TextField();
        setTag1TextField();
        setUsername1TextField();
        setUser1TextField();
        setComment2TextField();
        setTag2TextField();
        setUsername2TextField();
        setUser2TextField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to A Bookshelf Radio Button field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setABookshelfRadioButtonField() {
        for (WebElement el : aBookshelf) {
            if (el.getAttribute("value").equals(aBookshelfValue)) {
                if (!el.isSelected()) {
                    el.click();
                }
                break;
            }
        }
        return this;
    }

    /**
     * Set default value to A Chair Correct Radio Button field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setAChairCorrectRadioButtonField() {
        for (WebElement el : aChairCorrect) {
            if (el.getAttribute("value").equals(aChairCorrectValue)) {
                if (!el.isSelected()) {
                    el.click();
                }
                break;
            }
        }
        return this;
    }

    /**
     * Set default value to A Desk Radio Button field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setADeskRadioButtonField() {
        for (WebElement el : aDesk) {
            if (el.getAttribute("value").equals(aDeskValue)) {
                if (!el.isSelected()) {
                    el.click();
                }
                break;
            }
        }
        return this;
    }

    /**
     * Set A Guitar Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setAGuitarCheckboxField() {
        if (!aGuitar.isSelected()) {
            aGuitar.click();
        }
        return this;
    }

    /**
     * Set A Piano Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setAPianoCheckboxField() {
        if (!aPiano.isSelected()) {
            aPiano.click();
        }
        return this;
    }

    /**
     * Set default value to A Table Radio Button field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setATableRadioButtonField() {
        for (WebElement el : aTable) {
            if (el.getAttribute("value").equals(aTableValue)) {
                if (!el.isSelected()) {
                    el.click();
                }
                break;
            }
        }
        return this;
    }

    /**
     * Set A Tree Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setATreeCheckboxField() {
        if (!aTree.isSelected()) {
            aTree.click();
        }
        return this;
    }

    /**
     * Set default value to A Window Drop Down List field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setAWindow1CheckboxField() {
        return setAWindow1CheckboxField(data.get("A_WINDOW"));
    }

    /**
     * Set A Window Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setAWindow1CheckboxField(String aWindowValue) {
        new Select(aWindow1).selectByVisibleText(aWindowValue);
        return this;
    }

    /**
     * Set A Window Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setAWindow2CheckboxField() {
        if (!aWindow2.isSelected()) {
            aWindow2.click();
        }
        return this;
    }

    /**
     * Set default value to Comment Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setComment1TextField() {
        return setComment1TextField(data.get("COMMENT_1"));
    }

    /**
     * Set value to Comment Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setComment1TextField(String comment1Value) {
        comment1.sendKeys(comment1Value);
        return this;
    }

    /**
     * Set default value to Comment Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setComment2TextField() {
        return setComment2TextField(data.get("COMMENT_2"));
    }

    /**
     * Set value to Comment Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setComment2TextField(String comment2Value) {
        comment2.sendKeys(comment2Value);
        return this;
    }

    /**
     * Set default value to Course Search Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setCourseSearchTextField() {
        return setCourseSearchTextField(data.get("COURSE_SEARCH"));
    }

    /**
     * Set value to Course Search Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setCourseSearchTextField(String courseSearchValue) {
        courseSearch.sendKeys(courseSearchValue);
        return this;
    }

    /**
     * Set default value to Tag Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setTag1TextField() {
        return setTag1TextField(data.get("TAG_1"));
    }

    /**
     * Set value to Tag Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setTag1TextField(String tag1Value) {
        tag1.sendKeys(tag1Value);
        return this;
    }

    /**
     * Set default value to Tag Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setTag2TextField() {
        return setTag2TextField(data.get("TAG_2"));
    }

    /**
     * Set value to Tag Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setTag2TextField(String tag2Value) {
        tag2.sendKeys(tag2Value);
        return this;
    }

    /**
     * Set default value to User Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUser1TextField() {
        return setUser1TextField(data.get("USER_1"));
    }

    /**
     * Set value to User Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUser1TextField(String user1Value) {
        user1.sendKeys(user1Value);
        return this;
    }

    /**
     * Set default value to User Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUser2TextField() {
        return setUser2TextField(data.get("USER_2"));
    }

    /**
     * Set value to User Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUser2TextField(String user2Value) {
        user2.sendKeys(user2Value);
        return this;
    }

    /**
     * Set default value to Username Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUsername1TextField() {
        return setUsername1TextField(data.get("USERNAME_1"));
    }

    /**
     * Set value to Username Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUsername1TextField(String username1Value) {
        username1.sendKeys(username1Value);
        return this;
    }

    /**
     * Set default value to Username Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUsername2TextField() {
        return setUsername2TextField(data.get("USERNAME_2"));
    }

    /**
     * Set value to Username Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUsername2TextField(String username2Value) {
        username2.sendKeys(username2Value);
        return this;
    }

    /**
     * Set default value to Username Or Email Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUsernameOrEmailTextField() {
        return setUsernameOrEmailTextField(data.get("USERNAME_OR_EMAIL"));
    }

    /**
     * Set value to Username Or Email Text field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setUsernameOrEmailTextField(String usernameOrEmailValue) {
        usernameOrEmail.sendKeys(usernameOrEmailValue);
        return this;
    }

    /**
     * Set default value to View This Course As Drop Down List field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setViewThisCourseAsDropDownListField() {
        return setViewThisCourseAsDropDownListField(data.get("VIEW_THIS_COURSE_AS"));
    }

    /**
     * Set value to View This Course As Drop Down List field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions setViewThisCourseAsDropDownListField(String viewThisCourseAsValue) {
        new Select(viewThisCourseAs).selectByVisibleText(viewThisCourseAsValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions submit() {
        clickSetPreviewModeButton();
        return this;
    }

    /**
     * Unset A Guitar Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetAGuitarCheckboxField() {
        if (aGuitar.isSelected()) {
            aGuitar.click();
        }
        return this;
    }

    /**
     * Unset A Piano Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetAPianoCheckboxField() {
        if (aPiano.isSelected()) {
            aPiano.click();
        }
        return this;
    }

    /**
     * Unset A Tree Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetATreeCheckboxField() {
        if (aTree.isSelected()) {
            aTree.click();
        }
        return this;
    }

    /**
     * Unset default value from A Window Drop Down List field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetAWindow1CheckboxField() {
        return unsetAWindow1CheckboxField(data.get("A_WINDOW"));
    }

    /**
     * Unset A Window Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetAWindow1CheckboxField(String aWindowValue) {
        new Select(aWindow1).deselectByVisibleText(aWindowValue);
        return this;
    }

    /**
     * Unset A Window Checkbox field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetAWindow2CheckboxField() {
        if (aWindow2.isSelected()) {
            aWindow2.click();
        }
        return this;
    }

    /**
     * Unset default value from View This Course As Drop Down List field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetViewThisCourseAsDropDownListField() {
        return unsetViewThisCourseAsDropDownListField(data.get("VIEW_THIS_COURSE_AS"));
    }

    /**
     * Unset value from View This Course As Drop Down List field.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions unsetViewThisCourseAsDropDownListField(String viewThisCourseAsValue) {
        new Select(viewThisCourseAs).deselectByVisibleText(viewThisCourseAsValue);
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXPreviewQuestions class instance.
     */
    public EdXPreviewQuestions verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
