package cz.cvut.openedx.pages;

import java.util.Map;

import cz.cvut.openedx.helpers.ClickHelper;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXOverview {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "input.action.action-secondary.action-cancel.new-library-cancel")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "a[href='/library/library-v1:BBAU+CS-001']")
    @CacheLookup
    private WebElement computerScienceOrganizationBbauCourse;

    @FindBy(css = "#course-index-tabs li:nth-of-type(1) a")
    @CacheLookup
    private WebElement courses;

    @FindBy(css = "input.action.action-primary.new-library-save")
    @CacheLookup
    private WebElement create;

    @FindBy(css = ".ltr.is-signedin.index.view-dashboard.lang_en.js div:nth-of-type(1) div:nth-of-type(4) footer.primary div:nth-of-type(1) div.colophon p a")
    @CacheLookup
    private WebElement edunext;

    @FindBy(css = "a[href='/course/course-v1:sandbox+sandbox+02']")
    @CacheLookup
    private WebElement edxDemonstrationCourseOrganizationSandbox1;

    @FindBy(css = "a[href='/course/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement edxDemonstrationCourseOrganizationSandbox2;

    @FindBy(css = "a[title='Access documentation on http://docs.edx.org']")
    @CacheLookup
    private WebElement edxDocumentation;

    @FindBy(css = "a[href='https://www.edx.org/']")
    @CacheLookup
    private WebElement edxInc;

    @FindBy(css = "a[title='Enroll in edX101: Overview of Creating an edX Course']")
    @CacheLookup
    private WebElement enrollInEdx101;

    @FindBy(css = "a[title='Enroll in StudioX: Creating a Course with edX Studio']")
    @CacheLookup
    private WebElement enrollInStudiox;

    @FindBy(css = "a[href='/library/library-v1:RenaissanceCentre+ETRDC']")
    @CacheLookup
    private WebElement enterpriseDevelopmentOrganizationRenaissancecentreCourse;

    @FindBy(css = "#content div:nth-of-type(2) section.content aside.content-supplementary div.bit ol.list-actions li.action-item a")
    @CacheLookup
    private WebElement gettingStartedWithStudio;

    @FindBy(css = "a[href='/library/library-v1:HCA+healthcare']")
    @CacheLookup
    private WebElement healthCareOrganizationHcaCourse;

    @FindBy(css = "a[title='Contextual Online Help']")
    @CacheLookup
    private WebElement help;

    @FindBy(css = "#course-index-tabs li:nth-of-type(2) a")
    @CacheLookup
    private WebElement libraries;

    @FindBy(id = "new-library-number")
    @CacheLookup
    private WebElement libraryCode;

    @FindBy(id = "new-library-name")
    @CacheLookup
    private WebElement libraryName;

    @FindBy(css = "a.cta.cta-show-sock")
    @CacheLookup
    private WebElement lookingForHelpWithStudio;

    @FindBy(css = "a[href='/library/library-v1:company+bo']")
    @CacheLookup
    private WebElement networkingOrganizationCompanyCourseNumber;

    @FindBy(css = "a.button.new-button.new-library-button")
    @CacheLookup
    private WebElement newLibrary;

    @FindBy(css = "a[title='Access the Open edX Portal']")
    @CacheLookup
    private WebElement openEdxPortal;

    @FindBy(css = "a[href='/library/library-v1:FCT-FCCN+FCCN-Services']")
    @CacheLookup
    private WebElement openedxTrainingOrganizationFctfccnCourse;

    @FindBy(id = "new-library-org")
    @CacheLookup
    private WebElement organization;

    private final String pageLoadedText = "Your request is currently being reviewed by eduNEXT staff and should be updated shortly";

    private final String pageUrl = "/home/";

    @FindBy(css = "a[href='/library/library-v1:eonit+safe']")
    @CacheLookup
    private WebElement safeOrganizationEonitCourseNumber;

    @FindBy(css = "a.action.action-signout")
    @CacheLookup
    private WebElement signOut;

    @FindBy(css = "a.nav-skip")
    @CacheLookup
    private WebElement skipToMainContent;

    @FindBy(css = "#view-top header.primary div:nth-of-type(2) nav.nav-account.nav-is-signedin.nav-dd.ui-right ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement studioHome;

    @FindBy(css = "a[href='//sandbox.edunext.io/courses/course-v1:sandbox+sandbox+02/jump_to/block-v1:sandbox+sandbox+02+type@course+block@course']")
    @CacheLookup
    private WebElement viewLive1;

    @FindBy(css = "a[href='//sandbox.edunext.io/courses/course-v1:sandbox+Demo+01/jump_to/block-v1:sandbox+Demo+01+type@course+block@course']")
    @CacheLookup
    private WebElement viewLive2;

    @FindBy(css = "a.ui-toggle-control.current.show-creationrights")
    @CacheLookup
    private WebElement yourCourseCreatorRequestStatus;

    public EdXOverview() {
    }

    public EdXOverview(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXOverview(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXOverview(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Computer Science Organization Bbau Course Number Cs001 Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickComputerScienceOrganizationBbauCourseLink() {
        computerScienceOrganizationBbauCourse.click();
        return this;
    }

    /**
     * Click on Courses Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickCoursesLink() {
        courses.click();
        return this;
    }

    /**
     * Click on Create Button.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickCreateButton() {
        create.click();
        return this;
    }

    /**
     * Click on Edunext Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEdunextLink() {
        edunext.click();
        return this;
    }

    /**
     * Click on Edx Demonstration Course Organization Sandbox Course Number Demo Course Run 01 Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEdxDemonstrationCourseOrganizationSandbox1Link() {
        edxDemonstrationCourseOrganizationSandbox1.click();
        return this;
    }

    /**
     * Click on Edx Demonstration Course Organization Sandbox Course Number Demo Course Run 01 Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEdxDemonstrationCourseOrganizationSandbox2Link() {
        edxDemonstrationCourseOrganizationSandbox2.click();
        return this;
    }

    /**
     * Click on Edx Documentation Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEdxDocumentationLink() {
        edxDocumentation.click();
        return this;
    }

    /**
     * Click on Edx Inc. Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEdxIncLink() {
        edxInc.click();
        return this;
    }

    /**
     * Click on Enroll In Edx101 Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEnrollInEdx101Link() {
        enrollInEdx101.click();
        return this;
    }

    /**
     * Click on Enroll In Studiox Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEnrollInStudioxLink() {
        enrollInStudiox.click();
        return this;
    }

    /**
     * Click on Enterprise Development Organization Renaissancecentre Course Number Etrdc Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickEnterpriseDevelopmentOrganizationRenaissancecentreCourseLink() {
        enterpriseDevelopmentOrganizationRenaissancecentreCourse.click();
        return this;
    }

    /**
     * Click on Getting Started With Studio Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickGettingStartedWithStudioLink() {
        gettingStartedWithStudio.click();
        return this;
    }

    /**
     * Click on Health Care Organization Hca Course Number Healthcare Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickHealthCareOrganizationHcaCourseLink() {
        healthCareOrganizationHcaCourse.click();
        return this;
    }

    /**
     * Click on Help Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickHelpLink() {
        help.click();
        return this;
    }

    /**
     * Click on Libraries Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickLibrariesLink() {
        libraries.click();
        return this;
    }

    /**
     * Click on Looking For Help With Studio Hide Studio Help Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickLookingForHelpWithStudioLink() {
        lookingForHelpWithStudio.click();
        return this;
    }

    /**
     * Click on Networking Organization Company Course Number Bo Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickNetworkingOrganizationCompanyCourseNumberLink() {
        networkingOrganizationCompanyCourseNumber.click();
        return this;
    }

    /**
     * Click on New Library Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickNewLibraryLink() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ClickHelper.advancedClick((RemoteWebDriver) driver, newLibrary);
        return this;
    }

    /**
     * Click on Open Edx Portal Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickOpenEdxPortalLink() {
        openEdxPortal.click();
        return this;
    }

    /**
     * Click on Openedx Training Organization Fctfccn Course Number Fccnservices Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickOpenedxTrainingOrganizationFctfccnCourseLink() {
        openedxTrainingOrganizationFctfccnCourse.click();
        return this;
    }

    /**
     * Click on Safe Organization Eonit Course Number Safe Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickSafeOrganizationEonitCourseNumberLink() {
        safeOrganizationEonitCourseNumber.click();
        return this;
    }

    /**
     * Click on Sign Out Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickSignOutLink() {
        signOut.click();
        return this;
    }

    /**
     * Click on Skip To Main Content Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickSkipToMainContentLink() {
        skipToMainContent.click();
        return this;
    }

    /**
     * Click on Studio Home Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickStudioHomeLink() {
        studioHome.click();
        return this;
    }

    /**
     * Click on View Live Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickViewLive1Link() {
        viewLive1.click();
        return this;
    }

    /**
     * Click on View Live Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickViewLive2Link() {
        viewLive2.click();
        return this;
    }

    /**
     * Click on Your Course Creator Request Status Link.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview clickYourCourseCreatorRequestStatusLink() {
        yourCourseCreatorRequestStatus.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview fill() {
        setLibraryNameTextField();
        setOrganizationTextField();
        setLibraryCodeTextField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Library Code Text field.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview setLibraryCodeTextField() {
        return setLibraryCodeTextField(data.get("LIBRARY_CODE"));
    }

    /**
     * Set value to Library Code Text field.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview setLibraryCodeTextField(String libraryCodeValue) {
        libraryCode.sendKeys(libraryCodeValue);
        return this;
    }

    /**
     * Set default value to Library Name Text field.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview setLibraryNameTextField() {
        return setLibraryNameTextField(data.get("LIBRARY_NAME"));
    }

    /**
     * Set value to Library Name Text field.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview setLibraryNameTextField(String libraryNameValue) {
        libraryName.sendKeys(libraryNameValue);
        return this;
    }

    /**
     * Set default value to Organization Text field.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview setOrganizationTextField() {
        return setOrganizationTextField(data.get("ORGANIZATION"));
    }

    /**
     * Set value to Organization Text field.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview setOrganizationTextField(String organizationValue) {
        organization.sendKeys(organizationValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview submit() {
        clickCreateButton();
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXOverview class instance.
     */
    public EdXOverview verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
