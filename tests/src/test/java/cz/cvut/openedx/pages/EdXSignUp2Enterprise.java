package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;

import cz.cvut.openedx.helpers.ClickHelper;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignUp2Enterprise {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#navigation-site-about a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el1;

    @FindBy(css = "#navigation-site-initiative a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el2;

    @FindBy(css = "#navigation-site-info a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el3;

    @FindBy(css = "#navigation-account a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el4;

    @FindBy(css = "a[href='#site-initiative']")
    @CacheLookup
    private WebElement aboutYourInitiative2;

    @FindBy(css = "button.js-kirigami-edit__save.kirigami-edit__save.modal-action.modal-close.btn.waves-effect.waves-light.blue")
    @CacheLookup
    private WebElement accept;

    @FindBy(css = "a[href='#account']")
    @CacheLookup
    private WebElement accountInfo4;

    @FindBy(css = "button.modal-action.modal-close.btn-flat.waves-effect.waves-light.red-text")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "a[href='https://demo.edunext.io']")
    @CacheLookup
    private WebElement goToTheDemoSite1;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.sandbox div.col.s12 a:nth-of-type(1) button.btn.light-blue.lighten-1.waves-effect.white-text")
    @CacheLookup
    private WebElement goToTheDemoSite2;

    @FindBy(css = "a[href='https://sandbox.edunext.io']")
    @CacheLookup
    private WebElement goToTheSandboxSite1;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.sandbox div.col.s12 a:nth-of-type(2) button.btn.light-blue.lighten-1.waves-effect.white-text")
    @CacheLookup
    private WebElement goToTheSandboxSite2;

    @FindBy(css = "a[href='#site-info']")
    @CacheLookup
    private WebElement micrositeSetup3;

    private final String pageLoadedText = "In eduNEXT's demo site, you’ll be able to register for an student account, and interact with demo courses featuring the most interesting and innovative features that the open edX platform has";

    private final String pageUrl = "/embed/register/#site-initiative";

    @FindBy(css = "a[href='#site-about']")
    @CacheLookup
    private WebElement start1;

    public EdXSignUp2Enterprise() {
    }

    public EdXSignUp2Enterprise(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignUp2Enterprise(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignUp2Enterprise(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 2 About Your Initiative Link.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickAboutYourInitiativeLink2() {
        aboutYourInitiative2.click();
        return this;
    }

    /**
     * Click on Accept Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickAcceptButton() {
        accept.click();
        return this;
    }

    /**
     * Click on 4 Account Info Link.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickAccountInfoLink4() {
        accountInfo4.click();
        return this;
    }

    /**
     * Click on 1 Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickButton1() {
        el1.click();
        return this;
    }

    /**
     * Click on 2 Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickButton2() {
        el2.click();
        return this;
    }

    /**
     * Click on 3 Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickButton3() {
        el3.click();
        return this;
    }

    /**
     * Click on 4 Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickButton4() {
        el4.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Go To The Demo Site Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickGoToTheDemoSite1Button() {
        goToTheDemoSite1.click();
        return this;
    }

    /**
     * Click on Go To The Demo Site Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickGoToTheDemoSite2Button() {
        goToTheDemoSite2.click();
        return this;
    }

    /**
     * Click on Go To The Sandbox Site Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickGoToTheSandboxSite1Button() {
        goToTheSandboxSite1.click();
        return this;
    }

    /**
     * Click on Go To The Sandbox Site Button.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickGoToTheSandboxSite2Button() {
        ClickHelper.advancedClick((RemoteWebDriver) driver, goToTheSandboxSite2);
        return this;
    }

    /**
     * Click on 3 Microsite Setup Link.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickMicrositeSetupLink3() {
        micrositeSetup3.click();
        return this;
    }

    /**
     * Click on 1 Start Link.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise clickStartLink1() {
        start1.click();
        return this;
    }

    /*
    /**
     * Submit the form to target page.
     *
     * @return the EdXSignUp2Enterprise class instance.

    public EdXSignUp2Enterprise submit() {
        click1Button();
        return this;
    } */

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSignUp2Enterprise class instance.
     */
    public EdXSignUp2Enterprise verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
