package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXScheduleDetails {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(1) td:nth-of-type(6) a.ui-state-default")
    @CacheLookup
    private WebElement we1;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(3) td:nth-of-type(1) a.ui-state-default")
    @CacheLookup
    private WebElement we10;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(3) td:nth-of-type(2) a.ui-state-default")
    @CacheLookup
    private WebElement we11;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(3) td:nth-of-type(3) a.ui-state-default")
    @CacheLookup
    private WebElement we12;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(3) td:nth-of-type(4) a.ui-state-default")
    @CacheLookup
    private WebElement we13;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(3) td:nth-of-type(5) a.ui-state-default")
    @CacheLookup
    private WebElement we14;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(3) td:nth-of-type(6) a.ui-state-default")
    @CacheLookup
    private WebElement we15;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(3) td:nth-of-type(7) a.ui-state-default")
    @CacheLookup
    private WebElement we16;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(4) td:nth-of-type(1) a.ui-state-default")
    @CacheLookup
    private WebElement we17;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(4) td:nth-of-type(2) a.ui-state-default")
    @CacheLookup
    private WebElement we18;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(4) td:nth-of-type(3) a.ui-state-default")
    @CacheLookup
    private WebElement we19;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(1) td:nth-of-type(7) a.ui-state-default")
    @CacheLookup
    private WebElement we2;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(4) td:nth-of-type(4) a.ui-state-default")
    @CacheLookup
    private WebElement we20;

    @FindBy(css = "a.ui-state-default.ui-state-active")
    @CacheLookup
    private WebElement we21;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(4) td:nth-of-type(6) a.ui-state-default")
    @CacheLookup
    private WebElement we22;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(4) td:nth-of-type(7) a.ui-state-default")
    @CacheLookup
    private WebElement we23;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(5) td:nth-of-type(1) a.ui-state-default")
    @CacheLookup
    private WebElement we24;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(5) td:nth-of-type(2) a.ui-state-default")
    @CacheLookup
    private WebElement we25;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(5) td:nth-of-type(3) a.ui-state-default")
    @CacheLookup
    private WebElement we26;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(5) td:nth-of-type(4) a.ui-state-default")
    @CacheLookup
    private WebElement we27;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(5) td:nth-of-type(5) a.ui-state-default")
    @CacheLookup
    private WebElement we28;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(5) td:nth-of-type(6) a.ui-state-default")
    @CacheLookup
    private WebElement we29;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(2) td:nth-of-type(1) a.ui-state-default")
    @CacheLookup
    private WebElement we3;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(5) td:nth-of-type(7) a.ui-state-default")
    @CacheLookup
    private WebElement we30;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(6) td:nth-of-type(1) a.ui-state-default")
    @CacheLookup
    private WebElement we31;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(2) td:nth-of-type(2) a.ui-state-default")
    @CacheLookup
    private WebElement we4;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(2) td:nth-of-type(3) a.ui-state-default")
    @CacheLookup
    private WebElement we5;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(2) td:nth-of-type(4) a.ui-state-default")
    @CacheLookup
    private WebElement we6;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(2) td:nth-of-type(5) a.ui-state-default")
    @CacheLookup
    private WebElement we7;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(2) td:nth-of-type(6) a.ui-state-default")
    @CacheLookup
    private WebElement we8;

    @FindBy(css = "#ui-datepicker-div table.ui-datepicker-calendar tbody tr:nth-of-type(2) td:nth-of-type(7) a.ui-state-default")
    @CacheLookup
    private WebElement we9;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(5) a")
    @CacheLookup
    private WebElement advancedSettings1;

    @FindBy(css = "#content div:nth-of-type(2) div.content div:nth-of-type(2) div:nth-of-type(2) nav.nav-related ul li:nth-of-type(4) a")
    @CacheLookup
    private WebElement advancedSettings2;

    @FindBy(name = "license-all-rights-reserved")
    @CacheLookup
    private WebElement allRightsReserved;

    @FindBy(id = "creative-commons-BY")
    @CacheLookup
    private WebElement attribution;

    @FindBy(css = "a[href='/certificates/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement certificates;

    @FindBy(id = "course-image-url")
    @CacheLookup
    private WebElement courseCardImage;

    @FindBy(id = "course-end-date")
    @CacheLookup
    private WebElement courseEndDate;

    @FindBy(id = "course-end-time")
    @CacheLookup
    private WebElement courseEndTime;

    @FindBy(id = "course-introduction-video")
    @CacheLookup
    private WebElement courseIntroductionVideo;

    @FindBy(id = "course-language")
    @CacheLookup
    private WebElement courseLanguage;

    @FindBy(id = "course-number")
    @CacheLookup
    private WebElement courseNumber;

    @FindBy(css = "#settings_details div:nth-of-type(5) ol.list-input li:nth-of-type(3) div.show-data div:nth-of-type(2) p:nth-of-type(1) span.tip.tip-inline a")
    @CacheLookup
    private WebElement courseOutline;

    @FindBy(id = "course-overview")
    @CacheLookup
    private WebElement courseOverview;

    @FindBy(id = "course-name")
    @CacheLookup
    private WebElement courseRun;

    @FindBy(id = "course-short-description")
    @CacheLookup
    private WebElement courseShortDescription;

    @FindBy(id = "course-start-date")
    @CacheLookup
    private WebElement courseStartDate;

    @FindBy(id = "course-start-time")
    @CacheLookup
    private WebElement courseStartTime;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(3) a")
    @CacheLookup
    private WebElement courseTeam1;

    @FindBy(css = "#content div:nth-of-type(2) div.content div:nth-of-type(2) div:nth-of-type(2) nav.nav-related ul li:nth-of-type(2) a")
    @CacheLookup
    private WebElement courseTeam2;

    @FindBy(name = "license-creative-commons")
    @CacheLookup
    private WebElement creativeCommons;

    @FindBy(css = "a.remove-item.remove-course-introduction-video.remove-video-data")
    @CacheLookup
    private WebElement deleteCurrentVideo;

    @FindBy(css = ".ltr.is-signedin.course.schedule.view-settings.feature-upload.lang_en.js div:nth-of-type(1) div:nth-of-type(4) footer.primary div:nth-of-type(1) div.colophon p a")
    @CacheLookup
    private WebElement edunext;

    @FindBy(css = "a[title='Access documentation on http://docs.edx.org']")
    @CacheLookup
    private WebElement edxDocumentation;

    @FindBy(css = "a[href='https://www.edx.org/']")
    @CacheLookup
    private WebElement edxInc;

    @FindBy(css = "a[title='Enroll in edX101: Overview of Creating an edX Course']")
    @CacheLookup
    private WebElement enrollInEdx101;

    @FindBy(css = "a[title='Enroll in StudioX: Creating a Course with edX Studio']")
    @CacheLookup
    private WebElement enrollInStudiox;

    @FindBy(id = "course-enrollment-end-date")
    @CacheLookup
    private WebElement enrollmentEndDate;

    @FindBy(id = "course-enrollment-end-time")
    @CacheLookup
    private WebElement enrollmentEndTime;

    @FindBy(id = "course-enrollment-start-date")
    @CacheLookup
    private WebElement enrollmentStartDate;

    @FindBy(id = "course-enrollment-start-time")
    @CacheLookup
    private WebElement enrollmentStartTime;

    @FindBy(css = "a[href='/export/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement export;

    @FindBy(css = "#field-course-image div:nth-of-type(1) span:nth-of-type(2) a")
    @CacheLookup
    private WebElement filesAndUploads;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(1) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(4) a")
    @CacheLookup
    private WebElement filesUploads;

    @FindBy(css = "a[href='https://creativecommons.org/licenses/by-nc-sa/4.0/']")
    @CacheLookup
    private WebElement gettextcreativeCommonsLicensedContentWith;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(2) a")
    @CacheLookup
    private WebElement grading1;

    @FindBy(css = "#content div:nth-of-type(2) div.content div:nth-of-type(2) div:nth-of-type(2) nav.nav-related ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement grading2;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(4) a")
    @CacheLookup
    private WebElement groupConfigurations1;

    @FindBy(css = "#content div:nth-of-type(2) div.content div:nth-of-type(2) div:nth-of-type(2) nav.nav-related ul li:nth-of-type(3) a")
    @CacheLookup
    private WebElement groupConfigurations2;

    @FindBy(css = "a[title='Contextual Online Help']")
    @CacheLookup
    private WebElement help;

    @FindBy(id = "course-effort")
    @CacheLookup
    private WebElement hoursOfEffortPerWeek;

    @FindBy(id = "course-overview-cm-textarea")
    @CacheLookup
    private WebElement htmlCodeEditor;

    @FindBy(css = "a[href='/import/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement weImport;

    @FindBy(name = "self-paced")
    @CacheLookup
    private List<WebElement> instructorpaced;

    private final String instructorpacedValue = "false";

    @FindBy(css = "a[title='Send a note to students via email']")
    @CacheLookup
    private WebElement inviteYourStudents;

    @FindBy(css = "a[href='https://creativecommons.org/about']")
    @CacheLookup
    private WebElement learnMoreAboutCreativeCommons;

    @FindBy(css = "a.cta.cta-show-sock")
    @CacheLookup
    private WebElement lookingForHelpWithStudio;

    @FindBy(css = "a.ui-datepicker-next.ui-corner-all")
    @CacheLookup
    private WebElement next;

    @FindBy(id = "creative-commons-ND")
    @CacheLookup
    private WebElement noDerivatives;

    @FindBy(id = "creative-commons-NC")
    @CacheLookup
    private WebElement noncommercial;

    @FindBy(css = "a[title='Access the Open edX Portal']")
    @CacheLookup
    private WebElement openEdxPortal;

    @FindBy(id = "course-organization")
    @CacheLookup
    private WebElement organization;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(1) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement outline;

    private final String pageLoadedText = "Allow others to copy, distribute, display and perform only verbatim copies of your work, not derivative works based upon it";

    private final String pageUrl = "/settings/details/course-v1:sandbox+Demo+01";

    @FindBy(css = "a[href='/tabs/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement pages;

    @FindBy(id = "pre-requisite-course")
    @CacheLookup
    private WebElement prerequisiteCourse;

    @FindBy(css = "a.ui-datepicker-prev.ui-corner-all")
    @CacheLookup
    private WebElement prev;

    @FindBy(id = "entrance-exam-enabled")
    @CacheLookup
    private WebElement requireStudentsToPassAnExam;

    @FindBy(css = "#settings_details div:nth-of-type(1) div.note.note-promotion.note-promotion-courseURL.has-actions div.copy p a.link-courseURL")
    @CacheLookup
    private WebElement sandboxEdunextIocoursescoursev1sandboxdemo01about;

    @FindBy(css = "a.course-link")
    @CacheLookup
    private WebElement sandboxdemoEdxDemonstrationCourse;

    @FindBy(css = "a[href='/settings/details/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement scheduleDetails;

    @FindBy(name = "self-paced")
    @CacheLookup
    private List<WebElement> selfpaced;

    private final String selfpacedValue = "true";

    @FindBy(name = "submit")
    @CacheLookup
    private WebElement setPrerequisiteCourse;

    @FindBy(id = "creative-commons-SA")
    @CacheLookup
    private WebElement shareAlike;

    @FindBy(css = "a.action.action-signout")
    @CacheLookup
    private WebElement signOut;

    @FindBy(css = "a.nav-skip")
    @CacheLookup
    private WebElement skipToMainContent;

    @FindBy(css = "#view-top header.primary div:nth-of-type(2) nav.nav-account.nav-is-signedin.nav-dd.ui-right ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement studioHome;

    @FindBy(css = "a[href='/textbooks/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement textbooks;

    @FindBy(css = "a[href='/course_info/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement updates;

    @FindBy(id = "upload-course-image")
    @CacheLookup
    private WebElement uploadCourseCardImage;

    @FindBy(id = "entrance-exam-minimum-score-pct")
    @CacheLookup
    private WebElement youCanNowViewAndAuthor;

    @FindBy(css = "#field-course-overview span.tip.tip-stacked a.link-courseURL")
    @CacheLookup
    private WebElement yourCourseSummaryPage;

    public EdXScheduleDetails() {
    }

    public EdXScheduleDetails(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXScheduleDetails(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXScheduleDetails(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Advanced Settings Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickAdvancedSettings1Link() {
        advancedSettings1.click();
        return this;
    }

    /**
     * Click on Advanced Settings Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickAdvancedSettings2Link() {
        advancedSettings2.click();
        return this;
    }

    /**
     * Click on All Rights Reserved Button.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickAllRightsReservedButton() {
        allRightsReserved.click();
        return this;
    }

    /**
     * Click on Certificates Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickCertificatesLink() {
        certificates.click();
        return this;
    }

    /**
     * Click on Course Outline Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickCourseOutlineLink() {
        courseOutline.click();
        return this;
    }

    /**
     * Click on Course Team Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickCourseTeam1Link() {
        courseTeam1.click();
        return this;
    }

    /**
     * Click on Course Team Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickCourseTeam2Link() {
        courseTeam2.click();
        return this;
    }

    /**
     * Click on Creative Commons Button.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickCreativeCommonsButton() {
        creativeCommons.click();
        return this;
    }

    /**
     * Click on Delete Current Video Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickDeleteCurrentVideoLink() {
        deleteCurrentVideo.click();
        return this;
    }

    /**
     * Click on Edunext Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickEdunextLink() {
        edunext.click();
        return this;
    }

    /**
     * Click on Edx Documentation Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickEdxDocumentationLink() {
        edxDocumentation.click();
        return this;
    }

    /**
     * Click on Edx Inc. Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickEdxIncLink() {
        edxInc.click();
        return this;
    }

    /**
     * Click on Enroll In Edx101 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickEnrollInEdx101Link() {
        enrollInEdx101.click();
        return this;
    }

    /**
     * Click on Enroll In Studiox Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickEnrollInStudioxLink() {
        enrollInStudiox.click();
        return this;
    }

    /**
     * Click on Export Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickExportLink() {
        export.click();
        return this;
    }

    /**
     * Click on Files And Uploads Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickFilesAndUploadsLink() {
        filesAndUploads.click();
        return this;
    }

    /**
     * Click on Files Uploads Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickFilesUploadsLink() {
        filesUploads.click();
        return this;
    }

    /**
     * Click on Gettextcreative Commons Licensed Content With Terms As Follow Attribution Noncommercial Share Alike Some Rights Reserved Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickGettextcreativeCommonsLicensedContentWithLink() {
        gettextcreativeCommonsLicensedContentWith.click();
        return this;
    }

    /**
     * Click on Grading Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickGrading1Link() {
        grading1.click();
        return this;
    }

    /**
     * Click on Grading Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickGrading2Link() {
        grading2.click();
        return this;
    }

    /**
     * Click on Group Configurations Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickGroupConfigurations1Link() {
        groupConfigurations1.click();
        return this;
    }

    /**
     * Click on Group Configurations Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickGroupConfigurations2Link() {
        groupConfigurations2.click();
        return this;
    }

    /**
     * Click on Help Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickHelpLink() {
        help.click();
        return this;
    }

    /**
     * Click on Import Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickImportLink() {
        weImport.click();
        return this;
    }

    /**
     * Click on Invite Your Students Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickInviteYourStudentsLink() {
        inviteYourStudents.click();
        return this;
    }

    /**
     * Click on Learn More About Creative Commons Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLearnMoreAboutCreativeCommonsLink() {
        learnMoreAboutCreativeCommons.click();
        return this;
    }

    /**
     * Click on 1 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink1() {
        we1.click();
        return this;
    }

    /**
     * Click on 10 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink10() {
        we10.click();
        return this;
    }

    /**
     * Click on 11 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink11() {
        we11.click();
        return this;
    }

    /**
     * Click on 12 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink12() {
        we12.click();
        return this;
    }

    /**
     * Click on 13 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink13() {
        we13.click();
        return this;
    }

    /**
     * Click on 14 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink14() {
        we14.click();
        return this;
    }

    /**
     * Click on 15 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink15() {
        we15.click();
        return this;
    }

    /**
     * Click on 16 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink16() {
        we16.click();
        return this;
    }

    /**
     * Click on 17 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink17() {
        we17.click();
        return this;
    }

    /**
     * Click on 18 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink18() {
        we18.click();
        return this;
    }

    /**
     * Click on 19 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink19() {
        we19.click();
        return this;
    }

    /**
     * Click on 2 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink2() {
        we2.click();
        return this;
    }

    /**
     * Click on 20 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink20() {
        we20.click();
        return this;
    }

    /**
     * Click on 21 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink21() {
        we21.click();
        return this;
    }

    /**
     * Click on 22 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink22() {
        we22.click();
        return this;
    }

    /**
     * Click on 23 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink23() {
        we23.click();
        return this;
    }

    /**
     * Click on 24 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink24() {
        we24.click();
        return this;
    }

    /**
     * Click on 25 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink25() {
        we25.click();
        return this;
    }

    /**
     * Click on 26 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink26() {
        we26.click();
        return this;
    }

    /**
     * Click on 27 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink27() {
        we27.click();
        return this;
    }

    /**
     * Click on 28 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink28() {
        we28.click();
        return this;
    }

    /**
     * Click on 29 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink29() {
        we29.click();
        return this;
    }

    /**
     * Click on 3 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink3() {
        we3.click();
        return this;
    }

    /**
     * Click on 30 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink30() {
        we30.click();
        return this;
    }

    /**
     * Click on 31 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink31() {
        we31.click();
        return this;
    }

    /**
     * Click on 4 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink4() {
        we4.click();
        return this;
    }

    /**
     * Click on 5 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink5() {
        we5.click();
        return this;
    }

    /**
     * Click on 6 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink6() {
        we6.click();
        return this;
    }

    /**
     * Click on 7 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink7() {
        we7.click();
        return this;
    }

    /**
     * Click on 8 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink8() {
        we8.click();
        return this;
    }

    /**
     * Click on 9 Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLink9() {
        we9.click();
        return this;
    }

    /**
     * Click on Looking For Help With Studio Hide Studio Help Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickLookingForHelpWithStudioLink() {
        lookingForHelpWithStudio.click();
        return this;
    }

    /**
     * Click on Next Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickNextLink() {
        next.click();
        return this;
    }

    /**
     * Click on Open Edx Portal Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickOpenEdxPortalLink() {
        openEdxPortal.click();
        return this;
    }

    /**
     * Click on Outline Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickOutlineLink() {
        outline.click();
        return this;
    }

    /**
     * Click on Pages Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickPagesLink() {
        pages.click();
        return this;
    }

    /**
     * Click on Prev Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickPrevLink() {
        prev.click();
        return this;
    }

    /**
     * Click on Sandbox.edunext.iocoursescoursev1sandboxdemo01about Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickSandboxEdunextIocoursescoursev1sandboxdemo01aboutLink() {
        sandboxEdunextIocoursescoursev1sandboxdemo01about.click();
        return this;
    }

    /**
     * Click on Sandboxdemo Edx Demonstration Course Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickSandboxdemoEdxDemonstrationCourseLink() {
        sandboxdemoEdxDemonstrationCourse.click();
        return this;
    }

    /**
     * Click on Schedule Details Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickScheduleDetailsLink() {
        scheduleDetails.click();
        return this;
    }

    /**
     * Click on Set Prerequisite Course Button.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickSetPrerequisiteCourseButton() {
        setPrerequisiteCourse.click();
        return this;
    }

    /**
     * Click on Sign Out Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickSignOutLink() {
        signOut.click();
        return this;
    }

    /**
     * Click on Skip To Main Content Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickSkipToMainContentLink() {
        skipToMainContent.click();
        return this;
    }

    /**
     * Click on Studio Home Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickStudioHomeLink() {
        studioHome.click();
        return this;
    }

    /**
     * Click on Textbooks Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickTextbooksLink() {
        textbooks.click();
        return this;
    }

    /**
     * Click on Updates Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickUpdatesLink() {
        updates.click();
        return this;
    }

    /**
     * Click on Upload Course Card Image Button.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickUploadCourseCardImageButton() {
        uploadCourseCardImage.click();
        return this;
    }

    /**
     * Click on Your Course Summary Page Link.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails clickYourCourseSummaryPageLink() {
        yourCourseSummaryPage.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails fill() {
        setOrganizationTextField();
        setCourseNumberTextField();
        setCourseRunTextField();
        setCourseStartDateTextField();
        setCourseStartTimeTextField();
        setCourseEndDateTextField();
        setCourseEndTimeTextField();
        setEnrollmentStartDateTextField();
        setEnrollmentStartTimeTextField();
        setEnrollmentEndDateTextField();
        setEnrollmentEndTimeTextField();
        setCourseLanguageDropDownListField();
        setCourseShortDescriptionTextareaField();
        setCourseOverviewTextareaField();
        setHtmlCodeEditorTextareaField();
        setCourseCardImageTextField();
        setCourseIntroductionVideoTextField();
        setHoursOfEffortPerWeekTextField();
        setPrerequisiteCourseDropDownListField();
        setRequireStudentsToPassAnExamCheckboxField();
        setYouCanNowViewAndAuthorTextField();
        setInstructorpacedRadioButtonField();
        setSelfpacedRadioButtonField();
        setAttributionCheckboxField();
        setNoncommercialCheckboxField();
        setNoDerivativesCheckboxField();
        setShareAlikeCheckboxField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set Attribution Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setAttributionCheckboxField() {
        if (!attribution.isSelected()) {
            attribution.click();
        }
        return this;
    }

    /**
     * Set default value to Course Card Image Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseCardImageTextField() {
        return setCourseCardImageTextField(data.get("COURSE_CARD_IMAGE"));
    }

    /**
     * Set value to Course Card Image Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseCardImageTextField(String courseCardImageValue) {
        courseCardImage.sendKeys(courseCardImageValue);
        return this;
    }

    /**
     * Set default value to Course End Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseEndDateTextField() {
        return setCourseEndDateTextField(data.get("COURSE_END_DATE"));
    }

    /**
     * Set value to Course End Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseEndDateTextField(String courseEndDateValue) {
        courseEndDate.clear();
        courseEndDate.sendKeys(courseEndDateValue);
        return this;
    }

    /**
     * Set default value to Course End Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseEndTimeTextField() {
        return setCourseEndTimeTextField(data.get("COURSE_END_TIME"));
    }

    /**
     * Set value to Course End Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseEndTimeTextField(String courseEndTimeValue) {
        courseEndTime.clear();
        courseEndTime.sendKeys(courseEndTimeValue);
        return this;
    }

    /**
     * Set default value to Course Introduction Video Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseIntroductionVideoTextField() {
        return setCourseIntroductionVideoTextField(data.get("COURSE_INTRODUCTION_VIDEO"));
    }

    /**
     * Set value to Course Introduction Video Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseIntroductionVideoTextField(String courseIntroductionVideoValue) {
        courseIntroductionVideo.sendKeys(courseIntroductionVideoValue);
        return this;
    }

    /**
     * Set default value to Course Language Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseLanguageDropDownListField() {
        return setCourseLanguageDropDownListField(data.get("COURSE_LANGUAGE"));
    }

    /**
     * Set value to Course Language Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseLanguageDropDownListField(String courseLanguageValue) {
        new Select(courseLanguage).selectByVisibleText(courseLanguageValue);
        return this;
    }

    /**
     * Set default value to Course Number Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseNumberTextField() {
        return setCourseNumberTextField(data.get("COURSE_NUMBER"));
    }

    /**
     * Set value to Course Number Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseNumberTextField(String courseNumberValue) {
        courseNumber.sendKeys(courseNumberValue);
        return this;
    }

    /**
     * Set default value to Course Overview Textarea field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseOverviewTextareaField() {
        return setCourseOverviewTextareaField(data.get("COURSE_OVERVIEW"));
    }

    /**
     * Set value to Course Overview Textarea field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseOverviewTextareaField(String courseOverviewValue) {
        courseOverview.sendKeys(courseOverviewValue);
        return this;
    }

    /**
     * Set default value to Course Run Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseRunTextField() {
        return setCourseRunTextField(data.get("COURSE_RUN"));
    }

    /**
     * Set value to Course Run Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseRunTextField(String courseRunValue) {
        courseRun.sendKeys(courseRunValue);
        return this;
    }

    /**
     * Set default value to Course Short Description Textarea field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseShortDescriptionTextareaField() {
        return setCourseShortDescriptionTextareaField(data.get("COURSE_SHORT_DESCRIPTION"));
    }

    /**
     * Set value to Course Short Description Textarea field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseShortDescriptionTextareaField(String courseShortDescriptionValue) {
        courseShortDescription.sendKeys(courseShortDescriptionValue);
        return this;
    }

    /**
     * Set default value to Course Start Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseStartDateTextField() {
        return setCourseStartDateTextField(data.get("COURSE_START_DATE"));
    }

    /**
     * Set value to Course Start Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseStartDateTextField(String courseStartDateValue) {
        courseStartDate.clear();
        courseStartDate.sendKeys(courseStartDateValue);
        return this;
    }

    /**
     * Set default value to Course Start Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseStartTimeTextField() {
        return setCourseStartTimeTextField(data.get("COURSE_START_TIME"));
    }

    /**
     * Set value to Course Start Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setCourseStartTimeTextField(String courseStartTimeValue) {
        courseStartTime.clear();
        courseStartTime.sendKeys(courseStartTimeValue);
        return this;
    }

    /**
     * Set default value to Enrollment End Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentEndDateTextField() {
        return setEnrollmentEndDateTextField(data.get("ENROLLMENT_END_DATE"));
    }

    /**
     * Set value to Enrollment End Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentEndDateTextField(String enrollmentEndDateValue) {
        enrollmentEndDate.sendKeys(enrollmentEndDateValue);
        return this;
    }

    /**
     * Set default value to Enrollment End Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentEndTimeTextField() {
        return setEnrollmentEndTimeTextField(data.get("ENROLLMENT_END_TIME"));
    }

    /**
     * Set value to Enrollment End Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentEndTimeTextField(String enrollmentEndTimeValue) {
        enrollmentEndTime.sendKeys(enrollmentEndTimeValue);
        return this;
    }

    /**
     * Set default value to Enrollment Start Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentStartDateTextField() {
        return setEnrollmentStartDateTextField(data.get("ENROLLMENT_START_DATE"));
    }

    /**
     * Set value to Enrollment Start Date Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentStartDateTextField(String enrollmentStartDateValue) {
        enrollmentStartDate.sendKeys(enrollmentStartDateValue);
        return this;
    }

    /**
     * Set default value to Enrollment Start Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentStartTimeTextField() {
        return setEnrollmentStartTimeTextField(data.get("ENROLLMENT_START_TIME"));
    }

    /**
     * Set value to Enrollment Start Time Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setEnrollmentStartTimeTextField(String enrollmentStartTimeValue) {
        enrollmentStartTime.sendKeys(enrollmentStartTimeValue);
        return this;
    }

    /**
     * Set default value to Hours Of Effort Per Week Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setHoursOfEffortPerWeekTextField() {
        return setHoursOfEffortPerWeekTextField(data.get("HOURS_OF_EFFORT_PER_WEEK"));
    }

    /**
     * Set value to Hours Of Effort Per Week Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setHoursOfEffortPerWeekTextField(String hoursOfEffortPerWeekValue) {
        hoursOfEffortPerWeek.sendKeys(hoursOfEffortPerWeekValue);
        return this;
    }

    /**
     * Set default value to Html Code Editor Textarea field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setHtmlCodeEditorTextareaField() {
        return setHtmlCodeEditorTextareaField(data.get("HTML_CODE_EDITOR"));
    }

    /**
     * Set value to Html Code Editor Textarea field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setHtmlCodeEditorTextareaField(String htmlCodeEditorValue) {
        htmlCodeEditor.sendKeys(htmlCodeEditorValue);
        return this;
    }

    /**
     * Set default value to Instructorpaced Radio Button field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setInstructorpacedRadioButtonField() {
        for (WebElement el : instructorpaced) {
            if (el.getAttribute("value").equals(instructorpacedValue)) {
                if (!el.isSelected()) {
                    el.click();
                }
                break;
            }
        }
        return this;
    }

    /**
     * Set No Derivatives Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setNoDerivativesCheckboxField() {
        if (!noDerivatives.isSelected()) {
            noDerivatives.click();
        }
        return this;
    }

    /**
     * Set Noncommercial Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setNoncommercialCheckboxField() {
        if (!noncommercial.isSelected()) {
            noncommercial.click();
        }
        return this;
    }

    /**
     * Set default value to Organization Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setOrganizationTextField() {
        return setOrganizationTextField(data.get("ORGANIZATION"));
    }

    /**
     * Set value to Organization Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setOrganizationTextField(String organizationValue) {
        organization.sendKeys(organizationValue);
        return this;
    }

    /**
     * Set default value to Prerequisite Course Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setPrerequisiteCourseDropDownListField() {
        return setPrerequisiteCourseDropDownListField(data.get("PREREQUISITE_COURSE"));
    }

    /**
     * Set value to Prerequisite Course Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setPrerequisiteCourseDropDownListField(String prerequisiteCourseValue) {
        new Select(prerequisiteCourse).selectByVisibleText(prerequisiteCourseValue);
        return this;
    }

    /**
     * Set Require Students To Pass An Exam Before Beginning The Course. Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setRequireStudentsToPassAnExamCheckboxField() {
        if (!requireStudentsToPassAnExam.isSelected()) {
            requireStudentsToPassAnExam.click();
        }
        return this;
    }

    /**
     * Set default value to Selfpaced Radio Button field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setSelfpacedRadioButtonField() {
        for (WebElement el : selfpaced) {
            if (el.getAttribute("value").equals(selfpacedValue)) {
                if (!el.isSelected()) {
                    el.click();
                }
                break;
            }
        }
        return this;
    }

    /**
     * Set Share Alike Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setShareAlikeCheckboxField() {
        if (!shareAlike.isSelected()) {
            shareAlike.click();
        }
        return this;
    }

    /**
     * Set default value to You Can Now View And Author Your Course Entrance Exam From The Course Outline Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setYouCanNowViewAndAuthorTextField() {
        return setYouCanNowViewAndAuthorTextField(data.get("YOU_CAN_NOW_VIEW_AND_AUTHOR"));
    }

    /**
     * Set value to You Can Now View And Author Your Course Entrance Exam From The Course Outline Text field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails setYouCanNowViewAndAuthorTextField(String youCanNowViewAndAuthorValue) {
        youCanNowViewAndAuthor.sendKeys(youCanNowViewAndAuthorValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails submit() {
        clickSetPrerequisiteCourseButton();
        return this;
    }

    /**
     * Unset Attribution Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetAttributionCheckboxField() {
        if (attribution.isSelected()) {
            attribution.click();
        }
        return this;
    }

    /**
     * Unset default value from Course Language Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetCourseLanguageDropDownListField() {
        return unsetCourseLanguageDropDownListField(data.get("COURSE_LANGUAGE"));
    }

    /**
     * Unset value from Course Language Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetCourseLanguageDropDownListField(String courseLanguageValue) {
        new Select(courseLanguage).deselectByVisibleText(courseLanguageValue);
        return this;
    }

    /**
     * Unset No Derivatives Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetNoDerivativesCheckboxField() {
        if (noDerivatives.isSelected()) {
            noDerivatives.click();
        }
        return this;
    }

    /**
     * Unset Noncommercial Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetNoncommercialCheckboxField() {
        if (noncommercial.isSelected()) {
            noncommercial.click();
        }
        return this;
    }

    /**
     * Unset default value from Prerequisite Course Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetPrerequisiteCourseDropDownListField() {
        return unsetPrerequisiteCourseDropDownListField(data.get("PREREQUISITE_COURSE"));
    }

    /**
     * Unset value from Prerequisite Course Drop Down List field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetPrerequisiteCourseDropDownListField(String prerequisiteCourseValue) {
        new Select(prerequisiteCourse).deselectByVisibleText(prerequisiteCourseValue);
        return this;
    }

    /**
     * Unset Require Students To Pass An Exam Before Beginning The Course. Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetRequireStudentsToPassAnExamCheckboxField() {
        if (requireStudentsToPassAnExam.isSelected()) {
            requireStudentsToPassAnExam.click();
        }
        return this;
    }

    /**
     * Unset Share Alike Checkbox field.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails unsetShareAlikeCheckboxField() {
        if (shareAlike.isSelected()) {
            shareAlike.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXScheduleDetails class instance.
     */
    public EdXScheduleDetails verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
