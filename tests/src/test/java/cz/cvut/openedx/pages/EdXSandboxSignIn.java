package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSandboxSignIn {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "button.close-modal")
    @CacheLookup
    private WebElement close;

    @FindBy(css = "a[href='/courses']")
    @CacheLookup
    private WebElement courses;

    @FindBy(id = "email")
    @CacheLookup
    private WebElement email;

    @FindBy(id = "forgot-password-link")
    @CacheLookup
    private WebElement forgotPassword;

    @FindBy(id = "submit")
    @CacheLookup
    private WebElement logIntoMySandboxAccount;

    private final String pageLoadedText = "Please provide the following information to log into your Sandbox account";

    private final String pageUrl = "/login";

    @FindBy(id = "password")
    @CacheLookup
    private WebElement password;

    @FindBy(css = "a.cta.cta-register")
    @CacheLookup
    private WebElement registerNow;

    @FindBy(id = "remember-yes")
    @CacheLookup
    private WebElement rememberMe;

    @FindBy(id = "pwd_reset_button")
    @CacheLookup
    private WebElement resetMyPassword;

    @FindBy(css = "a.cta.cta-login")
    @CacheLookup
    private WebElement signIn;

    @FindBy(css = "a[href='/register']")
    @CacheLookup
    private WebElement signUpForSandboxToday;

    @FindBy(css = "a.nav-skip")
    @CacheLookup
    private WebElement skipToMainContent;

    @FindBy(css = "a[href='#tos']")
    @CacheLookup
    private WebElement terms;

    @FindBy(css = "a[href='/faq']")
    @CacheLookup
    private WebElement viewOurHelpSectionFor;

    @FindBy(id = "pwd_reset_email")
    @CacheLookup
    private WebElement yourEmailAddress;

    public EdXSandboxSignIn() {
    }

    public EdXSandboxSignIn(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSandboxSignIn(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSandboxSignIn(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Close Button.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickCloseButton() {
        close.click();
        return this;
    }

    /**
     * Click on Courses Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickCoursesLink() {
        courses.click();
        return this;
    }

    /**
     * Click on Forgot Password Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickForgotPasswordLink() {
        forgotPassword.click();
        return this;
    }

    /**
     * Click on Log Into My Sandbox Account Access My Courses Button.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickLogIntoMySandboxAccountButton() {
        logIntoMySandboxAccount.click();
        return this;
    }

    /**
     * Click on Register Now Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickRegisterNowLink() {
        registerNow.click();
        return this;
    }

    /**
     * Click on Reset My Password Button.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickResetMyPasswordButton() {
        resetMyPassword.click();
        return this;
    }

    /**
     * Click on Sign In Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickSignInLink() {
        signIn.click();
        return this;
    }

    /**
     * Click on Sign Up For Sandbox Today Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickSignUpForSandboxTodayLink() {
        signUpForSandboxToday.click();
        return this;
    }

    /**
     * Click on Skip To Main Content Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickSkipToMainContentLink() {
        skipToMainContent.click();
        return this;
    }

    /**
     * Click on Terms Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickTermsLink() {
        terms.click();
        return this;
    }

    /**
     * Click on View Our Help Section For Answers To Commonly Asked Questions. Link.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn clickViewOurHelpSectionForLink() {
        viewOurHelpSectionFor.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn fill() {
        setYourEmailAddressEmailField();
        setEmailEmailField();
        setPasswordPasswordField();
        setRememberMeCheckboxField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Email Email field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn setEmailEmailField() {
        return setEmailEmailField(data.get("EMAIL"));
    }

    /**
     * Set value to Email Email field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn setEmailEmailField(String emailValue) {
        email.sendKeys(emailValue);
        return this;
    }

    /**
     * Set default value to Password Password field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn setPasswordPasswordField() {
        return setPasswordPasswordField(data.get("PASSWORD"));
    }

    /**
     * Set value to Password Password field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn setPasswordPasswordField(String passwordValue) {
        password.sendKeys(passwordValue);
        return this;
    }

    /**
     * Set Remember Me Checkbox field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn setRememberMeCheckboxField() {
        if (!rememberMe.isSelected()) {
            rememberMe.click();
        }
        return this;
    }

    /**
     * Set default value to Your Email Address Email field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn setYourEmailAddressEmailField() {
        return setYourEmailAddressEmailField(data.get("YOUR_EMAIL_ADDRESS"));
    }

    /**
     * Set value to Your Email Address Email field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn setYourEmailAddressEmailField(String yourEmailAddressValue) {
        yourEmailAddress.sendKeys(yourEmailAddressValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn submit() {
        clickCloseButton();
        return this;
    }

    /**
     * Unset Remember Me Checkbox field.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn unsetRememberMeCheckboxField() {
        if (rememberMe.isSelected()) {
            rememberMe.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSandboxSignIn class instance.
     */
    public EdXSandboxSignIn verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
