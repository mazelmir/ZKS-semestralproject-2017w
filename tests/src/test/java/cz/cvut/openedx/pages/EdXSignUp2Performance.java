package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignUp2Performance {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#navigation-site-about a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el1;

    @FindBy(css = "#navigation-site-initiative a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el2;

    @FindBy(css = "#navigation-site-info a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el3;

    @FindBy(css = "#navigation-account a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el4;

    @FindBy(css = "a[href='#site-initiative']")
    @CacheLookup
    private WebElement aboutYourInitiative2;

    @FindBy(css = "button.js-kirigami-edit__save.kirigami-edit__save.modal-action.modal-close.btn.waves-effect.waves-light.blue")
    @CacheLookup
    private WebElement accept;

    @FindBy(css = "a[href='#account']")
    @CacheLookup
    private WebElement accountInfo4;

    @FindBy(css = "button.modal-action.modal-close.btn-flat.waves-effect.waves-light.red-text")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "a[href='https://www.edunext.co/support/']")
    @CacheLookup
    private WebElement contactSupport1;

    @FindBy(css = "button.btn.light-blue.lighten-1.waves-effect.white-text")
    @CacheLookup
    private WebElement contactSupport2;

    @FindBy(css = "a[href='#site-info']")
    @CacheLookup
    private WebElement micrositeSetup3;

    private final String pageLoadedText = "If you lost access to your microsite, or need to change the configuration, there is no need to create a new one, please contact us with your requirement and we’ll be able to help you get your microsite recovered or reconfigured";

    private final String pageUrl = "/embed/register/#site-initiative";

    @FindBy(css = "a[href='#site-about']")
    @CacheLookup
    private WebElement start1;

    public EdXSignUp2Performance() {
    }

    public EdXSignUp2Performance(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignUp2Performance(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignUp2Performance(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 2 About Your Initiative Link.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickAboutYourInitiativeLink2() {
        aboutYourInitiative2.click();
        return this;
    }

    /**
     * Click on Accept Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickAcceptButton() {
        accept.click();
        return this;
    }

    /**
     * Click on 4 Account Info Link.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickAccountInfoLink4() {
        accountInfo4.click();
        return this;
    }

    /**
     * Click on 1 Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickButton1() {
        el1.click();
        return this;
    }

    /**
     * Click on 2 Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickButton2() {
        el2.click();
        return this;
    }

    /**
     * Click on 3 Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickButton3() {
        el3.click();
        return this;
    }

    /**
     * Click on 4 Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickButton4() {
        el4.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Contact Support Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickContactSupport1Button() {
        contactSupport1.click();
        return this;
    }

    /**
     * Click on Contact Support Button.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickContactSupport2Button() {
        contactSupport2.click();
        return this;
    }

    /**
     * Click on 3 Microsite Setup Link.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickMicrositeSetupLink3() {
        micrositeSetup3.click();
        return this;
    }

    /**
     * Click on 1 Start Link.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance clickStartLink1() {
        start1.click();
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the EdXSignUp2Performance class instance.

    public EdXSignUp2Performance submit() {
        click1Button();
        return this;
    }*/

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSignUp2Performance class instance.
     */
    public EdXSignUp2Performance verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
