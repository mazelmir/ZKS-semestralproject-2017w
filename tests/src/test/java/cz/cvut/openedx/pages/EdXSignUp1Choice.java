package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignUp1Choice {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#navigation-site-about a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el1;

    @FindBy(css = "#navigation-site-initiative a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el2;

    @FindBy(css = "#navigation-site-info a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el3;

    @FindBy(css = "#navigation-account a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el4;

    @FindBy(css = "a[href='#site-initiative']")
    @CacheLookup
    private WebElement aboutYourInitiative2;

    @FindBy(css = "button.js-kirigami-edit__save.kirigami-edit__save.modal-action.modal-close.btn.waves-effect.waves-light.blue")
    @CacheLookup
    private WebElement accept;

    @FindBy(css = "a[href='#account']")
    @CacheLookup
    private WebElement accountInfo4;

    @FindBy(css = "button.modal-action.modal-close.btn-flat.waves-effect.waves-light.red-text")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "a[href='#site-info']")
    @CacheLookup
    private WebElement micrositeSetup3;

    @FindBy(css = "a[href='#site-iniative']")
    @CacheLookup
    private WebElement next1;

    @FindBy(id = "finish-site-about")
    @CacheLookup
    private WebElement next2;

    private final String pageLoadedText = "Please, follow the steps in each screen to sign up for an eduNEXT's open edX as a service subscription and deploy an open edX site for your online learning initiative";

    private final String pageUrl = "/embed/register/#site-about";

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) form div:nth-of-type(1) div:nth-of-type(2) div.select-goal__option.card.lite. div.card-content button.select-goal__option-button.btn-floating")
    @CacheLookup
    private WebElement radiobuttonuncheckedRadiobuttonchecked1;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) form div:nth-of-type(1) div:nth-of-type(3) div.select-goal__option.card.premium. div.card-content button.select-goal__option-button.btn-floating")
    @CacheLookup
    private WebElement radiobuttonuncheckedRadiobuttonchecked2;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) form div:nth-of-type(1) div:nth-of-type(4) div.select-goal__option.card.enterprise. div.card-content button.select-goal__option-button.btn-floating")
    @CacheLookup
    private WebElement radiobuttonuncheckedRadiobuttonchecked3;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) form div:nth-of-type(1) div:nth-of-type(5) div.select-goal__option.card.performance. div.card-content button.select-goal__option-button.btn-floating")
    @CacheLookup
    private WebElement radiobuttonuncheckedRadiobuttonchecked4;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) form div:nth-of-type(1) div:nth-of-type(6) div.select-goal__option.card.other. div.card-content button.select-goal__option-button.btn-floating")
    @CacheLookup
    private WebElement radiobuttonuncheckedRadiobuttonchecked5;

    @FindBy(css = "a[href='#site-about']")
    @CacheLookup
    private WebElement start1;

    public EdXSignUp1Choice() {
    }

    public EdXSignUp1Choice(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignUp1Choice(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignUp1Choice(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 2 About Your Initiative Link.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickAboutYourInitiativeLink2() {
        aboutYourInitiative2.click();
        return this;
    }

    /**
     * Click on Accept Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickAcceptButton() {
        accept.click();
        return this;
    }

    /**
     * Click on 4 Account Info Link.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickAccountInfoLink4() {
        accountInfo4.click();
        return this;
    }

    /**
     * Click on 1 Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickButton1() {
        el1.click();
        return this;
    }

    /**
     * Click on 2 Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickButton2() {
        el2.click();
        return this;
    }

    /**
     * Click on 3 Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickButton3() {
        el3.click();
        return this;
    }

    /**
     * Click on 4 Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickButton4() {
        el4.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on 3 Microsite Setup Link.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickMicrositeSetupLink3() {
        micrositeSetup3.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickNext1Button() {
        next1.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickNext2Button() {
        next2.click();
        return this;
    }

    /**
     * Click on Radiobuttonunchecked Radiobuttonchecked Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickRadiobuttonuncheckedRadiobuttonchecked1Button() {
        radiobuttonuncheckedRadiobuttonchecked1.click();
        return this;
    }

    /**
     * Click on Radiobuttonunchecked Radiobuttonchecked Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickRadiobuttonuncheckedRadiobuttonchecked2Button() {
        radiobuttonuncheckedRadiobuttonchecked2.click();
        return this;
    }

    /**
     * Click on Radiobuttonunchecked Radiobuttonchecked Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickRadiobuttonuncheckedRadiobuttonchecked3Button() {
        radiobuttonuncheckedRadiobuttonchecked3.click();
        return this;
    }

    /**
     * Click on Radiobuttonunchecked Radiobuttonchecked Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickRadiobuttonuncheckedRadiobuttonchecked4Button() {
        radiobuttonuncheckedRadiobuttonchecked4.click();
        return this;
    }

    /**
     * Click on Radiobuttonunchecked Radiobuttonchecked Button.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickRadiobuttonuncheckedRadiobuttonchecked5Button() {
        radiobuttonuncheckedRadiobuttonchecked5.click();
        return this;
    }

    /**
     * Click on 1 Start Link.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice clickStartLink1() {
        start1.click();
        return this;
    }

    /*
    /**
     * Submit the form to target page.
     *
     * @return the EdXSignUp1Choice class instance.

    public EdXSignUp1Choice submit() {
        click1Button();
        return this;
    } */

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSignUp1Choice class instance.
     */
    public EdXSignUp1Choice verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
