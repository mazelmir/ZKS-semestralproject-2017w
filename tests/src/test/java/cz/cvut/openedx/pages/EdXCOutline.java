package cz.cvut.openedx.pages;

import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXCOutline {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[href='/settings/advanced/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement advancedSettings;

    @FindBy(name = "cancel")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "a[href='/certificates/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement certificates;

    @FindBy(css = "a.button.button-toggle.button-toggle-expand-collapse.collapse-all")
    @CacheLookup
    private WebElement collapseAllSectionsExpandAll;

    @FindBy(css = "a.configure-button.action-button")
    @CacheLookup
    private WebElement configure;

    @FindBy(css = "a[href='/course_team/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement courseTeam;

    @FindBy(css = "a.duplicate-button.action-button")
    @CacheLookup
    private WebElement duplicate;

    @FindBy(css = "a.action-edit.action-inline.xblock-field-value-edit.incontext-editor-open-action")
    @CacheLookup
    private WebElement edit;

    @FindBy(css = "input.xblock-field-input.incontext-editor-input")
    @CacheLookup
    private WebElement editDisplayNameRequired;

    @FindBy(css = "a.edit-button.action-button")
    @CacheLookup
    private WebElement editStartDate;

    @FindBy(css = "a[title='This link will open in a new browser window/tab']")
    @CacheLookup
    private WebElement edunext;

    @FindBy(css = "a[title='Access documentation on http://docs.edx.org']")
    @CacheLookup
    private WebElement edxDocumentation;

    @FindBy(css = "a[href='https://www.edx.org/']")
    @CacheLookup
    private WebElement edxInc;

    @FindBy(css = "a[title='Enroll in edX101: Overview of Creating an edX Course']")
    @CacheLookup
    private WebElement enrollInEdx101;

    @FindBy(css = "a[title='Enroll in StudioX: Creating a Course with edX Studio']")
    @CacheLookup
    private WebElement enrollInStudiox;

    @FindBy(css = "a[href='/export/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement export;

    @FindBy(css = "a[href='/assets/course-v1:sandbox+Demo+01/']")
    @CacheLookup
    private WebElement filesUploads;

    @FindBy(css = "a[href='/settings/grading/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement grading;

    @FindBy(css = "a[href='/group_configurations/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement groupConfigurations;

    @FindBy(css = "a[title='Contextual Online Help']")
    @CacheLookup
    private WebElement help;

    @FindBy(css = "a[href='/import/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement importEl;

    @FindBy(css = "a[href='https://edx.readthedocs.io/projects/open-edx-building-and-running-a-course/en/latest/developing_course/controlling_content_visibility.html']")
    @CacheLookup
    private WebElement learnMoreAboutContentVisibilitySettings;

    @FindBy(css = "a[href='https://edx.readthedocs.io/projects/open-edx-building-and-running-a-course/en/latest/grading/index.html']")
    @CacheLookup
    private WebElement learnMoreAboutGradingPolicySettings;

    @FindBy(css = "a[href='https://edx.readthedocs.io/projects/open-edx-building-and-running-a-course/en/latest/developing_course/course_outline.html']")
    @CacheLookup
    private WebElement learnMoreAboutTheCourseOutline;

    @FindBy(css = "a.cta.cta-show-sock")
    @CacheLookup
    private WebElement lookingForHelpWithStudio;

    @FindBy(css = "a[title='Click to add a new section']")
    @CacheLookup
    private WebElement newSection1;

    @FindBy(css = "a[title='Click to add a new Section']")
    @CacheLookup
    private WebElement newSection2;

    @FindBy(css = "a[title='Click to add a new Unit']")
    @CacheLookup
    private WebElement newUnit;

    @FindBy(css = "a[title='Access the Open edX Portal']")
    @CacheLookup
    private WebElement openEdxPortal;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(1) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement outline;

    private final String pageLoadedText = "To hide the content of a subsection from learners after the subsection due date has passed, select the Configure icon for a subsection, then select";

    private final String pageUrl = "/course/course-v1:sandbox+Demo+01";

    @FindBy(css = "a[href='/tabs/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement pages;

    @FindBy(css = "a.button.button-reindex")
    @CacheLookup
    private WebElement reindex;

    @FindBy(css = "a.course-link")
    @CacheLookup
    private WebElement sandboxdemoEdxDemonstrationCourse;

    @FindBy(name = "submit")
    @CacheLookup
    private WebElement save;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement scheduleDetails;

    @FindBy(css = "a.action.action-signout")
    @CacheLookup
    private WebElement signOut;

    @FindBy(css = "a.nav-skip")
    @CacheLookup
    private WebElement skipToMainContent;

    @FindBy(css = "#view-top header.primary div:nth-of-type(2) nav.nav-account.nav-is-signedin.nav-dd.ui-right ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement studioHome;

    @FindBy(css = "a[href='/textbooks/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement textbooks;

    @FindBy(css = "a[href='/course_info/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement updates;

    @FindBy(css = "a.button.view-button.view-live-button")
    @CacheLookup
    private WebElement viewLive;

    public EdXCOutline() {
    }

    public EdXCOutline(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXCOutline(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXCOutline(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Advanced Settings Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickAdvancedSettingsLink() {
        advancedSettings.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Certificates Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickCertificatesLink() {
        certificates.click();
        return this;
    }

    /**
     * Click on Collapse All Sections Expand All Sections Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickCollapseAllSectionsExpandAllLink() {
        collapseAllSectionsExpandAll.click();
        return this;
    }

    /**
     * Click on Configure Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickConfigureLink() {
        configure.click();
        return this;
    }

    /**
     * Click on Course Team Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickCourseTeamLink() {
        courseTeam.click();
        return this;
    }

    /**
     * Click on Duplicate Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickDuplicateLink() {
        duplicate.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickEditLink() {
        edit.click();
        return this;
    }

    /**
     * Click on Edit Start Date Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickEditStartDateLink() {
        editStartDate.click();
        return this;
    }

    /**
     * Click on Edunext Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickEdunextLink() {
        edunext.click();
        return this;
    }

    /**
     * Click on Edx Documentation Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickEdxDocumentationLink() {
        edxDocumentation.click();
        return this;
    }

    /**
     * Click on Edx Inc. Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickEdxIncLink() {
        edxInc.click();
        return this;
    }

    /**
     * Click on Enroll In Edx101 Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickEnrollInEdx101Link() {
        enrollInEdx101.click();
        return this;
    }

    /**
     * Click on Enroll In Studiox Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickEnrollInStudioxLink() {
        enrollInStudiox.click();
        return this;
    }

    /**
     * Click on Export Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickExportLink() {
        export.click();
        return this;
    }

    /**
     * Click on Files Uploads Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickFilesUploadsLink() {
        filesUploads.click();
        return this;
    }

    /**
     * Click on Grading Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickGradingLink() {
        grading.click();
        return this;
    }

    /**
     * Click on Group Configurations Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickGroupConfigurationsLink() {
        groupConfigurations.click();
        return this;
    }

    /**
     * Click on Help Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickHelpLink() {
        help.click();
        return this;
    }

    /**
     * Click on Import Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickImportLink() {
        importEl.click();
        return this;
    }

    /**
     * Click on Learn More About Content Visibility Settings Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickLearnMoreAboutContentVisibilitySettingsLink() {
        learnMoreAboutContentVisibilitySettings.click();
        return this;
    }

    /**
     * Click on Learn More About Grading Policy Settings Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickLearnMoreAboutGradingPolicySettingsLink() {
        learnMoreAboutGradingPolicySettings.click();
        return this;
    }

    /**
     * Click on Learn More About The Course Outline Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickLearnMoreAboutTheCourseOutlineLink() {
        learnMoreAboutTheCourseOutline.click();
        return this;
    }

    /**
     * Click on Looking For Help With Studio Hide Studio Help Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickLookingForHelpWithStudioLink() {
        lookingForHelpWithStudio.click();
        return this;
    }

    /**
     * Click on New Section Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickNewSection1Link() {
        newSection1.click();
        return this;
    }

    /**
     * Click on New Section Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickNewSection2Link() {
        newSection2.click();
        return this;
    }

    /**
     * Click on New Unit Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickNewUnitLink() {
        newUnit.click();
        return this;
    }

    /**
     * Click on Open Edx Portal Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickOpenEdxPortalLink() {
        openEdxPortal.click();
        return this;
    }

    /**
     * Click on Outline Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickOutlineLink() {
        outline.click();
        return this;
    }

    /**
     * Click on Pages Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickPagesLink() {
        pages.click();
        return this;
    }

    /**
     * Click on Reindex Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickReindexLink() {
        reindex.click();
        return this;
    }

    /**
     * Click on Sandboxdemo Edx Demonstration Course Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickSandboxdemoEdxDemonstrationCourseLink() {
        sandboxdemoEdxDemonstrationCourse.click();
        return this;
    }

    /**
     * Click on Save Button.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickSaveButton() {
        save.click();
        return this;
    }

    /**
     * Click on Schedule Details Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickScheduleDetailsLink() {
        scheduleDetails.click();
        return this;
    }

    /**
     * Click on Sign Out Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickSignOutLink() {
        signOut.click();
        return this;
    }

    /**
     * Click on Skip To Main Content Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickSkipToMainContentLink() {
        skipToMainContent.click();
        return this;
    }

    /**
     * Click on Studio Home Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickStudioHomeLink() {
        studioHome.click();
        return this;
    }

    /**
     * Click on Textbooks Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickTextbooksLink() {
        textbooks.click();
        return this;
    }

    /**
     * Click on Updates Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickUpdatesLink() {
        updates.click();
        return this;
    }

    /**
     * Click on View Live Link.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline clickViewLiveLink() {
        viewLive.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline fill() {
        setEditDisplayNameRequiredTextField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Edit Display Name Required Text field.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline setEditDisplayNameRequiredTextField() {
        return setEditDisplayNameRequiredTextField(data.get("EDIT_DISPLAY_NAME_REQUIRED"));
    }

    /**
     * Set value to Edit Display Name Required Text field.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline setEditDisplayNameRequiredTextField(String editDisplayNameRequiredValue) {
        editDisplayNameRequired.sendKeys(editDisplayNameRequiredValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline submit() {
        clickSaveButton();
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the test.EdXCOutline class instance.
     */
    public EdXCOutline verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
