package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignUp2Other {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#navigation-site-about a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el1;

    @FindBy(css = "#navigation-site-initiative a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el2;

    @FindBy(css = "#navigation-site-info a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el3;

    @FindBy(css = "#navigation-account a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el4;

    @FindBy(css = "a[href='#site-initiative']")
    @CacheLookup
    private WebElement aboutYourInitiative2;

    @FindBy(css = "button.js-kirigami-edit__save.kirigami-edit__save.modal-action.modal-close.btn.waves-effect.waves-light.blue")
    @CacheLookup
    private WebElement accept;

    @FindBy(css = "a[href='#account']")
    @CacheLookup
    private WebElement accountInfo4;

    @FindBy(css = "button.modal-action.modal-close.btn-flat.waves-effect.waves-light.red-text")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "#navigation-site-info a")
    @CacheLookup
    private WebElement micrositeSetup3;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.other-goal div.col.s12 div.row div.col.s12.right-align a")
    @CacheLookup
    private WebElement next1;

    @FindBy(id = "finish-site-info")
    @CacheLookup
    private WebElement next2;

    private final String pageLoadedText = "Please, tell us about your online learning initiative";

    private final String pageUrl = "/embed/register/#site-initiative";

    @FindBy(id = "primary_goal_other")
    @CacheLookup
    private WebElement pleaseTellUsAboutYourGoals;

    @FindBy(css = "a[href='#site-about']")
    @CacheLookup
    private WebElement start1;

    public EdXSignUp2Other() {
    }

    public EdXSignUp2Other(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignUp2Other(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignUp2Other(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 2 About Your Initiative Link.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickAboutYourInitiativeLink2() {
        aboutYourInitiative2.click();
        return this;
    }

    /**
     * Click on Accept Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickAcceptButton() {
        accept.click();
        return this;
    }

    /**
     * Click on 4 Account Info Link.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickAccountInfoLink4() {
        accountInfo4.click();
        return this;
    }

    /**
     * Click on 1 Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickButton1() {
        el1.click();
        return this;
    }

    /**
     * Click on 2 Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickButton2() {
        el2.click();
        return this;
    }

    /**
     * Click on 3 Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickButton3() {
        el3.click();
        return this;
    }

    /**
     * Click on 4 Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickButton4() {
        el4.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on 3 Microsite Setup Link.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickMicrositeSetupLink3() {
        micrositeSetup3.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickNext1Button() {
        next1.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickNext2Button() {
        next2.click();
        return this;
    }

    /**
     * Click on 1 Start Link.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other clickStartLink1() {
        start1.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other fill() {
        setPleaseTellUsAboutYourGoalsTextField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EdXSignUp2Other class instance.

    public EdXSignUp2Other fillAndSubmit() {
        fill();
        return submit();
    }
 */
    /**
     * Set default value to Please Tell Us About Your Goals With The Open Edx As A Service Subscription Text field.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other setPleaseTellUsAboutYourGoalsTextField() {
        return setPleaseTellUsAboutYourGoalsTextField(data.get("PLEASE_TELL_US_ABOUT_YOUR_GOALS"));
    }

    /**
     * Set value to Please Tell Us About Your Goals With The Open Edx As A Service Subscription Text field.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other setPleaseTellUsAboutYourGoalsTextField(String pleaseTellUsAboutYourGoalsValue) {
        pleaseTellUsAboutYourGoals.sendKeys(pleaseTellUsAboutYourGoalsValue);
        return this;
    }

    /*
     * Submit the form to target page.
     *
     * @return the EdXSignUp2Other class instance.

    public EdXSignUp2Other submit() {
        click1Button();
        return this;
    } */

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSignUp2Other class instance.
     */
    public EdXSignUp2Other verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
