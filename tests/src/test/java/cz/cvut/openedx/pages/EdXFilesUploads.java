package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cz.cvut.openedx.helpers.ClickHelper;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXFilesUploads {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[href='/settings/advanced/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement advancedSettings;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_Al2O3.png']")
    @CacheLookup
    private WebElement al2o3Png;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_4B0D-H.pdb']")
    @CacheLookup
    private WebElement b0dhPdb4;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_benzene.c3xml']")
    @CacheLookup
    private WebElement benzeneC3xml;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_c6h6.smol']")
    @CacheLookup
    private WebElement c6h6Smol;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_c.spt']")
    @CacheLookup
    private WebElement cSpt;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_caffeine.jvxl']")
    @CacheLookup
    private WebElement caffeineJvxl;

    @FindBy(css = "a[href='/certificates/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement certificates;

    @FindBy(css = "a.choose-file-button")
    @CacheLookup
    private WebElement chooseFile;

    @FindBy(css = "a.close-button")
    @CacheLookup
    private WebElement close;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_co2.smol']")
    @CacheLookup
    private WebElement co2Smol;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@css_collapsiblehdr.css']")
    @CacheLookup
    private WebElement collapsiblehdrCss;

    @FindBy(css = "a[href='/course_team/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement courseTeam;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_1crn.pdb']")
    @CacheLookup
    private WebElement crnPdb1;

    @FindBy(css = "#asset-table-body tr:nth-of-type(1) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset1;

    @FindBy(css = "#asset-table-body tr:nth-of-type(10) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset10;

    @FindBy(css = "#asset-table-body tr:nth-of-type(11) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset11;

    @FindBy(css = "#asset-table-body tr:nth-of-type(12) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset12;

    @FindBy(css = "#asset-table-body tr:nth-of-type(13) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset13;

    @FindBy(css = "#asset-table-body tr:nth-of-type(14) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset14;

    @FindBy(css = "#asset-table-body tr:nth-of-type(15) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset15;

    @FindBy(css = "#asset-table-body tr:nth-of-type(16) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset16;

    @FindBy(css = "#asset-table-body tr:nth-of-type(17) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset17;

    @FindBy(css = "#asset-table-body tr:nth-of-type(18) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset18;

    @FindBy(css = "#asset-table-body tr:nth-of-type(19) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset19;

    @FindBy(css = "#asset-table-body tr:nth-of-type(2) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset2;

    @FindBy(css = "#asset-table-body tr:nth-of-type(20) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset20;

    @FindBy(css = "#asset-table-body tr:nth-of-type(21) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset21;

    @FindBy(css = "#asset-table-body tr:nth-of-type(22) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset22;

    @FindBy(css = "#asset-table-body tr:nth-of-type(23) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset23;

    @FindBy(css = "#asset-table-body tr:nth-of-type(24) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset24;

    @FindBy(css = "#asset-table-body tr:nth-of-type(25) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset25;

    @FindBy(css = "#asset-table-body tr:nth-of-type(26) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset26;

    @FindBy(css = "#asset-table-body tr:nth-of-type(27) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset27;

    @FindBy(css = "#asset-table-body tr:nth-of-type(28) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset28;

    @FindBy(css = "#asset-table-body tr:nth-of-type(29) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset29;

    @FindBy(css = "#asset-table-body tr:nth-of-type(3) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset3;

    @FindBy(css = "#asset-table-body tr:nth-of-type(30) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset30;

    @FindBy(css = "#asset-table-body tr:nth-of-type(31) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset31;

    @FindBy(css = "#asset-table-body tr:nth-of-type(32) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset32;

    @FindBy(css = "#asset-table-body tr:nth-of-type(33) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset33;

    @FindBy(css = "#asset-table-body tr:nth-of-type(34) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset34;

    @FindBy(css = "#asset-table-body tr:nth-of-type(35) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset35;

    @FindBy(css = "#asset-table-body tr:nth-of-type(36) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset36;

    @FindBy(css = "#asset-table-body tr:nth-of-type(37) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset37;

    @FindBy(css = "#asset-table-body tr:nth-of-type(38) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset38;

    @FindBy(css = "#asset-table-body tr:nth-of-type(39) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset39;

    @FindBy(css = "#asset-table-body tr:nth-of-type(4) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset4;

    @FindBy(css = "#asset-table-body tr:nth-of-type(40) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset40;

    @FindBy(css = "#asset-table-body tr:nth-of-type(41) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset41;

    @FindBy(css = "#asset-table-body tr:nth-of-type(42) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset42;

    @FindBy(css = "#asset-table-body tr:nth-of-type(43) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset43;

    @FindBy(css = "#asset-table-body tr:nth-of-type(44) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset44;

    @FindBy(css = "#asset-table-body tr:nth-of-type(45) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset45;

    @FindBy(css = "#asset-table-body tr:nth-of-type(46) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset46;

    @FindBy(css = "#asset-table-body tr:nth-of-type(47) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset47;

    @FindBy(css = "#asset-table-body tr:nth-of-type(48) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset48;

    @FindBy(css = "#asset-table-body tr:nth-of-type(49) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset49;

    @FindBy(css = "#asset-table-body tr:nth-of-type(5) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset5;

    @FindBy(css = "#asset-table-body tr:nth-of-type(50) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset50;

    @FindBy(css = "#asset-table-body tr:nth-of-type(6) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset6;

    @FindBy(css = "#asset-table-body tr:nth-of-type(7) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset7;

    @FindBy(css = "#asset-table-body tr:nth-of-type(8) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset8;

    @FindBy(css = "#asset-table-body tr:nth-of-type(9) td:nth-of-type(6) ul.actions-list li:nth-of-type(1) a.remove-asset-button.action-button")
    @CacheLookup
    private WebElement deleteThisAsset9;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@molecules_dopamine.mol']")
    @CacheLookup
    private WebElement dopamineMol;

    @FindBy(css = "a[title='This link will open in a new browser window/tab']")
    @CacheLookup
    private WebElement edunext;

    @FindBy(css = "a[title='Access documentation on http://docs.edx.org']")
    @CacheLookup
    private WebElement edxDocumentation;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_IGV_js_eDX.html']")
    @CacheLookup
    private WebElement edxHtml1;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_IGV_eDX.html']")
    @CacheLookup
    private WebElement edxHtml2;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_IGV_js_eDX.html-e']")
    @CacheLookup
    private WebElement edxHtmle1;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_IGV_eDX.html-e']")
    @CacheLookup
    private WebElement edxHtmle2;

    @FindBy(css = "a[href='https://www.edx.org/']")
    @CacheLookup
    private WebElement edxInc;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_IGV_js_edx.js']")
    @CacheLookup
    private WebElement edxJs;

    @FindBy(css = "a[title='Enroll in edX101: Overview of Creating an edX Course']")
    @CacheLookup
    private WebElement enrollInEdx101;

    @FindBy(css = "a[title='Enroll in StudioX: Creating a Course with edX Studio']")
    @CacheLookup
    private WebElement enrollInStudiox;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_estron.cml']")
    @CacheLookup
    private WebElement estronCml;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_estron.cml-e']")
    @CacheLookup
    private WebElement estronCmle;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@applets_ExploreGene.jar']")
    @CacheLookup
    private WebElement exploregeneJar;

    @FindBy(css = "a[href='/export/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement export;

    @FindBy(css = "a[href='/assets/course-v1:sandbox+Demo+01/']")
    @CacheLookup
    private WebElement filesUploads;

    @FindBy(css = "a[href='/settings/grading/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement grading;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-ar.js']")
    @CacheLookup
    private WebElement gridLocalearJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-bg1251.js']")
    @CacheLookup
    private WebElement gridLocalebg1251Js;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-fr.js']")
    @CacheLookup
    private WebElement gridLocalefrJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-hr1250.js']")
    @CacheLookup
    private WebElement gridLocalehr1250Js;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-is.js']")
    @CacheLookup
    private WebElement gridLocaleisJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-ja.js']")
    @CacheLookup
    private WebElement gridLocalejaJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-nl.js']")
    @CacheLookup
    private WebElement gridLocalenlJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-ro.js']")
    @CacheLookup
    private WebElement gridLocaleroJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_i18n_grid.locale-sr.js']")
    @CacheLookup
    private WebElement gridLocalesrJs;

    @FindBy(css = "a[href='/group_configurations/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement groupConfigurations;

    @FindBy(css = "a[title='Contextual Online Help']")
    @CacheLookup
    private WebElement help;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_1hxw.png']")
    @CacheLookup
    private WebElement hxwPng1;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_1hxw.pngj']")
    @CacheLookup
    private WebElement hxwPngj1;

    @FindBy(css = "a[href='/import/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement limport;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@VGL_index.html']")
    @CacheLookup
    private WebElement indexHtml;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@VGL_index.html-e']")
    @CacheLookup
    private WebElement indexHtmle;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@js_javacheck.js']")
    @CacheLookup
    private WebElement javacheckJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_JSmol.js']")
    @CacheLookup
    private WebElement jsmolJs;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_jsmol.php']")
    @CacheLookup
    private WebElement jsmolPhp;

    @FindBy(css = "a.button.external-help-button")
    @CacheLookup
    private WebElement learnMoreAboutManagingFiles;

    @FindBy(id = "lock_asset_61")
    @CacheLookup
    private WebElement lockThisAsset1;

    @FindBy(id = "lock_asset_97")
    @CacheLookup
    private WebElement lockThisAsset10;

    @FindBy(id = "lock_asset_101")
    @CacheLookup
    private WebElement lockThisAsset11;

    @FindBy(id = "lock_asset_105")
    @CacheLookup
    private WebElement lockThisAsset12;

    @FindBy(id = "lock_asset_109")
    @CacheLookup
    private WebElement lockThisAsset13;

    @FindBy(id = "lock_asset_113")
    @CacheLookup
    private WebElement lockThisAsset14;

    @FindBy(id = "lock_asset_117")
    @CacheLookup
    private WebElement lockThisAsset15;

    @FindBy(id = "lock_asset_121")
    @CacheLookup
    private WebElement lockThisAsset16;

    @FindBy(id = "lock_asset_125")
    @CacheLookup
    private WebElement lockThisAsset17;

    @FindBy(id = "lock_asset_129")
    @CacheLookup
    private WebElement lockThisAsset18;

    @FindBy(id = "lock_asset_133")
    @CacheLookup
    private WebElement lockThisAsset19;

    @FindBy(id = "lock_asset_65")
    @CacheLookup
    private WebElement lockThisAsset2;

    @FindBy(id = "lock_asset_137")
    @CacheLookup
    private WebElement lockThisAsset20;

    @FindBy(id = "lock_asset_141")
    @CacheLookup
    private WebElement lockThisAsset21;

    @FindBy(id = "lock_asset_145")
    @CacheLookup
    private WebElement lockThisAsset22;

    @FindBy(id = "lock_asset_149")
    @CacheLookup
    private WebElement lockThisAsset23;

    @FindBy(id = "lock_asset_153")
    @CacheLookup
    private WebElement lockThisAsset24;

    @FindBy(id = "lock_asset_157")
    @CacheLookup
    private WebElement lockThisAsset25;

    @FindBy(id = "lock_asset_161")
    @CacheLookup
    private WebElement lockThisAsset26;

    @FindBy(id = "lock_asset_165")
    @CacheLookup
    private WebElement lockThisAsset27;

    @FindBy(id = "lock_asset_169")
    @CacheLookup
    private WebElement lockThisAsset28;

    @FindBy(id = "lock_asset_173")
    @CacheLookup
    private WebElement lockThisAsset29;

    @FindBy(id = "lock_asset_69")
    @CacheLookup
    private WebElement lockThisAsset3;

    @FindBy(id = "lock_asset_177")
    @CacheLookup
    private WebElement lockThisAsset30;

    @FindBy(id = "lock_asset_181")
    @CacheLookup
    private WebElement lockThisAsset31;

    @FindBy(id = "lock_asset_185")
    @CacheLookup
    private WebElement lockThisAsset32;

    @FindBy(id = "lock_asset_189")
    @CacheLookup
    private WebElement lockThisAsset33;

    @FindBy(id = "lock_asset_193")
    @CacheLookup
    private WebElement lockThisAsset34;

    @FindBy(id = "lock_asset_197")
    @CacheLookup
    private WebElement lockThisAsset35;

    @FindBy(id = "lock_asset_201")
    @CacheLookup
    private WebElement lockThisAsset36;

    @FindBy(id = "lock_asset_205")
    @CacheLookup
    private WebElement lockThisAsset37;

    @FindBy(id = "lock_asset_209")
    @CacheLookup
    private WebElement lockThisAsset38;

    @FindBy(id = "lock_asset_213")
    @CacheLookup
    private WebElement lockThisAsset39;

    @FindBy(id = "lock_asset_73")
    @CacheLookup
    private WebElement lockThisAsset4;

    @FindBy(id = "lock_asset_217")
    @CacheLookup
    private WebElement lockThisAsset40;

    @FindBy(id = "lock_asset_221")
    @CacheLookup
    private WebElement lockThisAsset41;

    @FindBy(id = "lock_asset_225")
    @CacheLookup
    private WebElement lockThisAsset42;

    @FindBy(id = "lock_asset_229")
    @CacheLookup
    private WebElement lockThisAsset43;

    @FindBy(id = "lock_asset_233")
    @CacheLookup
    private WebElement lockThisAsset44;

    @FindBy(id = "lock_asset_237")
    @CacheLookup
    private WebElement lockThisAsset45;

    @FindBy(id = "lock_asset_241")
    @CacheLookup
    private WebElement lockThisAsset46;

    @FindBy(id = "lock_asset_245")
    @CacheLookup
    private WebElement lockThisAsset47;

    @FindBy(id = "lock_asset_249")
    @CacheLookup
    private WebElement lockThisAsset48;

    @FindBy(id = "lock_asset_253")
    @CacheLookup
    private WebElement lockThisAsset49;

    @FindBy(id = "lock_asset_77")
    @CacheLookup
    private WebElement lockThisAsset5;

    @FindBy(id = "lock_asset_257")
    @CacheLookup
    private WebElement lockThisAsset50;

    @FindBy(id = "lock_asset_81")
    @CacheLookup
    private WebElement lockThisAsset6;

    @FindBy(id = "lock_asset_85")
    @CacheLookup
    private WebElement lockThisAsset7;

    @FindBy(id = "lock_asset_89")
    @CacheLookup
    private WebElement lockThisAsset8;

    @FindBy(id = "lock_asset_93")
    @CacheLookup
    private WebElement lockThisAsset9;

    @FindBy(css = "a.cta.cta-show-sock")
    @CacheLookup
    private WebElement lookingForHelpWithStudio;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_lyso-H.pdb']")
    @CacheLookup
    private WebElement lysohPdb;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_1m19.pdb.gz']")
    @CacheLookup
    private WebElement m19PdbGz1;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@VGL_make']")
    @CacheLookup
    private WebElement make;

    @FindBy(css = "a.nav-link.next-page-link")
    @CacheLookup
    private WebElement next1;

    @FindBy(css = "button.nav-link.next-page-link")
    @CacheLookup
    private WebElement next2;

    @FindBy(css = "a[title='Access the Open edX Portal']")
    @CacheLookup
    private WebElement openEdxPortal;

    @FindBy(css = "#js-asset-type-col div.wrapper-nav-sub div.nav-sub ul li:nth-of-type(2) a.column-filter-link")
    @CacheLookup
    private WebElement other;

    @FindBy(css = "#view-top header.primary div:nth-of-type(1) nav.nav-course.nav-dd.ui-left ol li:nth-of-type(1) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement outline;

    private final String pageLoadedText = "The course image, textbook chapters, and files that appear on your Course Handouts sidebar also appear in this list";

    @FindBy(id = "page-number-input")
    @CacheLookup
    private WebElement pageNumberOutOf23;

    private final String pageUrl = "/assets/course-v1:sandbox+Demo+01/";

    @FindBy(css = "a[href='/tabs/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement pages;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@css_periodic-table.css']")
    @CacheLookup
    private WebElement periodictableCss;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@css_periodic-table-colors.css']")
    @CacheLookup
    private WebElement periodictablecolorsCss;

    @FindBy(css = "a.nav-link.previous-page-link.is-disabled")
    @CacheLookup
    private WebElement previous1;

    @FindBy(css = "button.nav-link.previous-page-link.is-disabled")
    @CacheLookup
    private WebElement previous2;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_README.TXT']")
    @CacheLookup
    private WebElement readmeTxt;

    @FindBy(css = "a.course-link")
    @CacheLookup
    private WebElement sandboxdemoEdxDemonstrationCourse;

    @FindBy(css = "a[href='/settings/details/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement scheduleDetails;

    @FindBy(css = "#js-asset-type-col div.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a.column-filter-link")
    @CacheLookup
    private WebElement showAll;

    @FindBy(css = "a.action.action-signout")
    @CacheLookup
    private WebElement signOut;

    @FindBy(css = "a.nav-skip")
    @CacheLookup
    private WebElement skipToMainContent;

    @FindBy(css = "#asset-table-body tr:nth-of-type(1) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio1;

    @FindBy(css = "#asset-table-body tr:nth-of-type(10) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio10;

    @FindBy(css = "#asset-table-body tr:nth-of-type(11) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio11;

    @FindBy(css = "#asset-table-body tr:nth-of-type(12) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio12;

    @FindBy(css = "#asset-table-body tr:nth-of-type(13) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio13;

    @FindBy(css = "#asset-table-body tr:nth-of-type(14) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio14;

    @FindBy(css = "#asset-table-body tr:nth-of-type(15) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio15;

    @FindBy(css = "#asset-table-body tr:nth-of-type(16) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio16;

    @FindBy(css = "#asset-table-body tr:nth-of-type(17) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio17;

    @FindBy(css = "#asset-table-body tr:nth-of-type(18) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio18;

    @FindBy(css = "#asset-table-body tr:nth-of-type(19) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio19;

    @FindBy(css = "#asset-table-body tr:nth-of-type(2) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio2;

    @FindBy(css = "#asset-table-body tr:nth-of-type(20) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio20;

    @FindBy(css = "#asset-table-body tr:nth-of-type(21) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio21;

    @FindBy(css = "#asset-table-body tr:nth-of-type(22) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio22;

    @FindBy(css = "#asset-table-body tr:nth-of-type(23) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio23;

    @FindBy(css = "#asset-table-body tr:nth-of-type(24) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio24;

    @FindBy(css = "#asset-table-body tr:nth-of-type(25) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio25;

    @FindBy(css = "#asset-table-body tr:nth-of-type(26) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio26;

    @FindBy(css = "#asset-table-body tr:nth-of-type(27) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio27;

    @FindBy(css = "#asset-table-body tr:nth-of-type(28) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio28;

    @FindBy(css = "#asset-table-body tr:nth-of-type(29) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio29;

    @FindBy(css = "#asset-table-body tr:nth-of-type(3) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio3;

    @FindBy(css = "#asset-table-body tr:nth-of-type(30) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio30;

    @FindBy(css = "#asset-table-body tr:nth-of-type(31) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio31;

    @FindBy(css = "#asset-table-body tr:nth-of-type(32) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio32;

    @FindBy(css = "#asset-table-body tr:nth-of-type(33) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio33;

    @FindBy(css = "#asset-table-body tr:nth-of-type(34) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio34;

    @FindBy(css = "#asset-table-body tr:nth-of-type(35) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio35;

    @FindBy(css = "#asset-table-body tr:nth-of-type(36) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio36;

    @FindBy(css = "#asset-table-body tr:nth-of-type(37) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio37;

    @FindBy(css = "#asset-table-body tr:nth-of-type(38) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio38;

    @FindBy(css = "#asset-table-body tr:nth-of-type(39) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio39;

    @FindBy(css = "#asset-table-body tr:nth-of-type(4) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio4;

    @FindBy(css = "#asset-table-body tr:nth-of-type(40) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio40;

    @FindBy(css = "#asset-table-body tr:nth-of-type(41) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio41;

    @FindBy(css = "#asset-table-body tr:nth-of-type(42) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio42;

    @FindBy(css = "#asset-table-body tr:nth-of-type(43) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio43;

    @FindBy(css = "#asset-table-body tr:nth-of-type(44) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio44;

    @FindBy(css = "#asset-table-body tr:nth-of-type(45) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio45;

    @FindBy(css = "#asset-table-body tr:nth-of-type(46) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio46;

    @FindBy(css = "#asset-table-body tr:nth-of-type(47) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio47;

    @FindBy(css = "#asset-table-body tr:nth-of-type(48) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio48;

    @FindBy(css = "#asset-table-body tr:nth-of-type(49) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio49;

    @FindBy(css = "#asset-table-body tr:nth-of-type(5) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio5;

    @FindBy(css = "#asset-table-body tr:nth-of-type(50) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio50;

    @FindBy(css = "#asset-table-body tr:nth-of-type(6) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio6;

    @FindBy(css = "#asset-table-body tr:nth-of-type(7) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio7;

    @FindBy(css = "#asset-table-body tr:nth-of-type(8) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio8;

    @FindBy(css = "#asset-table-body tr:nth-of-type(9) td:nth-of-type(5) ul li:nth-of-type(1) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement studio9;

    @FindBy(css = "#view-top header.primary div:nth-of-type(2) nav.nav-account.nav-is-signedin.nav-dd.ui-right ol li:nth-of-type(2) div.wrapper.wrapper-nav-sub div.nav-sub ul li:nth-of-type(1) a")
    @CacheLookup
    private WebElement studioHome;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@subs_AEwvgztBf44.srt.sjson']")
    @CacheLookup
    private WebElement subsaewvgztbf44SrtSjson;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_t2.spt']")
    @CacheLookup
    private WebElement t2Spt;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_test3.htm']")
    @CacheLookup
    private WebElement test3Htm;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_test.molden']")
    @CacheLookup
    private WebElement testMolden;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_test.spt']")
    @CacheLookup
    private WebElement testSpt;

    @FindBy(css = "a[href='/textbooks/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement textbooks;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@css_ui.jqgrid.css']")
    @CacheLookup
    private WebElement uiJqgridCss;

    @FindBy(css = "a[href='/course_info/course-v1:sandbox+Demo+01']")
    @CacheLookup
    private WebElement updates;

    @FindBy(css = "#content div:nth-of-type(1) header.mast.has-actions.has-subtitle nav.nav-actions ul li.nav-item a.button.upload-button.new-button")
    @CacheLookup
    private WebElement uploadNewFile;

    @FindBy(css = "#content div:nth-of-type(2) section.content article.content-primary div.wrapper-assets div:nth-of-type(2) p a.button.new-button.upload-button")
    @CacheLookup
    private WebElement uploadYourFirstAsset;

    @FindBy(css = "#content div:nth-of-type(3) div.modal-body div:nth-of-type(2) input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement url1;

    @FindBy(name = "file")
    @CacheLookup
    private WebElement url2;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@VGL_VGL-jnslp-for-sandbox.tgz']")
    @CacheLookup
    private WebElement vgljnslpforsandboxTgz;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@jsmol_vib.xml']")
    @CacheLookup
    private WebElement vibXml;

    @FindBy(css = "#asset-table-body tr:nth-of-type(1) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web1;

    @FindBy(css = "#asset-table-body tr:nth-of-type(10) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web10;

    @FindBy(css = "#asset-table-body tr:nth-of-type(11) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web11;

    @FindBy(css = "#asset-table-body tr:nth-of-type(12) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web12;

    @FindBy(css = "#asset-table-body tr:nth-of-type(13) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web13;

    @FindBy(css = "#asset-table-body tr:nth-of-type(14) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web14;

    @FindBy(css = "#asset-table-body tr:nth-of-type(15) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web15;

    @FindBy(css = "#asset-table-body tr:nth-of-type(16) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web16;

    @FindBy(css = "#asset-table-body tr:nth-of-type(17) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web17;

    @FindBy(css = "#asset-table-body tr:nth-of-type(18) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web18;

    @FindBy(css = "#asset-table-body tr:nth-of-type(19) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web19;

    @FindBy(css = "#asset-table-body tr:nth-of-type(2) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web2;

    @FindBy(css = "#asset-table-body tr:nth-of-type(20) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web20;

    @FindBy(css = "#asset-table-body tr:nth-of-type(21) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web21;

    @FindBy(css = "#asset-table-body tr:nth-of-type(22) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web22;

    @FindBy(css = "#asset-table-body tr:nth-of-type(23) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web23;

    @FindBy(css = "#asset-table-body tr:nth-of-type(24) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web24;

    @FindBy(css = "#asset-table-body tr:nth-of-type(25) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web25;

    @FindBy(css = "#asset-table-body tr:nth-of-type(26) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web26;

    @FindBy(css = "#asset-table-body tr:nth-of-type(27) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web27;

    @FindBy(css = "#asset-table-body tr:nth-of-type(28) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web28;

    @FindBy(css = "#asset-table-body tr:nth-of-type(29) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web29;

    @FindBy(css = "#asset-table-body tr:nth-of-type(3) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web3;

    @FindBy(css = "#asset-table-body tr:nth-of-type(30) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web30;

    @FindBy(css = "#asset-table-body tr:nth-of-type(31) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web31;

    @FindBy(css = "#asset-table-body tr:nth-of-type(32) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web32;

    @FindBy(css = "#asset-table-body tr:nth-of-type(33) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web33;

    @FindBy(css = "#asset-table-body tr:nth-of-type(34) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web34;

    @FindBy(css = "#asset-table-body tr:nth-of-type(35) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web35;

    @FindBy(css = "#asset-table-body tr:nth-of-type(36) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web36;

    @FindBy(css = "#asset-table-body tr:nth-of-type(37) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web37;

    @FindBy(css = "#asset-table-body tr:nth-of-type(38) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web38;

    @FindBy(css = "#asset-table-body tr:nth-of-type(39) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web39;

    @FindBy(css = "#asset-table-body tr:nth-of-type(4) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web4;

    @FindBy(css = "#asset-table-body tr:nth-of-type(40) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web40;

    @FindBy(css = "#asset-table-body tr:nth-of-type(41) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web41;

    @FindBy(css = "#asset-table-body tr:nth-of-type(42) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web42;

    @FindBy(css = "#asset-table-body tr:nth-of-type(43) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web43;

    @FindBy(css = "#asset-table-body tr:nth-of-type(44) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web44;

    @FindBy(css = "#asset-table-body tr:nth-of-type(45) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web45;

    @FindBy(css = "#asset-table-body tr:nth-of-type(46) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web46;

    @FindBy(css = "#asset-table-body tr:nth-of-type(47) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web47;

    @FindBy(css = "#asset-table-body tr:nth-of-type(48) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web48;

    @FindBy(css = "#asset-table-body tr:nth-of-type(49) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web49;

    @FindBy(css = "#asset-table-body tr:nth-of-type(5) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web5;

    @FindBy(css = "#asset-table-body tr:nth-of-type(50) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web50;

    @FindBy(css = "#asset-table-body tr:nth-of-type(6) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web6;

    @FindBy(css = "#asset-table-body tr:nth-of-type(7) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web7;

    @FindBy(css = "#asset-table-body tr:nth-of-type(8) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web8;

    @FindBy(css = "#asset-table-body tr:nth-of-type(9) td:nth-of-type(5) ul li:nth-of-type(2) label input.embeddable-xml-input[type='text']")
    @CacheLookup
    private WebElement web9;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@handouts_7.00x_Readings.pdf']")
    @CacheLookup
    private WebElement xReadingsPdf700;

    @FindBy(css = "a[href='/asset-v1:sandbox+Demo+01+type@asset+block@handouts_7.00x_Schedule.pdf']")
    @CacheLookup
    private WebElement xSchedulePdf700;

    public EdXFilesUploads() {
    }

    public EdXFilesUploads(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXFilesUploads(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXFilesUploads(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Advanced Settings Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickAdvancedSettingsLink() {
        advancedSettings.click();
        return this;
    }

    /**
     * Click on Al2o3.png Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickAl2o3PngLink() {
        al2o3Png.click();
        return this;
    }

    /**
     * Click on 4b0dh.pdb Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickB0dhPdbLink4() {
        b0dhPdb4.click();
        return this;
    }

    /**
     * Click on Benzene.c3xml Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickBenzeneC3xmlLink() {
        benzeneC3xml.click();
        return this;
    }

    /**
     * Click on C6h6.smol Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickC6h6SmolLink() {
        c6h6Smol.click();
        return this;
    }

    /**
     * Click on C.spt Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCSptLink() {
        cSpt.click();
        return this;
    }

    /**
     * Click on Caffeine.jvxl Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCaffeineJvxlLink() {
        caffeineJvxl.click();
        return this;
    }

    /**
     * Click on Certificates Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCertificatesLink() {
        certificates.click();
        return this;
    }

    /**
     * Click on Choose File Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickChooseFileLink() {
        chooseFile.click();
        return this;
    }

    /**
     * Click on Close Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCloseLink() {
        close.click();
        return this;
    }

    /**
     * Click on Co2.smol Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCo2SmolLink() {
        co2Smol.click();
        return this;
    }

    /**
     * Click on Collapsiblehdr.css Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCollapsiblehdrCssLink() {
        collapsiblehdrCss.click();
        return this;
    }

    /**
     * Click on Course Team Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCourseTeamLink() {
        courseTeam.click();
        return this;
    }

    /**
     * Click on 1crn.pdb Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickCrnPdbLink1() {
        crnPdb1.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset10Link() {
        deleteThisAsset10.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset11Link() {
        deleteThisAsset11.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset12Link() {
        deleteThisAsset12.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset13Link() {
        deleteThisAsset13.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset14Link() {
        deleteThisAsset14.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset15Link() {
        deleteThisAsset15.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset16Link() {
        deleteThisAsset16.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset17Link() {
        deleteThisAsset17.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset18Link() {
        deleteThisAsset18.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset19Link() {
        deleteThisAsset19.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset1Link() {
        deleteThisAsset1.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset20Link() {
        deleteThisAsset20.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset21Link() {
        deleteThisAsset21.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset22Link() {
        deleteThisAsset22.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset23Link() {
        deleteThisAsset23.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset24Link() {
        deleteThisAsset24.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset25Link() {
        deleteThisAsset25.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset26Link() {
        deleteThisAsset26.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset27Link() {
        deleteThisAsset27.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset28Link() {
        deleteThisAsset28.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset29Link() {
        deleteThisAsset29.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset2Link() {
        deleteThisAsset2.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset30Link() {
        deleteThisAsset30.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset31Link() {
        deleteThisAsset31.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset32Link() {
        deleteThisAsset32.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset33Link() {
        deleteThisAsset33.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset34Link() {
        deleteThisAsset34.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset35Link() {
        deleteThisAsset35.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset36Link() {
        deleteThisAsset36.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset37Link() {
        deleteThisAsset37.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset38Link() {
        deleteThisAsset38.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset39Link() {
        deleteThisAsset39.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset3Link() {
        deleteThisAsset3.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset40Link() {
        deleteThisAsset40.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset41Link() {
        deleteThisAsset41.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset42Link() {
        deleteThisAsset42.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset43Link() {
        deleteThisAsset43.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset44Link() {
        deleteThisAsset44.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset45Link() {
        deleteThisAsset45.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset46Link() {
        deleteThisAsset46.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset47Link() {
        deleteThisAsset47.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset48Link() {
        deleteThisAsset48.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset49Link() {
        deleteThisAsset49.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset4Link() {
        deleteThisAsset4.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset50Link() {
        deleteThisAsset50.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset5Link() {
        deleteThisAsset5.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset6Link() {
        deleteThisAsset6.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset7Link() {
        deleteThisAsset7.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset8Link() {
        deleteThisAsset8.click();
        return this;
    }

    /**
     * Click on Delete This Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDeleteThisAsset9Link() {
        deleteThisAsset9.click();
        return this;
    }

    /**
     * Click on Dopamine.mol Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickDopamineMolLink() {
        dopamineMol.click();
        return this;
    }

    /**
     * Click on Edunext Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdunextLink() {
        edunext.click();
        return this;
    }

    /**
     * Click on Edx Documentation Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdxDocumentationLink() {
        edxDocumentation.click();
        return this;
    }

    /**
     * Click on Edx.html Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdxHtml1Link() {
        edxHtml1.click();
        return this;
    }

    /**
     * Click on Edx.html Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdxHtml2Link() {
        edxHtml2.click();
        return this;
    }

    /**
     * Click on Edx.htmle Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdxHtmle1Link() {
        edxHtmle1.click();
        return this;
    }

    /**
     * Click on Edx.htmle Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdxHtmle2Link() {
        edxHtmle2.click();
        return this;
    }

    /**
     * Click on Edx Inc. Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdxIncLink() {
        edxInc.click();
        return this;
    }

    /**
     * Click on Edx.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEdxJsLink() {
        edxJs.click();
        return this;
    }

    /**
     * Click on Enroll In Edx101 Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEnrollInEdx101Link() {
        enrollInEdx101.click();
        return this;
    }

    /**
     * Click on Enroll In Studiox Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEnrollInStudioxLink() {
        enrollInStudiox.click();
        return this;
    }

    /**
     * Click on Estron.cml Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEstronCmlLink() {
        estronCml.click();
        return this;
    }

    /**
     * Click on Estron.cmle Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickEstronCmleLink() {
        estronCmle.click();
        return this;
    }

    /**
     * Click on Exploregene.jar Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickExploregeneJarLink() {
        exploregeneJar.click();
        return this;
    }

    /**
     * Click on Export Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickExportLink() {
        export.click();
        return this;
    }

    /**
     * Click on Files Uploads Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickFilesUploadsLink() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ClickHelper.advancedClick((RemoteWebDriver) driver, filesUploads);
        //filesUploads.click();
        return this;
    }

    /**
     * Click on Grading Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGradingLink() {
        grading.click();
        return this;
    }

    /**
     * Click on Grid.localear.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocalearJsLink() {
        gridLocalearJs.click();
        return this;
    }

    /**
     * Click on Grid.localebg1251.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocalebg1251JsLink() {
        gridLocalebg1251Js.click();
        return this;
    }

    /**
     * Click on Grid.localefr.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocalefrJsLink() {
        gridLocalefrJs.click();
        return this;
    }

    /**
     * Click on Grid.localehr1250.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocalehr1250JsLink() {
        gridLocalehr1250Js.click();
        return this;
    }

    /**
     * Click on Grid.localeis.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocaleisJsLink() {
        gridLocaleisJs.click();
        return this;
    }

    /**
     * Click on Grid.localeja.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocalejaJsLink() {
        gridLocalejaJs.click();
        return this;
    }

    /**
     * Click on Grid.localenl.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocalenlJsLink() {
        gridLocalenlJs.click();
        return this;
    }

    /**
     * Click on Grid.localero.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocaleroJsLink() {
        gridLocaleroJs.click();
        return this;
    }

    /**
     * Click on Grid.localesr.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGridLocalesrJsLink() {
        gridLocalesrJs.click();
        return this;
    }

    /**
     * Click on Group Configurations Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickGroupConfigurationsLink() {
        groupConfigurations.click();
        return this;
    }

    /**
     * Click on Help Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickHelpLink() {
        help.click();
        return this;
    }

    /**
     * Click on 1hxw.png Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickHxwPngLink1() {
        hxwPng1.click();
        return this;
    }

    /**
     * Click on 1hxw.pngj Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickHxwPngjLink1() {
        hxwPngj1.click();
        return this;
    }

    /**
     * Click on Import Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickImportLink() {
        limport.click();
        return this;
    }

    /**
     * Click on Index.html Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickIndexHtmlLink() {
        indexHtml.click();
        return this;
    }

    /**
     * Click on Index.htmle Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickIndexHtmleLink() {
        indexHtmle.click();
        return this;
    }

    /**
     * Click on Javacheck.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickJavacheckJsLink() {
        javacheckJs.click();
        return this;
    }

    /**
     * Click on Jsmol.js Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickJsmolJsLink() {
        jsmolJs.click();
        return this;
    }

    /**
     * Click on Jsmol.php Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickJsmolPhpLink() {
        jsmolPhp.click();
        return this;
    }

    /**
     * Click on Learn More About Managing Files Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickLearnMoreAboutManagingFilesLink() {
        learnMoreAboutManagingFiles.click();
        return this;
    }

    /**
     * Click on Looking For Help With Studio Hide Studio Help Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickLookingForHelpWithStudioLink() {
        lookingForHelpWithStudio.click();
        return this;
    }

    /**
     * Click on Lysoh.pdb Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickLysohPdbLink() {
        lysohPdb.click();
        return this;
    }

    /**
     * Click on 1m19.pdb.gz Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickM19PdbGzLink1() {
        m19PdbGz1.click();
        return this;
    }

    /**
     * Click on Make Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickMakeLink() {
        make.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickNext1Button() {
        next1.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickNext2Button() {
        next2.click();
        return this;
    }

    /**
     * Click on Open Edx Portal Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickOpenEdxPortalLink() {
        openEdxPortal.click();
        return this;
    }

    /**
     * Click on Other Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickOtherLink() {
        other.click();
        return this;
    }

    /**
     * Click on Outline Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickOutlineLink() {
        outline.click();
        return this;
    }

    /**
     * Click on Pages Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickPagesLink() {
        pages.click();
        return this;
    }

    /**
     * Click on Periodictable.css Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickPeriodictableCssLink() {
        periodictableCss.click();
        return this;
    }

    /**
     * Click on Periodictablecolors.css Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickPeriodictablecolorsCssLink() {
        periodictablecolorsCss.click();
        return this;
    }

    /**
     * Click on Previous Button.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickPrevious1Button() {
        previous1.click();
        return this;
    }

    /**
     * Click on Previous Button.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickPrevious2Button() {
        previous2.click();
        return this;
    }

    /**
     * Click on Readme.txt Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickReadmeTxtLink() {
        readmeTxt.click();
        return this;
    }

    /**
     * Click on Sandboxdemo Edx Demonstration Course Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickSandboxdemoEdxDemonstrationCourseLink() {
        sandboxdemoEdxDemonstrationCourse.click();
        return this;
    }

    /**
     * Click on Schedule Details Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickScheduleDetailsLink() {
        scheduleDetails.click();
        return this;
    }

    /**
     * Click on Show All Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickShowAllLink() {
        showAll.click();
        return this;
    }

    /**
     * Click on Sign Out Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickSignOutLink() {
        signOut.click();
        return this;
    }

    /**
     * Click on Skip To Main Content Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickSkipToMainContentLink() {
        skipToMainContent.click();
        return this;
    }

    /**
     * Click on Studio Home Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickStudioHomeLink() {
        studioHome.click();
        return this;
    }

    /**
     * Click on Subsaewvgztbf44.srt.sjson Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickSubsaewvgztbf44SrtSjsonLink() {
        subsaewvgztbf44SrtSjson.click();
        return this;
    }

    /**
     * Click on T2.spt Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickT2SptLink() {
        t2Spt.click();
        return this;
    }

    /**
     * Click on Test3.htm Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickTest3HtmLink() {
        test3Htm.click();
        return this;
    }

    /**
     * Click on Test.molden Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickTestMoldenLink() {
        testMolden.click();
        return this;
    }

    /**
     * Click on Test.spt Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickTestSptLink() {
        testSpt.click();
        return this;
    }

    /**
     * Click on Textbooks Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickTextbooksLink() {
        textbooks.click();
        return this;
    }

    /**
     * Click on Ui.jqgrid.css Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickUiJqgridCssLink() {
        uiJqgridCss.click();
        return this;
    }

    /**
     * Click on Updates Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickUpdatesLink() {
        updates.click();
        return this;
    }

    /**
     * Click on Upload New File Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickUploadNewFileLink() {
        uploadNewFile.click();
        return this;
    }

    /**
     * Click on Upload Your First Asset Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickUploadYourFirstAssetLink() {
        uploadYourFirstAsset.click();
        return this;
    }

    /**
     * Click on Vgljnslpforsandbox.tgz Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickVgljnslpforsandboxTgzLink() {
        vgljnslpforsandboxTgz.click();
        return this;
    }

    /**
     * Click on Vib.xml Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickVibXmlLink() {
        vibXml.click();
        return this;
    }

    /**
     * Click on 7.00x Readings.pdf Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickXReadingsPdfLink700() {
        xReadingsPdf700.click();
        return this;
    }

    /**
     * Click on 7.00x Schedule.pdf Link.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads clickXSchedulePdfLink700() {
        xSchedulePdf700.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads fill() {
        setStudio1TextField();
        setWeb1TextField();
        setLockThisAsset1CheckboxField();
        setStudio2TextField();
        setWeb2TextField();
        setLockThisAsset2CheckboxField();
        setStudio3TextField();
        setWeb3TextField();
        setLockThisAsset3CheckboxField();
        setStudio4TextField();
        setWeb4TextField();
        setLockThisAsset4CheckboxField();
        setStudio5TextField();
        setWeb5TextField();
        setLockThisAsset5CheckboxField();
        setStudio6TextField();
        setWeb6TextField();
        setLockThisAsset6CheckboxField();
        setStudio7TextField();
        setWeb7TextField();
        setLockThisAsset7CheckboxField();
        setStudio8TextField();
        setWeb8TextField();
        setLockThisAsset8CheckboxField();
        setStudio9TextField();
        setWeb9TextField();
        setLockThisAsset9CheckboxField();
        setStudio10TextField();
        setWeb10TextField();
        setLockThisAsset10CheckboxField();
        setStudio11TextField();
        setWeb11TextField();
        setLockThisAsset11CheckboxField();
        setStudio12TextField();
        setWeb12TextField();
        setLockThisAsset12CheckboxField();
        setStudio13TextField();
        setWeb13TextField();
        setLockThisAsset13CheckboxField();
        setStudio14TextField();
        setWeb14TextField();
        setLockThisAsset14CheckboxField();
        setStudio15TextField();
        setWeb15TextField();
        setLockThisAsset15CheckboxField();
        setStudio16TextField();
        setWeb16TextField();
        setLockThisAsset16CheckboxField();
        setStudio17TextField();
        setWeb17TextField();
        setLockThisAsset17CheckboxField();
        setStudio18TextField();
        setWeb18TextField();
        setLockThisAsset18CheckboxField();
        setStudio19TextField();
        setWeb19TextField();
        setLockThisAsset19CheckboxField();
        setStudio20TextField();
        setWeb20TextField();
        setLockThisAsset20CheckboxField();
        setStudio21TextField();
        setWeb21TextField();
        setLockThisAsset21CheckboxField();
        setStudio22TextField();
        setWeb22TextField();
        setLockThisAsset22CheckboxField();
        setStudio23TextField();
        setWeb23TextField();
        setLockThisAsset23CheckboxField();
        setStudio24TextField();
        setWeb24TextField();
        setLockThisAsset24CheckboxField();
        setStudio25TextField();
        setWeb25TextField();
        setLockThisAsset25CheckboxField();
        setStudio26TextField();
        setWeb26TextField();
        setLockThisAsset26CheckboxField();
        setStudio27TextField();
        setWeb27TextField();
        setLockThisAsset27CheckboxField();
        setStudio28TextField();
        setWeb28TextField();
        setLockThisAsset28CheckboxField();
        setStudio29TextField();
        setWeb29TextField();
        setLockThisAsset29CheckboxField();
        setStudio30TextField();
        setWeb30TextField();
        setLockThisAsset30CheckboxField();
        setStudio31TextField();
        setWeb31TextField();
        setLockThisAsset31CheckboxField();
        setStudio32TextField();
        setWeb32TextField();
        setLockThisAsset32CheckboxField();
        setStudio33TextField();
        setWeb33TextField();
        setLockThisAsset33CheckboxField();
        setStudio34TextField();
        setWeb34TextField();
        setLockThisAsset34CheckboxField();
        setStudio35TextField();
        setWeb35TextField();
        setLockThisAsset35CheckboxField();
        setStudio36TextField();
        setWeb36TextField();
        setLockThisAsset36CheckboxField();
        setStudio37TextField();
        setWeb37TextField();
        setLockThisAsset37CheckboxField();
        setStudio38TextField();
        setWeb38TextField();
        setLockThisAsset38CheckboxField();
        setStudio39TextField();
        setWeb39TextField();
        setLockThisAsset39CheckboxField();
        setStudio40TextField();
        setWeb40TextField();
        setLockThisAsset40CheckboxField();
        setStudio41TextField();
        setWeb41TextField();
        setLockThisAsset41CheckboxField();
        setStudio42TextField();
        setWeb42TextField();
        setLockThisAsset42CheckboxField();
        setStudio43TextField();
        setWeb43TextField();
        setLockThisAsset43CheckboxField();
        setStudio44TextField();
        setWeb44TextField();
        setLockThisAsset44CheckboxField();
        setStudio45TextField();
        setWeb45TextField();
        setLockThisAsset45CheckboxField();
        setStudio46TextField();
        setWeb46TextField();
        setLockThisAsset46CheckboxField();
        setStudio47TextField();
        setWeb47TextField();
        setLockThisAsset47CheckboxField();
        setStudio48TextField();
        setWeb48TextField();
        setLockThisAsset48CheckboxField();
        setStudio49TextField();
        setWeb49TextField();
        setLockThisAsset49CheckboxField();
        setStudio50TextField();
        setWeb50TextField();
        setLockThisAsset50CheckboxField();
        setPageNumberOutOf23TextField();
        setUrl1FileField();
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset10CheckboxField() {
        if (!lockThisAsset10.isSelected()) {
            lockThisAsset10.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset11CheckboxField() {
        if (!lockThisAsset11.isSelected()) {
            lockThisAsset11.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset12CheckboxField() {
        if (!lockThisAsset12.isSelected()) {
            lockThisAsset12.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset13CheckboxField() {
        if (!lockThisAsset13.isSelected()) {
            lockThisAsset13.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset14CheckboxField() {
        if (!lockThisAsset14.isSelected()) {
            lockThisAsset14.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset15CheckboxField() {
        if (!lockThisAsset15.isSelected()) {
            lockThisAsset15.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset16CheckboxField() {
        if (!lockThisAsset16.isSelected()) {
            lockThisAsset16.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset17CheckboxField() {
        if (!lockThisAsset17.isSelected()) {
            lockThisAsset17.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset18CheckboxField() {
        if (!lockThisAsset18.isSelected()) {
            lockThisAsset18.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset19CheckboxField() {
        if (!lockThisAsset19.isSelected()) {
            lockThisAsset19.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset1CheckboxField() {
        if (!lockThisAsset1.isSelected()) {
            lockThisAsset1.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset20CheckboxField() {
        if (!lockThisAsset20.isSelected()) {
            lockThisAsset20.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset21CheckboxField() {
        if (!lockThisAsset21.isSelected()) {
            lockThisAsset21.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset22CheckboxField() {
        if (!lockThisAsset22.isSelected()) {
            lockThisAsset22.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset23CheckboxField() {
        if (!lockThisAsset23.isSelected()) {
            lockThisAsset23.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset24CheckboxField() {
        if (!lockThisAsset24.isSelected()) {
            lockThisAsset24.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset25CheckboxField() {
        if (!lockThisAsset25.isSelected()) {
            lockThisAsset25.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset26CheckboxField() {
        if (!lockThisAsset26.isSelected()) {
            lockThisAsset26.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset27CheckboxField() {
        if (!lockThisAsset27.isSelected()) {
            lockThisAsset27.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset28CheckboxField() {
        if (!lockThisAsset28.isSelected()) {
            lockThisAsset28.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset29CheckboxField() {
        if (!lockThisAsset29.isSelected()) {
            lockThisAsset29.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset2CheckboxField() {
        if (!lockThisAsset2.isSelected()) {
            lockThisAsset2.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset30CheckboxField() {
        if (!lockThisAsset30.isSelected()) {
            lockThisAsset30.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset31CheckboxField() {
        if (!lockThisAsset31.isSelected()) {
            lockThisAsset31.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset32CheckboxField() {
        if (!lockThisAsset32.isSelected()) {
            lockThisAsset32.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset33CheckboxField() {
        if (!lockThisAsset33.isSelected()) {
            lockThisAsset33.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset34CheckboxField() {
        if (!lockThisAsset34.isSelected()) {
            lockThisAsset34.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset35CheckboxField() {
        if (!lockThisAsset35.isSelected()) {
            lockThisAsset35.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset36CheckboxField() {
        if (!lockThisAsset36.isSelected()) {
            lockThisAsset36.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset37CheckboxField() {
        if (!lockThisAsset37.isSelected()) {
            lockThisAsset37.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset38CheckboxField() {
        if (!lockThisAsset38.isSelected()) {
            lockThisAsset38.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset39CheckboxField() {
        if (!lockThisAsset39.isSelected()) {
            lockThisAsset39.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset3CheckboxField() {
        if (!lockThisAsset3.isSelected()) {
            lockThisAsset3.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset40CheckboxField() {
        if (!lockThisAsset40.isSelected()) {
            lockThisAsset40.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset41CheckboxField() {
        if (!lockThisAsset41.isSelected()) {
            lockThisAsset41.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset42CheckboxField() {
        if (!lockThisAsset42.isSelected()) {
            lockThisAsset42.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset43CheckboxField() {
        if (!lockThisAsset43.isSelected()) {
            lockThisAsset43.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset44CheckboxField() {
        if (!lockThisAsset44.isSelected()) {
            lockThisAsset44.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset45CheckboxField() {
        if (!lockThisAsset45.isSelected()) {
            lockThisAsset45.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset46CheckboxField() {
        if (!lockThisAsset46.isSelected()) {
            lockThisAsset46.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset47CheckboxField() {
        if (!lockThisAsset47.isSelected()) {
            lockThisAsset47.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset48CheckboxField() {
        if (!lockThisAsset48.isSelected()) {
            lockThisAsset48.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset49CheckboxField() {
        if (!lockThisAsset49.isSelected()) {
            lockThisAsset49.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset4CheckboxField() {
        if (!lockThisAsset4.isSelected()) {
            lockThisAsset4.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset50CheckboxField() {
        if (!lockThisAsset50.isSelected()) {
            lockThisAsset50.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset5CheckboxField() {
        if (!lockThisAsset5.isSelected()) {
            lockThisAsset5.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset6CheckboxField() {
        if (!lockThisAsset6.isSelected()) {
            lockThisAsset6.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset7CheckboxField() {
        if (!lockThisAsset7.isSelected()) {
            lockThisAsset7.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset8CheckboxField() {
        if (!lockThisAsset8.isSelected()) {
            lockThisAsset8.click();
        }
        return this;
    }

    /**
     * Set Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setLockThisAsset9CheckboxField() {
        if (!lockThisAsset9.isSelected()) {
            lockThisAsset9.click();
        }
        return this;
    }

    /**
     * Set default value to Page Number Out Of 23 Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setPageNumberOutOf23TextField() {
        return setPageNumberOutOf23TextField(data.get("PAGE_NUMBER_OUT_OF_23"));
    }

    /**
     * Set value to Page Number Out Of 23 Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setPageNumberOutOf23TextField(String pageNumberOutOf23Value) {
        pageNumberOutOf23.sendKeys(pageNumberOutOf23Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio10TextField() {
        return setStudio10TextField(data.get("STUDIO_10"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio10TextField(String studio10Value) {
        studio10.sendKeys(studio10Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio11TextField() {
        return setStudio11TextField(data.get("STUDIO_11"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio11TextField(String studio11Value) {
        studio11.sendKeys(studio11Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio12TextField() {
        return setStudio12TextField(data.get("STUDIO_12"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio12TextField(String studio12Value) {
        studio12.sendKeys(studio12Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio13TextField() {
        return setStudio13TextField(data.get("STUDIO_13"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio13TextField(String studio13Value) {
        studio13.sendKeys(studio13Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio14TextField() {
        return setStudio14TextField(data.get("STUDIO_14"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio14TextField(String studio14Value) {
        studio14.sendKeys(studio14Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio15TextField() {
        return setStudio15TextField(data.get("STUDIO_15"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio15TextField(String studio15Value) {
        studio15.sendKeys(studio15Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio16TextField() {
        return setStudio16TextField(data.get("STUDIO_16"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio16TextField(String studio16Value) {
        studio16.sendKeys(studio16Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio17TextField() {
        return setStudio17TextField(data.get("STUDIO_17"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio17TextField(String studio17Value) {
        studio17.sendKeys(studio17Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio18TextField() {
        return setStudio18TextField(data.get("STUDIO_18"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio18TextField(String studio18Value) {
        studio18.sendKeys(studio18Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio19TextField() {
        return setStudio19TextField(data.get("STUDIO_19"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio19TextField(String studio19Value) {
        studio19.sendKeys(studio19Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio1TextField() {
        return setStudio1TextField(data.get("STUDIO_1"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio1TextField(String studio1Value) {
        studio1.sendKeys(studio1Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio20TextField() {
        return setStudio20TextField(data.get("STUDIO_20"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio20TextField(String studio20Value) {
        studio20.sendKeys(studio20Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio21TextField() {
        return setStudio21TextField(data.get("STUDIO_21"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio21TextField(String studio21Value) {
        studio21.sendKeys(studio21Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio22TextField() {
        return setStudio22TextField(data.get("STUDIO_22"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio22TextField(String studio22Value) {
        studio22.sendKeys(studio22Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio23TextField() {
        return setStudio23TextField(data.get("STUDIO_23"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio23TextField(String studio23Value) {
        studio23.sendKeys(studio23Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio24TextField() {
        return setStudio24TextField(data.get("STUDIO_24"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio24TextField(String studio24Value) {
        studio24.sendKeys(studio24Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio25TextField() {
        return setStudio25TextField(data.get("STUDIO_25"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio25TextField(String studio25Value) {
        studio25.sendKeys(studio25Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio26TextField() {
        return setStudio26TextField(data.get("STUDIO_26"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio26TextField(String studio26Value) {
        studio26.sendKeys(studio26Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio27TextField() {
        return setStudio27TextField(data.get("STUDIO_27"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio27TextField(String studio27Value) {
        studio27.sendKeys(studio27Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio28TextField() {
        return setStudio28TextField(data.get("STUDIO_28"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio28TextField(String studio28Value) {
        studio28.sendKeys(studio28Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio29TextField() {
        return setStudio29TextField(data.get("STUDIO_29"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio29TextField(String studio29Value) {
        studio29.sendKeys(studio29Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio2TextField() {
        return setStudio2TextField(data.get("STUDIO_2"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio2TextField(String studio2Value) {
        studio2.sendKeys(studio2Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio30TextField() {
        return setStudio30TextField(data.get("STUDIO_30"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio30TextField(String studio30Value) {
        studio30.sendKeys(studio30Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio31TextField() {
        return setStudio31TextField(data.get("STUDIO_31"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio31TextField(String studio31Value) {
        studio31.sendKeys(studio31Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio32TextField() {
        return setStudio32TextField(data.get("STUDIO_32"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio32TextField(String studio32Value) {
        studio32.sendKeys(studio32Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio33TextField() {
        return setStudio33TextField(data.get("STUDIO_33"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio33TextField(String studio33Value) {
        studio33.sendKeys(studio33Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio34TextField() {
        return setStudio34TextField(data.get("STUDIO_34"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio34TextField(String studio34Value) {
        studio34.sendKeys(studio34Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio35TextField() {
        return setStudio35TextField(data.get("STUDIO_35"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio35TextField(String studio35Value) {
        studio35.sendKeys(studio35Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio36TextField() {
        return setStudio36TextField(data.get("STUDIO_36"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio36TextField(String studio36Value) {
        studio36.sendKeys(studio36Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio37TextField() {
        return setStudio37TextField(data.get("STUDIO_37"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio37TextField(String studio37Value) {
        studio37.sendKeys(studio37Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio38TextField() {
        return setStudio38TextField(data.get("STUDIO_38"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio38TextField(String studio38Value) {
        studio38.sendKeys(studio38Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio39TextField() {
        return setStudio39TextField(data.get("STUDIO_39"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio39TextField(String studio39Value) {
        studio39.sendKeys(studio39Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio3TextField() {
        return setStudio3TextField(data.get("STUDIO_3"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio3TextField(String studio3Value) {
        studio3.sendKeys(studio3Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio40TextField() {
        return setStudio40TextField(data.get("STUDIO_40"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio40TextField(String studio40Value) {
        studio40.sendKeys(studio40Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio41TextField() {
        return setStudio41TextField(data.get("STUDIO_41"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio41TextField(String studio41Value) {
        studio41.sendKeys(studio41Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio42TextField() {
        return setStudio42TextField(data.get("STUDIO_42"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio42TextField(String studio42Value) {
        studio42.sendKeys(studio42Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio43TextField() {
        return setStudio43TextField(data.get("STUDIO_43"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio43TextField(String studio43Value) {
        studio43.sendKeys(studio43Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio44TextField() {
        return setStudio44TextField(data.get("STUDIO_44"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio44TextField(String studio44Value) {
        studio44.sendKeys(studio44Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio45TextField() {
        return setStudio45TextField(data.get("STUDIO_45"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio45TextField(String studio45Value) {
        studio45.sendKeys(studio45Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio46TextField() {
        return setStudio46TextField(data.get("STUDIO_46"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio46TextField(String studio46Value) {
        studio46.sendKeys(studio46Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio47TextField() {
        return setStudio47TextField(data.get("STUDIO_47"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio47TextField(String studio47Value) {
        studio47.sendKeys(studio47Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio48TextField() {
        return setStudio48TextField(data.get("STUDIO_48"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio48TextField(String studio48Value) {
        studio48.sendKeys(studio48Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio49TextField() {
        return setStudio49TextField(data.get("STUDIO_49"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio49TextField(String studio49Value) {
        studio49.sendKeys(studio49Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio4TextField() {
        return setStudio4TextField(data.get("STUDIO_4"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio4TextField(String studio4Value) {
        studio4.sendKeys(studio4Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio50TextField() {
        return setStudio50TextField(data.get("STUDIO_50"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio50TextField(String studio50Value) {
        studio50.sendKeys(studio50Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio5TextField() {
        return setStudio5TextField(data.get("STUDIO_5"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio5TextField(String studio5Value) {
        studio5.sendKeys(studio5Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio6TextField() {
        return setStudio6TextField(data.get("STUDIO_6"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio6TextField(String studio6Value) {
        studio6.sendKeys(studio6Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio7TextField() {
        return setStudio7TextField(data.get("STUDIO_7"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio7TextField(String studio7Value) {
        studio7.sendKeys(studio7Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio8TextField() {
        return setStudio8TextField(data.get("STUDIO_8"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio8TextField(String studio8Value) {
        studio8.sendKeys(studio8Value);
        return this;
    }

    /**
     * Set default value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio9TextField() {
        return setStudio9TextField(data.get("STUDIO_9"));
    }

    /**
     * Set value to Studio Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setStudio9TextField(String studio9Value) {
        studio9.sendKeys(studio9Value);
        return this;
    }

    /**
     * Set default value to Url Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setUrl1FileField() {
        return setUrl1FileField(data.get("URL"));
    }

    /**
     * Set Url File field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setUrl1FileField(String urlValue) {
        url1.sendKeys(urlValue);
        return this;
    }

    /**
     * Set Url File field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setUrl2FileField() {
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb10TextField() {
        return setWeb10TextField(data.get("WEB_10"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb10TextField(String web10Value) {
        web10.sendKeys(web10Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb11TextField() {
        return setWeb11TextField(data.get("WEB_11"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb11TextField(String web11Value) {
        web11.sendKeys(web11Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb12TextField() {
        return setWeb12TextField(data.get("WEB_12"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb12TextField(String web12Value) {
        web12.sendKeys(web12Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb13TextField() {
        return setWeb13TextField(data.get("WEB_13"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb13TextField(String web13Value) {
        web13.sendKeys(web13Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb14TextField() {
        return setWeb14TextField(data.get("WEB_14"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb14TextField(String web14Value) {
        web14.sendKeys(web14Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb15TextField() {
        return setWeb15TextField(data.get("WEB_15"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb15TextField(String web15Value) {
        web15.sendKeys(web15Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb16TextField() {
        return setWeb16TextField(data.get("WEB_16"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb16TextField(String web16Value) {
        web16.sendKeys(web16Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb17TextField() {
        return setWeb17TextField(data.get("WEB_17"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb17TextField(String web17Value) {
        web17.sendKeys(web17Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb18TextField() {
        return setWeb18TextField(data.get("WEB_18"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb18TextField(String web18Value) {
        web18.sendKeys(web18Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb19TextField() {
        return setWeb19TextField(data.get("WEB_19"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb19TextField(String web19Value) {
        web19.sendKeys(web19Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb1TextField() {
        return setWeb1TextField(data.get("WEB_1"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb1TextField(String web1Value) {
        web1.sendKeys(web1Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb20TextField() {
        return setWeb20TextField(data.get("WEB_20"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb20TextField(String web20Value) {
        web20.sendKeys(web20Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb21TextField() {
        return setWeb21TextField(data.get("WEB_21"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb21TextField(String web21Value) {
        web21.sendKeys(web21Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb22TextField() {
        return setWeb22TextField(data.get("WEB_22"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb22TextField(String web22Value) {
        web22.sendKeys(web22Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb23TextField() {
        return setWeb23TextField(data.get("WEB_23"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb23TextField(String web23Value) {
        web23.sendKeys(web23Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb24TextField() {
        return setWeb24TextField(data.get("WEB_24"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb24TextField(String web24Value) {
        web24.sendKeys(web24Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb25TextField() {
        return setWeb25TextField(data.get("WEB_25"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb25TextField(String web25Value) {
        web25.sendKeys(web25Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb26TextField() {
        return setWeb26TextField(data.get("WEB_26"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb26TextField(String web26Value) {
        web26.sendKeys(web26Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb27TextField() {
        return setWeb27TextField(data.get("WEB_27"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb27TextField(String web27Value) {
        web27.sendKeys(web27Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb28TextField() {
        return setWeb28TextField(data.get("WEB_28"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb28TextField(String web28Value) {
        web28.sendKeys(web28Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb29TextField() {
        return setWeb29TextField(data.get("WEB_29"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb29TextField(String web29Value) {
        web29.sendKeys(web29Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb2TextField() {
        return setWeb2TextField(data.get("WEB_2"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb2TextField(String web2Value) {
        web2.sendKeys(web2Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb30TextField() {
        return setWeb30TextField(data.get("WEB_30"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb30TextField(String web30Value) {
        web30.sendKeys(web30Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb31TextField() {
        return setWeb31TextField(data.get("WEB_31"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb31TextField(String web31Value) {
        web31.sendKeys(web31Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb32TextField() {
        return setWeb32TextField(data.get("WEB_32"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb32TextField(String web32Value) {
        web32.sendKeys(web32Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb33TextField() {
        return setWeb33TextField(data.get("WEB_33"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb33TextField(String web33Value) {
        web33.sendKeys(web33Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb34TextField() {
        return setWeb34TextField(data.get("WEB_34"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb34TextField(String web34Value) {
        web34.sendKeys(web34Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb35TextField() {
        return setWeb35TextField(data.get("WEB_35"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb35TextField(String web35Value) {
        web35.sendKeys(web35Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb36TextField() {
        return setWeb36TextField(data.get("WEB_36"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb36TextField(String web36Value) {
        web36.sendKeys(web36Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb37TextField() {
        return setWeb37TextField(data.get("WEB_37"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb37TextField(String web37Value) {
        web37.sendKeys(web37Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb38TextField() {
        return setWeb38TextField(data.get("WEB_38"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb38TextField(String web38Value) {
        web38.sendKeys(web38Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb39TextField() {
        return setWeb39TextField(data.get("WEB_39"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb39TextField(String web39Value) {
        web39.sendKeys(web39Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb3TextField() {
        return setWeb3TextField(data.get("WEB_3"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb3TextField(String web3Value) {
        web3.sendKeys(web3Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb40TextField() {
        return setWeb40TextField(data.get("WEB_40"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb40TextField(String web40Value) {
        web40.sendKeys(web40Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb41TextField() {
        return setWeb41TextField(data.get("WEB_41"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb41TextField(String web41Value) {
        web41.sendKeys(web41Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb42TextField() {
        return setWeb42TextField(data.get("WEB_42"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb42TextField(String web42Value) {
        web42.sendKeys(web42Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb43TextField() {
        return setWeb43TextField(data.get("WEB_43"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb43TextField(String web43Value) {
        web43.sendKeys(web43Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb44TextField() {
        return setWeb44TextField(data.get("WEB_44"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb44TextField(String web44Value) {
        web44.sendKeys(web44Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb45TextField() {
        return setWeb45TextField(data.get("WEB_45"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb45TextField(String web45Value) {
        web45.sendKeys(web45Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb46TextField() {
        return setWeb46TextField(data.get("WEB_46"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb46TextField(String web46Value) {
        web46.sendKeys(web46Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb47TextField() {
        return setWeb47TextField(data.get("WEB_47"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb47TextField(String web47Value) {
        web47.sendKeys(web47Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb48TextField() {
        return setWeb48TextField(data.get("WEB_48"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb48TextField(String web48Value) {
        web48.sendKeys(web48Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb49TextField() {
        return setWeb49TextField(data.get("WEB_49"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb49TextField(String web49Value) {
        web49.sendKeys(web49Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb4TextField() {
        return setWeb4TextField(data.get("WEB_4"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb4TextField(String web4Value) {
        web4.sendKeys(web4Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb50TextField() {
        return setWeb50TextField(data.get("WEB_50"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb50TextField(String web50Value) {
        web50.sendKeys(web50Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb5TextField() {
        return setWeb5TextField(data.get("WEB_5"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb5TextField(String web5Value) {
        web5.sendKeys(web5Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb6TextField() {
        return setWeb6TextField(data.get("WEB_6"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb6TextField(String web6Value) {
        web6.sendKeys(web6Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb7TextField() {
        return setWeb7TextField(data.get("WEB_7"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb7TextField(String web7Value) {
        web7.sendKeys(web7Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb8TextField() {
        return setWeb8TextField(data.get("WEB_8"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb8TextField(String web8Value) {
        web8.sendKeys(web8Value);
        return this;
    }

    /**
     * Set default value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb9TextField() {
        return setWeb9TextField(data.get("WEB_9"));
    }

    /**
     * Set value to Web Text field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads setWeb9TextField(String web9Value) {
        web9.sendKeys(web9Value);
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset10CheckboxField() {
        if (lockThisAsset10.isSelected()) {
            lockThisAsset10.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset11CheckboxField() {
        if (lockThisAsset11.isSelected()) {
            lockThisAsset11.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset12CheckboxField() {
        if (lockThisAsset12.isSelected()) {
            lockThisAsset12.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset13CheckboxField() {
        if (lockThisAsset13.isSelected()) {
            lockThisAsset13.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset14CheckboxField() {
        if (lockThisAsset14.isSelected()) {
            lockThisAsset14.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset15CheckboxField() {
        if (lockThisAsset15.isSelected()) {
            lockThisAsset15.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset16CheckboxField() {
        if (lockThisAsset16.isSelected()) {
            lockThisAsset16.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset17CheckboxField() {
        if (lockThisAsset17.isSelected()) {
            lockThisAsset17.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset18CheckboxField() {
        if (lockThisAsset18.isSelected()) {
            lockThisAsset18.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset19CheckboxField() {
        if (lockThisAsset19.isSelected()) {
            lockThisAsset19.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset1CheckboxField() {
        if (lockThisAsset1.isSelected()) {
            lockThisAsset1.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset20CheckboxField() {
        if (lockThisAsset20.isSelected()) {
            lockThisAsset20.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset21CheckboxField() {
        if (lockThisAsset21.isSelected()) {
            lockThisAsset21.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset22CheckboxField() {
        if (lockThisAsset22.isSelected()) {
            lockThisAsset22.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset23CheckboxField() {
        if (lockThisAsset23.isSelected()) {
            lockThisAsset23.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset24CheckboxField() {
        if (lockThisAsset24.isSelected()) {
            lockThisAsset24.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset25CheckboxField() {
        if (lockThisAsset25.isSelected()) {
            lockThisAsset25.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset26CheckboxField() {
        if (lockThisAsset26.isSelected()) {
            lockThisAsset26.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset27CheckboxField() {
        if (lockThisAsset27.isSelected()) {
            lockThisAsset27.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset28CheckboxField() {
        if (lockThisAsset28.isSelected()) {
            lockThisAsset28.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset29CheckboxField() {
        if (lockThisAsset29.isSelected()) {
            lockThisAsset29.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset2CheckboxField() {
        if (lockThisAsset2.isSelected()) {
            lockThisAsset2.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset30CheckboxField() {
        if (lockThisAsset30.isSelected()) {
            lockThisAsset30.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset31CheckboxField() {
        if (lockThisAsset31.isSelected()) {
            lockThisAsset31.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset32CheckboxField() {
        if (lockThisAsset32.isSelected()) {
            lockThisAsset32.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset33CheckboxField() {
        if (lockThisAsset33.isSelected()) {
            lockThisAsset33.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset34CheckboxField() {
        if (lockThisAsset34.isSelected()) {
            lockThisAsset34.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset35CheckboxField() {
        if (lockThisAsset35.isSelected()) {
            lockThisAsset35.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset36CheckboxField() {
        if (lockThisAsset36.isSelected()) {
            lockThisAsset36.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset37CheckboxField() {
        if (lockThisAsset37.isSelected()) {
            lockThisAsset37.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset38CheckboxField() {
        if (lockThisAsset38.isSelected()) {
            lockThisAsset38.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset39CheckboxField() {
        if (lockThisAsset39.isSelected()) {
            lockThisAsset39.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset3CheckboxField() {
        if (lockThisAsset3.isSelected()) {
            lockThisAsset3.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset40CheckboxField() {
        if (lockThisAsset40.isSelected()) {
            lockThisAsset40.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset41CheckboxField() {
        if (lockThisAsset41.isSelected()) {
            lockThisAsset41.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset42CheckboxField() {
        if (lockThisAsset42.isSelected()) {
            lockThisAsset42.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset43CheckboxField() {
        if (lockThisAsset43.isSelected()) {
            lockThisAsset43.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset44CheckboxField() {
        if (lockThisAsset44.isSelected()) {
            lockThisAsset44.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset45CheckboxField() {
        if (lockThisAsset45.isSelected()) {
            lockThisAsset45.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset46CheckboxField() {
        if (lockThisAsset46.isSelected()) {
            lockThisAsset46.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset47CheckboxField() {
        if (lockThisAsset47.isSelected()) {
            lockThisAsset47.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset48CheckboxField() {
        if (lockThisAsset48.isSelected()) {
            lockThisAsset48.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset49CheckboxField() {
        if (lockThisAsset49.isSelected()) {
            lockThisAsset49.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset4CheckboxField() {
        if (lockThisAsset4.isSelected()) {
            lockThisAsset4.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset50CheckboxField() {
        if (lockThisAsset50.isSelected()) {
            lockThisAsset50.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset5CheckboxField() {
        if (lockThisAsset5.isSelected()) {
            lockThisAsset5.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset6CheckboxField() {
        if (lockThisAsset6.isSelected()) {
            lockThisAsset6.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset7CheckboxField() {
        if (lockThisAsset7.isSelected()) {
            lockThisAsset7.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset8CheckboxField() {
        if (lockThisAsset8.isSelected()) {
            lockThisAsset8.click();
        }
        return this;
    }

    /**
     * Unset Lock This Asset Checkbox field.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads unsetLockThisAsset9CheckboxField() {
        if (lockThisAsset9.isSelected()) {
            lockThisAsset9.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXFilesUploads class instance.
     */
    public EdXFilesUploads verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
