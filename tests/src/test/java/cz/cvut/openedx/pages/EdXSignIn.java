package cz.cvut.openedx.pages;

import java.util.Map;

import cz.cvut.openedx.helpers.ClickHelper;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignIn {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#content div.wrapper-content.wrapper section.content header a.action.action-signin")
    @CacheLookup
    private WebElement dontHaveAStudioAccountSign;

    @FindBy(css = "a[title='This link will open in a new browser window/tab']")
    @CacheLookup
    private WebElement edunext;

    @FindBy(css = "a[href='https://www.edx.org/']")
    @CacheLookup
    private WebElement edxInc;

    @FindBy(id = "email")
    @CacheLookup
    private WebElement email;

    @FindBy(css = "a.action.action-forgotpassword")
    @CacheLookup
    private WebElement forgotPassword;

    @FindBy(css = "a[title='Contextual Online Help']")
    @CacheLookup
    private WebElement help;

    private final String pageLoadedText = "EdX, Open edX, Studio, and the edX and Open edX logos are registered trademarks or trademarks of";

    private final String pageUrl = "/signin";

    @FindBy(id = "password")
    @CacheLookup
    private WebElement password1;

    @FindBy(name = "honor_code")
    @CacheLookup
    private WebElement password2;

    @FindBy(css = "a[href='/signin']")
    @CacheLookup
    private WebElement signIn;

    @FindBy(id = "submit")
    @CacheLookup
    private WebElement signInToStudio;

    @FindBy(css = "a.action.action-signup")
    @CacheLookup
    private WebElement signUp;

    @FindBy(css = "a.nav-skip")
    @CacheLookup
    private WebElement skipToMainContent;

    public EdXSignIn() {
    }

    public EdXSignIn(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignIn(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignIn(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Dont Have A Studio Account Sign Up Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickDontHaveAStudioAccountSignLink() {
        dontHaveAStudioAccountSign.click();
        return this;
    }

    /**
     * Click on Edunext Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickEdunextLink() {
        edunext.click();
        return this;
    }

    /**
     * Click on Edx Inc. Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickEdxIncLink() {
        edxInc.click();
        return this;
    }

    /**
     * Click on Forgot Password Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickForgotPasswordLink() {
        forgotPassword.click();
        return this;
    }

    /**
     * Click on Help Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickHelpLink() {
        help.click();
        return this;
    }

    /**
     * Click on Sign In Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickSignInLink() {
        signIn.click();
        return this;
    }

    /**
     * Click on Sign In To Studio Button.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickSignInToStudioButton() {
        ClickHelper.advancedClick((RemoteWebDriver) driver, signInToStudio);
        //signInToStudio.click();
        return this;
    }

    /**
     * Click on Sign Up Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickSignUpLink() {
        signUp.click();
        return this;
    }

    /**
     * Click on Skip To Main Content Link.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn clickSkipToMainContentLink() {
        skipToMainContent.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn fill() {
        setEmailEmailField();
        setPassword1CheckboxField();
        setPassword2CheckboxField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Email Email field.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn setEmailEmailField() {
        return setEmailEmailField(data.get("EMAIL"));
    }

    /**
     * Set value to Email Email field.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn setEmailEmailField(String emailValue) {
        email.sendKeys(emailValue);
        return this;
    }

    /**
     * Set default value to Password Password field.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn setPassword1CheckboxField() {
        return setPassword1CheckboxField(data.get("PASSWORD"));
    }

    /**
     * Set Password Checkbox field.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn setPassword1CheckboxField(String passwordValue) {
        password1.sendKeys(passwordValue);
        return this;
    }

    /**
     * Set Password Checkbox field.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn setPassword2CheckboxField() {
        if (!password2.isSelected()) {
            password2.click();
        }
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn submit() {
        clickSignInToStudioButton();
        return this;
    }

    /**
     * Unset Password Checkbox field.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn unsetPassword2CheckboxField() {
        if (password2.isSelected()) {
            password2.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the test.EdXSignIn class instance.
     */
    public EdXSignIn verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
