package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignUp2Lite {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#navigation-site-about a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el1;

    @FindBy(css = "#navigation-site-initiative a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el2;

    @FindBy(css = "#navigation-site-info a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el3;

    @FindBy(css = "#navigation-account a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el4;

    @FindBy(css = "a[href='#site-initiative']")
    @CacheLookup
    private WebElement aboutYourInitiative2;

    @FindBy(css = "button.js-kirigami-edit__save.kirigami-edit__save.modal-action.modal-close.btn.waves-effect.waves-light.blue")
    @CacheLookup
    private WebElement accept;

    @FindBy(css = "a[href='#account']")
    @CacheLookup
    private WebElement accountInfo4;

    @FindBy(id = "feature3")
    @CacheLookup
    private WebElement badges;

    @FindBy(css = "button.modal-action.modal-close.btn-flat.waves-effect.waves-light.red-text")
    @CacheLookup
    private WebElement cancel;

    @FindBy(id = "feature12")
    @CacheLookup
    private WebElement customDevelopments;

    @FindBy(id = "feature4")
    @CacheLookup
    private WebElement customReports;

    @FindBy(id = "faeture9")
    @CacheLookup
    private WebElement ecommerce;

    @FindBy(id = "feature10")
    @CacheLookup
    private WebElement edxInsights;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.openedx div:nth-of-type(6) div.input-field.input-field--large.col.s12.validator div.select-wrapper input.select-dropdown[type='text']")
    @CacheLookup
    private WebElement howAreYouPlanningToProduce1;

    @FindBy(id = "content_generation")
    @CacheLookup
    private WebElement howAreYouPlanningToProduce2;

    @FindBy(id = "estimated_monthly_users")
    @CacheLookup
    private WebElement howManyMonthlyActiveUsersAre;

    @FindBy(id = "feature1")
    @CacheLookup
    private WebElement lmsAndStudio;

    @FindBy(css = "#navigation-site-info a")
    @CacheLookup
    private WebElement micrositeSetup3;

    @FindBy(id = "feature5")
    @CacheLookup
    private WebElement multilingualConfigurations;

    @FindBy(id = "feature8")
    @CacheLookup
    private WebElement multisites;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.openedx div:nth-of-type(7) div.col.s12.right-align a")
    @CacheLookup
    private WebElement next1;

    @FindBy(id = "finish-site-questions")
    @CacheLookup
    private WebElement next2;

    @FindBy(id = "feature11")
    @CacheLookup
    private WebElement openEdxMobileApps;

    private final String pageLoadedText = "What is the timeframe for your online learning site and courses to be live";

    private final String pageUrl = "/embed/register/#site-initiative";

    @FindBy(css = "a[href='#site-about']")
    @CacheLookup
    private WebElement start1;

    @FindBy(id = "feature13")
    @CacheLookup
    private WebElement supportConsulting;

    @FindBy(id = "feature6")
    @CacheLookup
    private WebElement thirdPartyAuthentication;

    @FindBy(id = "feature2")
    @CacheLookup
    private WebElement webCertificates;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.openedx div:nth-of-type(2) div.input-field.input-field--large.col.s12.validator div.select-wrapper input.select-dropdown[type='text']")
    @CacheLookup
    private WebElement whatIsThePurposeOfYour1;

    @FindBy(id = "initiative_goal")
    @CacheLookup
    private WebElement whatIsThePurposeOfYour2;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.openedx div:nth-of-type(5) div.input-field.input-field--large.col.s12.validator div.select-wrapper input.select-dropdown[type='text']")
    @CacheLookup
    private WebElement whatIsTheTimeframeForYour1;

    @FindBy(id = "initiative_timeframe")
    @CacheLookup
    private WebElement whatIsTheTimeframeForYour2;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) div.row.fields-container div.openedx div:nth-of-type(1) div.input-field.input-field--large.col.s12.validator div.select-wrapper input.select-dropdown[type='text']")
    @CacheLookup
    private WebElement whatTypeOfOrganizationIsBehind1;

    @FindBy(id = "organization_type")
    @CacheLookup
    private WebElement whatTypeOfOrganizationIsBehind2;

    @FindBy(id = "feature7")
    @CacheLookup
    private WebElement xblocks;

    public EdXSignUp2Lite() {
    }

    public EdXSignUp2Lite(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignUp2Lite(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignUp2Lite(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 2 About Your Initiative Link.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickAboutYourInitiativeLink2() {
        aboutYourInitiative2.click();
        return this;
    }

    /**
     * Click on Accept Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickAcceptButton() {
        accept.click();
        return this;
    }

    /**
     * Click on 4 Account Info Link.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickAccountInfoLink4() {
        accountInfo4.click();
        return this;
    }

    /**
     * Click on 1 Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickButton1() {
        el1.click();
        return this;
    }

    /**
     * Click on 2 Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickButton2() {
        el2.click();
        return this;
    }

    /**
     * Click on 3 Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickButton3() {
        el3.click();
        return this;
    }

    /**
     * Click on 4 Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickButton4() {
        el4.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on 3 Microsite Setup Link.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickMicrositeSetupLink3() {
        micrositeSetup3.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickNext1Button() {
        next1.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickNext2Button() {
        next2.click();
        return this;
    }

    /**
     * Click on 1 Start Link.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickStartLink1() {
        start1.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite fill() {
        setWhatTypeOfOrganizationIsBehind1DropDownListField();
        setWhatTypeOfOrganizationIsBehind2DropDownListField();
        setWhatIsThePurposeOfYour1DropDownListField();
        setWhatIsThePurposeOfYour2DropDownListField();
        setHowManyMonthlyActiveUsersAreTextField();
        setLmsAndStudioCheckboxField();
        setWebCertificatesCheckboxField();
        setBadgesCheckboxField();
        setCustomDevelopmentsCheckboxField();
        setCustomReportsCheckboxField();
        setMultilingualConfigurationsCheckboxField();
        setThirdPartyAuthenticationCheckboxField();
        setXblocksCheckboxField();
        setMultisitesCheckboxField();
        setEcommerceCheckboxField();
        setEdxInsightsCheckboxField();
        setOpenEdxMobileAppsCheckboxField();
        setSupportConsultingCheckboxField();
        setWhatIsTheTimeframeForYour1DropDownListField();
        setWhatIsTheTimeframeForYour2DropDownListField();
        setHowAreYouPlanningToProduce1DropDownListField();
        setHowAreYouPlanningToProduce2DropDownListField();
        return this;
    }

    /*
    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EdXSignUp2Lite class instance.

    public EdXSignUp2Lite fillAndSubmit() {
        fill();
        return submit();
    } */

    /**
     * Set Badges Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setBadgesCheckboxField() {
        if (!badges.isSelected()) {
            badges.click();
        }
        return this;
    }

    /**
     * Set Custom Developments Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setCustomDevelopmentsCheckboxField() {
        if (!customDevelopments.isSelected()) {
            customDevelopments.click();
        }
        return this;
    }

    /**
     * Set Custom Reports Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setCustomReportsCheckboxField() {
        if (!customReports.isSelected()) {
            customReports.click();
        }
        return this;
    }

    /**
     * Set Ecommerce Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setEcommerceCheckboxField() {
        if (!ecommerce.isSelected()) {
            ecommerce.click();
        }
        return this;
    }

    /**
     * Set Edx Insights Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setEdxInsightsCheckboxField() {
        if (!edxInsights.isSelected()) {
            edxInsights.click();
        }
        return this;
    }

    /**
     * Set default value to How Are You Planning To Produce The Contents For Your Courses Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setHowAreYouPlanningToProduce1DropDownListField() {
        return setHowAreYouPlanningToProduce1DropDownListField(data.get("HOW_ARE_YOU_PLANNING_TO_PRODUCE_1"));
    }

    /**
     * Set value to How Are You Planning To Produce The Contents For Your Courses Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setHowAreYouPlanningToProduce1DropDownListField(String howAreYouPlanningToProduce1Value) {
        howAreYouPlanningToProduce1.sendKeys(howAreYouPlanningToProduce1Value);
        return this;
    }

    /**
     * Set default value to How Are You Planning To Produce The Contents For Your Courses Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setHowAreYouPlanningToProduce2DropDownListField() {
        return setHowAreYouPlanningToProduce2DropDownListField(data.get("HOW_ARE_YOU_PLANNING_TO_PRODUCE_2"));
    }

    /**
     * Set value to How Are You Planning To Produce The Contents For Your Courses Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setHowAreYouPlanningToProduce2DropDownListField(String howAreYouPlanningToProduce2Value) {
        new Select(howAreYouPlanningToProduce2).selectByVisibleText(howAreYouPlanningToProduce2Value);
        return this;
    }

    public EdXSignUp2Lite clickHowAreYouPlanningToProduce2DropDownListField() {
        howAreYouPlanningToProduce1.click();
        return this;
    }

    /**
     * Set default value to How Many Monthly Active Users Are You Estimating To Have Text field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setHowManyMonthlyActiveUsersAreTextField() {
        return setHowManyMonthlyActiveUsersAreTextField(data.get("HOW_MANY_MONTHLY_ACTIVE_USERS_ARE"));
    }

    /**
     * Set value to How Many Monthly Active Users Are You Estimating To Have Text field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setHowManyMonthlyActiveUsersAreTextField(String howManyMonthlyActiveUsersAreValue) {
        howManyMonthlyActiveUsersAre.sendKeys(howManyMonthlyActiveUsersAreValue);
        return this;
    }

    /**
     * Set Lms And Studio Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setLmsAndStudioCheckboxField() {
        if (!lmsAndStudio.isSelected()) {
            lmsAndStudio.click();
        }
        return this;
    }

    /**
     * Set Multilingual Configurations Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setMultilingualConfigurationsCheckboxField() {
        if (!multilingualConfigurations.isSelected()) {
            multilingualConfigurations.click();
        }
        return this;
    }

    /**
     * Set Multisites Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setMultisitesCheckboxField() {
        if (!multisites.isSelected()) {
            multisites.click();
        }
        return this;
    }

    /**
     * Set Open Edx Mobile Apps Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setOpenEdxMobileAppsCheckboxField() {
        if (!openEdxMobileApps.isSelected()) {
            openEdxMobileApps.click();
        }
        return this;
    }

    /**
     * Set Support Consulting Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setSupportConsultingCheckboxField() {
        if (!supportConsulting.isSelected()) {
            supportConsulting.click();
        }
        return this;
    }

    /**
     * Set Third Party Authentication Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setThirdPartyAuthenticationCheckboxField() {
        if (!thirdPartyAuthentication.isSelected()) {
            thirdPartyAuthentication.click();
        }
        return this;
    }

    /**
     * Set Web Certificates Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWebCertificatesCheckboxField() {
        if (!webCertificates.isSelected()) {
            webCertificates.click();
        }
        return this;
    }

    /**
     * Set default value to What Is The Purpose Of Your Initiative In Terms On Online Learning Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsThePurposeOfYour1DropDownListField() {
        return setWhatIsThePurposeOfYour1DropDownListField(data.get("WHAT_IS_THE_PURPOSE_OF_YOUR_1"));
    }

    /**
     * Set value to What Is The Purpose Of Your Initiative In Terms On Online Learning Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsThePurposeOfYour1DropDownListField(String whatIsThePurposeOfYour1Value) {
        whatIsThePurposeOfYour1.sendKeys(whatIsThePurposeOfYour1Value);
        return this;
    }

    public EdXSignUp2Lite clickWhatIsThePurposeOfYour1DropDownListField() {
        whatIsThePurposeOfYour1.click();
        return this;
    }

    /**
     * Set default value to What Is The Purpose Of Your Initiative In Terms On Online Learning Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsThePurposeOfYour2DropDownListField() {
        return setWhatIsThePurposeOfYour2DropDownListField(data.get("WHAT_IS_THE_PURPOSE_OF_YOUR_2"));
    }

    /**
     * Set value to What Is The Purpose Of Your Initiative In Terms On Online Learning Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsThePurposeOfYour2DropDownListField(String whatIsThePurposeOfYour2Value) {
        new Select(whatIsThePurposeOfYour2).selectByVisibleText(whatIsThePurposeOfYour2Value);
        return this;
    }

    /**
     * Set default value to What Is The Timeframe For Your Online Learning Site And Courses To Be Live Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsTheTimeframeForYour1DropDownListField() {
        return setWhatIsTheTimeframeForYour1DropDownListField(data.get("WHAT_IS_THE_TIMEFRAME_FOR_YOUR_1"));
    }

    public EdXSignUp2Lite clickWhatIsTheTimeframeForYour1DropDownListField() {
        whatIsTheTimeframeForYour1.click();
        return this;
    }

    /**
     * Set value to What Is The Timeframe For Your Online Learning Site And Courses To Be Live Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsTheTimeframeForYour1DropDownListField(String whatIsTheTimeframeForYour1Value) {
        whatIsTheTimeframeForYour1.sendKeys(whatIsTheTimeframeForYour1Value);
        return this;
    }

    /**
     * Set default value to What Is The Timeframe For Your Online Learning Site And Courses To Be Live Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsTheTimeframeForYour2DropDownListField() {
        return setWhatIsTheTimeframeForYour2DropDownListField(data.get("WHAT_IS_THE_TIMEFRAME_FOR_YOUR_2"));
    }

    /**
     * Set value to What Is The Timeframe For Your Online Learning Site And Courses To Be Live Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatIsTheTimeframeForYour2DropDownListField(String whatIsTheTimeframeForYour2Value) {
        new Select(whatIsTheTimeframeForYour2).selectByVisibleText(whatIsTheTimeframeForYour2Value);
        return this;
    }

    /**
     * Set default value to What Type Of Organization Is Behind Your Online Learning Initiative Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatTypeOfOrganizationIsBehind1DropDownListField() {
        return setWhatTypeOfOrganizationIsBehind1DropDownListField(data.get("WHAT_TYPE_OF_ORGANIZATION_IS_BEHIND_1"));
    }

    /**
     * Set value to What Type Of Organization Is Behind Your Online Learning Initiative Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatTypeOfOrganizationIsBehind1DropDownListField(String whatTypeOfOrganizationIsBehind1Value) {
        whatTypeOfOrganizationIsBehind1.sendKeys(whatTypeOfOrganizationIsBehind1Value);
        return this;
    }

    /**
     * Set value to What Type Of Organization Is Behind Your Online Learning Initiative Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite clickWhatTypeOfOrganizationIsBehind1DropDownListField() {
        whatTypeOfOrganizationIsBehind1.click();
        return this;
    }

    /**
     * Set default value to What Type Of Organization Is Behind Your Online Learning Initiative Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatTypeOfOrganizationIsBehind2DropDownListField() {
        return setWhatTypeOfOrganizationIsBehind2DropDownListField(data.get("WHAT_TYPE_OF_ORGANIZATION_IS_BEHIND_2"));
    }

    /**
     * Set value to What Type Of Organization Is Behind Your Online Learning Initiative Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setWhatTypeOfOrganizationIsBehind2DropDownListField(String whatTypeOfOrganizationIsBehind2Value) {
        new Select(whatTypeOfOrganizationIsBehind2).selectByVisibleText(whatTypeOfOrganizationIsBehind2Value);
        return this;
    }

    /**
     * Set Xblocks Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite setXblocksCheckboxField() {
        if (!xblocks.isSelected()) {
            xblocks.click();
        }
        return this;
    }

    /*

    /**
     * Submit the form to target page.
     *
     * @return the EdXSignUp2Lite class instance.

    public EdXSignUp2Lite submit() {
        click1Button();
        return this;
    } */

    /**
     * Unset Badges Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetBadgesCheckboxField() {
        if (badges.isSelected()) {
            badges.click();
        }
        return this;
    }

    /**
     * Unset Custom Developments Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetCustomDevelopmentsCheckboxField() {
        if (customDevelopments.isSelected()) {
            customDevelopments.click();
        }
        return this;
    }

    /**
     * Unset Custom Reports Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetCustomReportsCheckboxField() {
        if (customReports.isSelected()) {
            customReports.click();
        }
        return this;
    }

    /**
     * Unset Ecommerce Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetEcommerceCheckboxField() {
        if (ecommerce.isSelected()) {
            ecommerce.click();
        }
        return this;
    }

    /**
     * Unset Edx Insights Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetEdxInsightsCheckboxField() {
        if (edxInsights.isSelected()) {
            edxInsights.click();
        }
        return this;
    }

    /**
     * Unset default value from How Are You Planning To Produce The Contents For Your Courses Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetHowAreYouPlanningToProduce2DropDownListField() {
        return unsetHowAreYouPlanningToProduce2DropDownListField(data.get("HOW_ARE_YOU_PLANNING_TO_PRODUCE_2"));
    }

    /**
     * Unset value from How Are You Planning To Produce The Contents For Your Courses Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetHowAreYouPlanningToProduce2DropDownListField(String howAreYouPlanningToProduce2Value) {
        new Select(howAreYouPlanningToProduce2).deselectByVisibleText(howAreYouPlanningToProduce2Value);
        return this;
    }

    /**
     * Unset Lms And Studio Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetLmsAndStudioCheckboxField() {
        if (lmsAndStudio.isSelected()) {
            lmsAndStudio.click();
        }
        return this;
    }

    /**
     * Unset Multilingual Configurations Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetMultilingualConfigurationsCheckboxField() {
        if (multilingualConfigurations.isSelected()) {
            multilingualConfigurations.click();
        }
        return this;
    }

    /**
     * Unset Multisites Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetMultisitesCheckboxField() {
        if (multisites.isSelected()) {
            multisites.click();
        }
        return this;
    }

    /**
     * Unset Open Edx Mobile Apps Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetOpenEdxMobileAppsCheckboxField() {
        if (openEdxMobileApps.isSelected()) {
            openEdxMobileApps.click();
        }
        return this;
    }

    /**
     * Unset Support Consulting Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetSupportConsultingCheckboxField() {
        if (supportConsulting.isSelected()) {
            supportConsulting.click();
        }
        return this;
    }

    /**
     * Unset Third Party Authentication Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetThirdPartyAuthenticationCheckboxField() {
        if (thirdPartyAuthentication.isSelected()) {
            thirdPartyAuthentication.click();
        }
        return this;
    }

    /**
     * Unset Web Certificates Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetWebCertificatesCheckboxField() {
        if (webCertificates.isSelected()) {
            webCertificates.click();
        }
        return this;
    }

    /**
     * Unset default value from What Is The Purpose Of Your Initiative In Terms On Online Learning Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetWhatIsThePurposeOfYour2DropDownListField() {
        return unsetWhatIsThePurposeOfYour2DropDownListField(data.get("WHAT_IS_THE_PURPOSE_OF_YOUR_2"));
    }

    /**
     * Unset value from What Is The Purpose Of Your Initiative In Terms On Online Learning Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetWhatIsThePurposeOfYour2DropDownListField(String whatIsThePurposeOfYour2Value) {
        new Select(whatIsThePurposeOfYour2).deselectByVisibleText(whatIsThePurposeOfYour2Value);
        return this;
    }

    /**
     * Unset default value from What Is The Timeframe For Your Online Learning Site And Courses To Be Live Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetWhatIsTheTimeframeForYour2DropDownListField() {
        return unsetWhatIsTheTimeframeForYour2DropDownListField(data.get("WHAT_IS_THE_TIMEFRAME_FOR_YOUR_2"));
    }

    /**
     * Unset value from What Is The Timeframe For Your Online Learning Site And Courses To Be Live Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetWhatIsTheTimeframeForYour2DropDownListField(String whatIsTheTimeframeForYour2Value) {
        new Select(whatIsTheTimeframeForYour2).deselectByVisibleText(whatIsTheTimeframeForYour2Value);
        return this;
    }

    /**
     * Unset default value from What Type Of Organization Is Behind Your Online Learning Initiative Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetWhatTypeOfOrganizationIsBehind2DropDownListField() {
        return unsetWhatTypeOfOrganizationIsBehind2DropDownListField(data.get("WHAT_TYPE_OF_ORGANIZATION_IS_BEHIND_2"));
    }

    /**
     * Unset value from What Type Of Organization Is Behind Your Online Learning Initiative Drop Down List field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetWhatTypeOfOrganizationIsBehind2DropDownListField(String whatTypeOfOrganizationIsBehind2Value) {
        new Select(whatTypeOfOrganizationIsBehind2).deselectByVisibleText(whatTypeOfOrganizationIsBehind2Value);
        return this;
    }

    /**
     * Unset Xblocks Checkbox field.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite unsetXblocksCheckboxField() {
        if (xblocks.isSelected()) {
            xblocks.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSignUp2Lite class instance.
     */
    public EdXSignUp2Lite verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
