package cz.cvut.openedx.pages;

import java.util.Map;

import cz.cvut.openedx.helpers.ClickHelper;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignUp3 {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#navigation-site-about a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el1;

    @FindBy(css = "#navigation-site-initiative a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el2;

    @FindBy(css = "#navigation-site-info a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el3;

    @FindBy(css = "#navigation-account a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el4;

    @FindBy(css = "a[href='#site-initiative']")
    @CacheLookup
    private WebElement aboutYourInitiative2;

    @FindBy(css = "button.js-kirigami-edit__save.kirigami-edit__save.modal-action.modal-close.btn.waves-effect.waves-light.blue")
    @CacheLookup
    private WebElement accept;

    @FindBy(css = "#navigation-account a")
    @CacheLookup
    private WebElement accountInfo4;

    @FindBy(css = "button.modal-action.modal-close.btn-flat.waves-effect.waves-light.red-text")
    @CacheLookup
    private WebElement cancel;

    @FindBy(css = "a.modal-trigger.btn-flat.waves-effect.white-text.js-edit-button.hide")
    @CacheLookup
    private WebElement edit;

    @FindBy(id = "platform_name")
    @CacheLookup
    private WebElement initiativeName;

    @FindBy(css = "input.select-dropdown")
    @CacheLookup
    private WebElement language1;

    @FindBy(name = "language")
    @CacheLookup
    private WebElement language2;

    @FindBy(css = "a[href='#site-info']")
    @CacheLookup
    private WebElement micrositeSetup3;

    @FindBy(css = "#card-holder div.register-content.row div:nth-of-type(2) form div:nth-of-type(6) div.col.s12.right-align a")
    @CacheLookup
    private WebElement next1;

    @FindBy(id = "finish-site-info")
    @CacheLookup
    private WebElement next2;

    private final String pageLoadedText = "Simply adding your logo and colors can get your site's look and feel to be much better from the start";

    private final String pageUrl = "/embed/register/#site-info";

    @FindBy(id = "color")
    @CacheLookup
    private WebElement pickAColor;

    @FindBy(css = "a[href='#site-about']")
    @CacheLookup
    private WebElement start1;

    @FindBy(id = "imgfile--kirigami-edit__modal--registration")
    @CacheLookup
    private WebElement uploadYourLogo;

    @FindBy(id = "organization_name")
    @CacheLookup
    private WebElement urlForYourOpenEdxSite;

    public EdXSignUp3() {
    }

    public EdXSignUp3(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignUp3(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignUp3(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 2 About Your Initiative Link.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickAboutYourInitiativeLink2() {
        aboutYourInitiative2.click();
        return this;
    }

    /**
     * Click on Accept Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickAcceptButton() {
        accept.click();
        return this;
    }

    /**
     * Click on 4 Account Info Link.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickAccountInfoLink4() {
        accountInfo4.click();
        return this;
    }

    /**
     * Click on 1 Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickButton1() {
        el1.click();
        return this;
    }

    /**
     * Click on 2 Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickButton2() {
        el2.click();
        return this;
    }

    /**
     * Click on 3 Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickButton3() {
        el3.click();
        return this;
    }

    /**
     * Click on 4 Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickButton4() {
        el4.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickEditLink() {
        edit.click();
        return this;
    }

    /**
     * Click on 3 Microsite Setup Link.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickMicrositeSetupLink3() {
        micrositeSetup3.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickNext1Button() {
        next1.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickNext2Button() { //todo change back?
        ClickHelper.advancedClick((RemoteWebDriver) driver, next2);
        //next2.click();
        return this;
    }

    /**
     * Click on 1 Start Link.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 clickStartLink1() {
        start1.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 fill() {
        setInitiativeNameTextField();
        setUrlForYourOpenEdxSiteTextField();
        setLanguage1DropDownListField();
        setLanguage2DropDownListField();
        setPickAColorDropDownListField();
        return this;
    }

    /**
     * Set default value to Initiative Name Text field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setInitiativeNameTextField() {
        return setInitiativeNameTextField(data.get("INITIATIVE_NAME"));
    }

    /**
     * Set value to Initiative Name Text field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setInitiativeNameTextField(String initiativeNameValue) {
        initiativeName.sendKeys(initiativeNameValue);
        return this;
    }

    /**
     * Set default value to Language Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setLanguage1DropDownListField() {
        return setLanguage1DropDownListField(data.get("LANGUAGE_1"));
    }

    /**
     * Set value to Language Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setLanguage1DropDownListField(String language1Value) {
        language1.sendKeys(language1Value);
        return this;
    }

    public EdXSignUp3 clickLanguage1DropDownListField() {
        // WebDriverWait wait = new WebDriverWait(driver, 10);
        // wait.until(ExpectedConditions.elementToBeClickable(language1));
        ClickHelper.advancedClick((RemoteWebDriver) driver, language1);
        // language1.click();
        return this;
    }

    /**
     * Set default value to Language Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setLanguage2DropDownListField() {
        return setLanguage2DropDownListField(data.get("LANGUAGE_2"));
    }

    /**
     * Set value to Language Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setLanguage2DropDownListField(String language2Value) {
        new Select(language2).selectByVisibleText(language2Value);
        return this;
    }

    /**
     * Set default value to Pick A Color Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setPickAColorDropDownListField() {
        return setPickAColorDropDownListField(data.get("PICK_A_COLOR"));
    }

    /**
     * Set value to Pick A Color Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setPickAColorDropDownListField(String pickAColorValue) {
        new Select(pickAColor).selectByVisibleText(pickAColorValue);
        return this;
    }

    /**
     * Set Upload Your Logo File field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setUploadYourLogoFileField() {
        return this;
    }

    /**
     * Set default value to Url For Your Open Edx Site Text field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setUrlForYourOpenEdxSiteTextField() {
        return setUrlForYourOpenEdxSiteTextField(data.get("URL_FOR_YOUR_OPEN_EDX_SITE"));
    }

    /**
     * Set value to Url For Your Open Edx Site Text field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 setUrlForYourOpenEdxSiteTextField(String urlForYourOpenEdxSiteValue) {
        urlForYourOpenEdxSite.sendKeys(urlForYourOpenEdxSiteValue);
        return this;
    }

    /**
     * Unset default value from Language Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 unsetLanguage2DropDownListField() {
        return unsetLanguage2DropDownListField(data.get("LANGUAGE_2"));
    }

    /**
     * Unset value from Language Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 unsetLanguage2DropDownListField(String language2Value) {
        new Select(language2).deselectByVisibleText(language2Value);
        return this;
    }

    /**
     * Unset default value from Pick A Color Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 unsetPickAColorDropDownListField() {
        return unsetPickAColorDropDownListField(data.get("PICK_A_COLOR"));
    }

    /**
     * Unset value from Pick A Color Drop Down List field.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 unsetPickAColorDropDownListField(String pickAColorValue) {
        new Select(pickAColor).deselectByVisibleText(pickAColorValue);
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSignUp3 class instance.
     */
    public EdXSignUp3 verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
