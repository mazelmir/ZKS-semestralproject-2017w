package cz.cvut.openedx.pages;

import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXHP {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#metaslider_780 ol.flex-control-nav.flex-control-paging li:nth-of-type(1) a")
    @CacheLookup
    private WebElement we1;

    @FindBy(css = "#metaslider_780 ol.flex-control-nav.flex-control-paging li:nth-of-type(2) a")
    @CacheLookup
    private WebElement we2;

    @FindBy(css = "a.flex-active")
    @CacheLookup
    private WebElement we3;

    @FindBy(css = "#metaslider_780 ol.flex-control-nav.flex-control-paging li:nth-of-type(4) a")
    @CacheLookup
    private WebElement we4;

    @FindBy(css = "#metaslider_780 ol.flex-control-nav.flex-control-paging li:nth-of-type(5) a")
    @CacheLookup
    private WebElement we5;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(3) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(1) a.submenu-component__link")
    @CacheLookup
    private WebElement aboutOpenEdx1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(7)")
    @CacheLookup
    private WebElement aboutOpenEdx2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(4) a.mdl-navigation__link")
    @CacheLookup
    private WebElement aboutUs1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(13)")
    @CacheLookup
    private WebElement aboutUs2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content footer.footer-component.mdl-mega-footer div:nth-of-type(1) div:nth-of-type(2) ul.footer-component__nav li:nth-of-type(4) a")
    @CacheLookup
    private WebElement aboutUs3;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(4) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement addEcommerceToYourOpen;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(2) a.mdl-navigation__link")
    @CacheLookup
    private WebElement consultingServices1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(5)")
    @CacheLookup
    private WebElement consultingServices2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content footer.footer-component.mdl-mega-footer div:nth-of-type(1) div:nth-of-type(2) ul.footer-component__nav li:nth-of-type(2) a")
    @CacheLookup
    private WebElement consultingServices3;

    @FindBy(css = "a[href='mailto:contact@edunext.co']")
    @CacheLookup
    private WebElement contactedunextCo;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(3) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(6) a.submenu-component__link")
    @CacheLookup
    private WebElement customerSupport1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(12)")
    @CacheLookup
    private WebElement customerSupport2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(3) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(5) a.submenu-component__link")
    @CacheLookup
    private WebElement demoSite1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(11)")
    @CacheLookup
    private WebElement demoSite2;

    @FindBy(css = "a[href='https://www.edunext.co/']")
    @CacheLookup
    private WebElement edunext;

    @FindBy(id = "email")
    @CacheLookup
    private WebElement email;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(3) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement enhancedAndOptimizedToCover;

    @FindBy(css = "#features-slider ul.features__list-images li:nth-of-type(3) a")
    @CacheLookup
    private WebElement enhancedAndOptimizedToCoverYour;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(5) a.mdl-navigation__link")
    @CacheLookup
    private WebElement es1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(14)")
    @CacheLookup
    private WebElement es2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content footer.footer-component.mdl-mega-footer div:nth-of-type(1) div:nth-of-type(2) ul.footer-component__nav li:nth-of-type(5) a")
    @CacheLookup
    private WebElement es3;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(3) div:nth-of-type(2) strong:nth-of-type(2) a")
    @CacheLookup
    private WebElement exclusiveFeatures;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(1) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(3) a.submenu-component__link")
    @CacheLookup
    private WebElement exclusiveFeatures1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(4)")
    @CacheLookup
    private WebElement exclusiveFeatures2;

    @FindBy(css = "a[href='https://www.edunext.co/articles/category/features/']")
    @CacheLookup
    private WebElement features;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(1) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(2) a.submenu-component__link")
    @CacheLookup
    private WebElement featuresPricing1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(3)")
    @CacheLookup
    private WebElement featuresPricing2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(3) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(3) a.submenu-component__link")
    @CacheLookup
    private WebElement freebies1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(9)")
    @CacheLookup
    private WebElement freebies2;

    @FindBy(css = "a.js-link-logged.mdl-button.mdl-js-button.mdl-button--primary.mdl-button--raised.mdl-js-ripple-effect.hidden")
    @CacheLookup
    private WebElement goToYourDashboard;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(5) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement hostingSolutionsForOpenEdx;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(3) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(2) a.submenu-component__link")
    @CacheLookup
    private WebElement knowledgeBase1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(8)")
    @CacheLookup
    private WebElement knowledgeBase2;

    @FindBy(css = "a[href='https://manage.edunext.co/login/']")
    @CacheLookup
    private WebElement logIn1;

    @FindBy(css = "button.mdl-button.mdl-js-button.mdl-button--raised.mdl-js-ripple-effect.mdl-button--accent")
    @CacheLookup
    private WebElement logIn2;

    @FindBy(id = "sample5")
    @CacheLookup
    private WebElement message1;

    @FindBy(name = "_gotcha")
    @CacheLookup
    private WebElement message2;

    @FindBy(id = "name")
    @CacheLookup
    private WebElement name;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(1) a.mdl-navigation__link")
    @CacheLookup
    private WebElement openEdxAsAService1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(1)")
    @CacheLookup
    private WebElement openEdxAsAService2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(1) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement openEdxAsAService3;

    @FindBy(css = "#features-slider ul.features__list-images li:nth-of-type(1) a")
    @CacheLookup
    private WebElement openEdxAsAService4;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content footer.footer-component.mdl-mega-footer div:nth-of-type(1) div:nth-of-type(2) ul.footer-component__nav li:nth-of-type(1) a")
    @CacheLookup
    private WebElement openEdxAsAService5;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement openEdxConference2017;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(3) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement openEdxFicusSecurityAnd;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(1) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(1) a.submenu-component__link")
    @CacheLookup
    private WebElement openEdxInTheCloud1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(2)")
    @CacheLookup
    private WebElement openEdxInTheCloud2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(1) section.headline-component__content.mdl-cell.mdl-cell--12-col a:nth-of-type(1)")
    @CacheLookup
    private WebElement openEdxReadyToGo;

    private final String pageLoadedText = "eduNEXT's Open edX Software as a Service subscriptions provide a way to integrate ecommerce into your online learning strategy, featuring payment processors such as Paypal and Cybersource, paid courses, course upgrades, coupon codes, and more";

    private final String pageUrl = "/#";

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content footer.footer-component.mdl-mega-footer div:nth-of-type(2) div:nth-of-type(1) a:nth-of-type(2)")
    @CacheLookup
    private WebElement pbx5713583867;

    @FindBy(id = "loggedTooltip")
    @CacheLookup
    private WebElement person1;

    @FindBy(css = "button.navbar-component__login.mdl-button.mdl-js-button.mdl-button--icon.mdl-shadow--8dp")
    @CacheLookup
    private WebElement person2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(2) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement readyToGo1;

    @FindBy(css = "#features-slider ul.features__list-images li:nth-of-type(2) a")
    @CacheLookup
    private WebElement readyToGo2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(3) a.mdl-navigation__link")
    @CacheLookup
    private WebElement resources1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(6)")
    @CacheLookup
    private WebElement resources2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content footer.footer-component.mdl-mega-footer div:nth-of-type(1) div:nth-of-type(2) ul.footer-component__nav li:nth-of-type(3) a")
    @CacheLookup
    private WebElement resources3;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded header.navbar-component.mdl-layout__header.is-casting-shadow div:nth-of-type(2) nav.mdl-navigation div:nth-of-type(2) div:nth-of-type(3) ul.mdl-list.mdl-shadow--2dp li:nth-of-type(4) a.submenu-component__link")
    @CacheLookup
    private WebElement sandbox1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded div:nth-of-type(2) nav.mdl-navigation a:nth-of-type(10)")
    @CacheLookup
    private WebElement sandbox2;

    @FindBy(id = "search-field")
    @CacheLookup
    private WebElement search;

    @FindBy(css = "a.js-link-login.launch_modal--register.mdl-button.mdl-js-button.mdl-button--primary.mdl-button--raised.mdl-js-ripple-effect")
    @CacheLookup
    private WebElement startTrialNow;

    @FindBy(css = "button.mdl-button.mdl-js-button.mdl-button--primary.mdl-button--raised.mdl-js-ripple-effect")
    @CacheLookup
    private WebElement submit;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(1) section.headline-component__content.mdl-cell.mdl-cell--12-col a:nth-of-type(2)")
    @CacheLookup
    private WebElement theOnlineLearningPlatformTrusted;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(1) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore1;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(2) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore2;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(3) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore3;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(4) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore4;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore5;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(3) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore6;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(4) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore7;

    @FindBy(css = "#home-articles div:nth-of-type(1) div:nth-of-type(5) div:nth-of-type(3) a.mdl-button.mdl-js-button.mdl-button--primary")
    @CacheLookup
    private WebElement viewMore8;

    @FindBy(css = "#home-articles div:nth-of-type(2) a.mdl-button.mdl-js-button.mdl-button--primary.mdl-button--raised.mdl-js-ripple-effect")
    @CacheLookup
    private WebElement viewMoreArticles;

    @FindBy(css = ".oaas-mdl.home.page.page-id-6.page-template.page-template-templateslanding-page-php.contact-form div:nth-of-type(2) div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header.has-drawer.is-small-screen.is-upgraded main.mdl-layout__content div:nth-of-type(2) div.features__wrapper.show div.features__content ul.features__list-text li:nth-of-type(4) div:nth-of-type(1) a")
    @CacheLookup
    private WebElement withNoStringsAttached1;

    @FindBy(css = "#features-slider ul.features__list-images li:nth-of-type(4) a")
    @CacheLookup
    private WebElement withNoStringsAttached2;

    public EdXHP() {
    }

    public EdXHP(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXHP(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXHP(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on About Open Edx Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickAboutOpenEdx1Link() {
        aboutOpenEdx1.click();
        return this;
    }

    /**
     * Click on About Open Edx Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickAboutOpenEdx2Link() {
        aboutOpenEdx2.click();
        return this;
    }

    /**
     * Click on About Us Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickAboutUs1Link() {
        aboutUs1.click();
        return this;
    }

    /**
     * Click on About Us Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickAboutUs2Link() {
        aboutUs2.click();
        return this;
    }

    /**
     * Click on About Us Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickAboutUs3Link() {
        aboutUs3.click();
        return this;
    }

    /**
     * Click on Add Ecommerce To Your Open Edx Site Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickAddEcommerceToYourOpenLink() {
        addEcommerceToYourOpen.click();
        return this;
    }

    /**
     * Click on Consulting Services Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickConsultingServices1Link() {
        consultingServices1.click();
        return this;
    }

    /**
     * Click on Consulting Services Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickConsultingServices2Link() {
        consultingServices2.click();
        return this;
    }

    /**
     * Click on Consulting Services Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickConsultingServices3Link() {
        consultingServices3.click();
        return this;
    }

    /**
     * Click on Contactedunext.co Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickContactedunextCoLink() {
        contactedunextCo.click();
        return this;
    }

    /**
     * Click on Customer Support Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickCustomerSupport1Link() {
        customerSupport1.click();
        return this;
    }

    /**
     * Click on Customer Support Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickCustomerSupport2Link() {
        customerSupport2.click();
        return this;
    }

    /**
     * Click on Demo Site Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickDemoSite1Link() {
        demoSite1.click();
        return this;
    }

    /**
     * Click on Demo Site Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickDemoSite2Link() {
        demoSite2.click();
        return this;
    }

    /**
     * Click on Edunext Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickEdunextLink() {
        edunext.click();
        return this;
    }

    /**
     * Click on Enhanced And Optimized To Cover Your Needs Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickEnhancedAndOptimizedToCoverLink() {
        enhancedAndOptimizedToCover.click();
        return this;
    }

    /**
     * Click on Enhanced And Optimized To Cover Your Needs Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickEnhancedAndOptimizedToCoverYourLink() {
        enhancedAndOptimizedToCoverYour.click();
        return this;
    }

    /**
     * Click on Es Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickEs1Link() {
        es1.click();
        return this;
    }

    /**
     * Click on Es Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickEs2Link() {
        es2.click();
        return this;
    }

    /**
     * Click on Es Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickEs3Link() {
        es3.click();
        return this;
    }

    /**
     * Click on Exclusive Features Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickExclusiveFeatures1Link() {
        exclusiveFeatures1.click();
        return this;
    }

    /**
     * Click on Exclusive Features Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickExclusiveFeatures2Link() {
        exclusiveFeatures2.click();
        return this;
    }

    /**
     * Click on Exclusive Features Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickExclusiveFeaturesLink() {
        exclusiveFeatures.click();
        return this;
    }

    /**
     * Click on Features Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickFeaturesLink() {
        features.click();
        return this;
    }

    /**
     * Click on Features Pricing Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickFeaturesPricing1Link() {
        featuresPricing1.click();
        return this;
    }

    /**
     * Click on Features Pricing Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickFeaturesPricing2Link() {
        featuresPricing2.click();
        return this;
    }

    /**
     * Click on Freebies Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickFreebies1Link() {
        freebies1.click();
        return this;
    }

    /**
     * Click on Freebies Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickFreebies2Link() {
        freebies2.click();
        return this;
    }

    /**
     * Click on Go To Your Dashboard Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickGoToYourDashboardLink() {
        goToYourDashboard.click();
        return this;
    }

    /**
     * Click on Hosting Solutions For Open Edx Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickHostingSolutionsForOpenEdxLink() {
        hostingSolutionsForOpenEdx.click();
        return this;
    }

    /**
     * Click on Knowledge Base Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickKnowledgeBase1Link() {
        knowledgeBase1.click();
        return this;
    }

    /**
     * Click on Knowledge Base Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickKnowledgeBase2Link() {
        knowledgeBase2.click();
        return this;
    }

    /**
     * Click on 1 Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickLink1() {
        we1.click();
        return this;
    }

    /**
     * Click on 2 Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickLink2() {
        we2.click();
        return this;
    }

    /**
     * Click on 3 Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickLink3() {
        we3.click();
        return this;
    }

    /**
     * Click on 4 Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickLink4() {
        we4.click();
        return this;
    }

    /**
     * Click on 5 Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickLink5() {
        we5.click();
        return this;
    }

    /**
     * Click on Log In Button.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickLogIn1Button() {
        logIn1.click();
        return this;
    }

    /**
     * Click on Log In Button.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickLogIn2Button() {
        logIn2.click();
        return this;
    }

    /**
     * Click on Open Edx As A Service Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxAsAService1Link() {
        openEdxAsAService1.click();
        return this;
    }

    /**
     * Click on Open Edx As A Service Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxAsAService2Link() {
        openEdxAsAService2.click();
        return this;
    }

    /**
     * Click on Open Edx As A Service Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxAsAService3Link() {
        openEdxAsAService3.click();
        return this;
    }

    /**
     * Click on Open Edx As A Service Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxAsAService4Link() {
        openEdxAsAService4.click();
        return this;
    }

    /**
     * Click on Open Edx As A Service Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxAsAService5Link() {
        openEdxAsAService5.click();
        return this;
    }

    /**
     * Click on Open Edx Conference 2017 Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxConference2017Link() {
        openEdxConference2017.click();
        return this;
    }

    /**
     * Click on Open Edx Ficus. Security And Performance Upgrades In This Latest Named Release Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxFicusSecurityAndLink() {
        openEdxFicusSecurityAnd.click();
        return this;
    }

    /**
     * Click on Open Edx In The Cloud Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxInTheCloud1Link() {
        openEdxInTheCloud1.click();
        return this;
    }

    /**
     * Click on Open Edx In The Cloud Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxInTheCloud2Link() {
        openEdxInTheCloud2.click();
        return this;
    }

    /**
     * Click on Open Edx Ready To Go Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickOpenEdxReadyToGoLink() {
        openEdxReadyToGo.click();
        return this;
    }

    /**
     * Click on Pbx 571 3583867 Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickPbx5713583867Link() {
        pbx5713583867.click();
        return this;
    }

    /**
     * Click on Person Button.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickPerson1Button() {
        person1.click();
        return this;
    }

    /**
     * Click on Person Button.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickPerson2Button() {
        person2.click();
        return this;
    }

    /**
     * Click on Ready To Go Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickReadyToGo1Link() {
        readyToGo1.click();
        return this;
    }

    /**
     * Click on Ready To Go Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickReadyToGo2Link() {
        readyToGo2.click();
        return this;
    }

    /**
     * Click on Resources Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickResources1Link() {
        resources1.click();
        return this;
    }

    /**
     * Click on Resources Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickResources2Link() {
        resources2.click();
        return this;
    }

    /**
     * Click on Resources Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickResources3Link() {
        resources3.click();
        return this;
    }

    /**
     * Click on Sandbox Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickSandbox1Link() {
        sandbox1.click();
        return this;
    }

    /**
     * Click on Sandbox Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickSandbox2Link() {
        sandbox2.click();
        return this;
    }

    /**
     * Click on Start Trial Now Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickStartTrialNowLink() {
        startTrialNow.click();
        return this;
    }

    /**
     * Click on Submit Button.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickSubmitButton() {
        submit.click();
        return this;
    }

    /**
     * Click on The Online Learning Platform Trusted By Harvard Mit Edx.org And Others At Your Fingertips. Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickTheOnlineLearningPlatformTrustedLink() {
        theOnlineLearningPlatformTrusted.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore1Link() {
        viewMore1.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore2Link() {
        viewMore2.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore3Link() {
        viewMore3.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore4Link() {
        viewMore4.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore5Link() {
        viewMore5.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore6Link() {
        viewMore6.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore7Link() {
        viewMore7.click();
        return this;
    }

    /**
     * Click on View More Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMore8Link() {
        viewMore8.click();
        return this;
    }

    /**
     * Click on View More Articles Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickViewMoreArticlesLink() {
        viewMoreArticles.click();
        return this;
    }

    /**
     * Click on With No Strings Attached Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickWithNoStringsAttached1Link() {
        withNoStringsAttached1.click();
        return this;
    }

    /**
     * Click on With No Strings Attached Link.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP clickWithNoStringsAttached2Link() {
        withNoStringsAttached2.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP fill() {
        setSearchTextField();
        setNameTextField();
        setEmailTextField();
        setMessage1TextField();
        setMessage2TextField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Email Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setEmailTextField() {
        return setEmailTextField(data.get("EMAIL"));
    }

    /**
     * Set value to Email Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setEmailTextField(String emailValue) {
        email.sendKeys(emailValue);
        return this;
    }

    /**
     * Set default value to Message Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setMessage1TextField() {
        return setMessage1TextField(data.get("MESSAGE_1"));
    }

    /**
     * Set value to Message Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setMessage1TextField(String message1Value) {
        message1.sendKeys(message1Value);
        return this;
    }

    /**
     * Set default value to Message Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setMessage2TextField() {
        return setMessage2TextField(data.get("MESSAGE_2"));
    }

    /**
     * Set value to Message Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setMessage2TextField(String message2Value) {
        message2.sendKeys(message2Value);
        return this;
    }

    /**
     * Set default value to Name Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setNameTextField() {
        return setNameTextField(data.get("NAME"));
    }

    /**
     * Set value to Name Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setNameTextField(String nameValue) {
        name.sendKeys(nameValue);
        return this;
    }

    /**
     * Set default value to Search Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setSearchTextField() {
        return setSearchTextField(data.get("SEARCH"));
    }

    /**
     * Set value to Search Text field.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP setSearchTextField(String searchValue) {
        search.sendKeys(searchValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP submit() {
        clickLogIn1Button(); //changed manually, added a 1
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXHP class instance.
     */
    public EdXHP verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
