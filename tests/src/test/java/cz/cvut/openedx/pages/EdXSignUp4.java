package cz.cvut.openedx.pages;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EdXSignUp4 {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "#navigation-site-about a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el1;

    @FindBy(css = "#navigation-site-initiative a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el2;

    @FindBy(css = "#navigation-site-info a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el3;

    @FindBy(css = "#navigation-account a button.register-navigation__option-button.btn-floating")
    @CacheLookup
    private WebElement el4;

    @FindBy(css = "a[href='#site-initiative']")
    @CacheLookup
    private WebElement aboutYourInitiative2;

    @FindBy(css = "button.js-kirigami-edit__save.kirigami-edit__save.modal-action.modal-close.btn.waves-effect.waves-light.blue")
    @CacheLookup
    private WebElement accept;

    @FindBy(css = "a[href='#account']")
    @CacheLookup
    private WebElement accountInfo4;

    @FindBy(css = "button.modal-action.modal-close.btn-flat.waves-effect.waves-light.red-text")
    @CacheLookup
    private WebElement cancel;

    @FindBy(id = "name")
    @CacheLookup
    private WebElement contactName;

    @FindBy(id = "email")
    @CacheLookup
    private WebElement email;

    @FindBy(css = "input.select-dropdown")
    @CacheLookup
    private WebElement howDidYouFindOutAbout1;

    @FindBy(id = "referrer")
    @CacheLookup
    private WebElement howDidYouFindOutAbout2;

    @FindBy(id = "terms")
    @CacheLookup
    private WebElement iAcceptTheServiceTermsAnd;

    @FindBy(id = "want_to_be_contacted")
    @CacheLookup
    private WebElement idLikeToBeContactedBy;

    @FindBy(css = "a[href='#']")
    @CacheLookup
    private WebElement launchMyOpenEdxSite1;

    @FindBy(id = "signUpButton")
    @CacheLookup
    private WebElement launchMyOpenEdxSite2;

    @FindBy(css = "a[href='#site-info']")
    @CacheLookup
    private WebElement micrositeSetup3;

    private final String pageLoadedText = "We'll use them to create an Open edX user with course creation privileges in STUDIO and also to create a user to manage your account in the eduNEXT management console";

    private final String pageUrl = "/embed/register/#account";

    @FindBy(id = "password")
    @CacheLookup
    private WebElement password;

    @FindBy(css = "a[href='#site-about']")
    @CacheLookup
    private WebElement start1;

    @FindBy(css = "a[href='https://www.edunext.co/tos']")
    @CacheLookup
    private WebElement termsAndConditions;

    @FindBy(id = "username")
    @CacheLookup
    private WebElement username;

    public EdXSignUp4() {
    }

    public EdXSignUp4(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EdXSignUp4(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EdXSignUp4(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 2 About Your Initiative Link.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickAboutYourInitiativeLink2() {
        aboutYourInitiative2.click();
        return this;
    }

    /**
     * Click on Accept Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickAcceptButton() {
        accept.click();
        return this;
    }

    /**
     * Click on 4 Account Info Link.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickAccountInfoLink4() {
        accountInfo4.click();
        return this;
    }

    /**
     * Click on 1 Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickButton1() {
        el1.click();
        return this;
    }

    /**
     * Click on 2 Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickButton2() {
        el2.click();
        return this;
    }

    /**
     * Click on 3 Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickButton3() {
        el3.click();
        return this;
    }

    /**
     * Click on 4 Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickButton4() {
        el4.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Launch My Open Edx Site Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickLaunchMyOpenEdxSite1Button() {
        launchMyOpenEdxSite1.click();
        return this;
    }

    /**
     * Click on Launch My Open Edx Site Button.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickLaunchMyOpenEdxSite2Button() {
        launchMyOpenEdxSite2.click();
        return this;
    }

    /**
     * Click on 3 Microsite Setup Link.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickMicrositeSetupLink3() {
        micrositeSetup3.click();
        return this;
    }

    /**
     * Click on 1 Start Link.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickStartLink1() {
        start1.click();
        return this;
    }

    /**
     * Click on Terms And Conditions Link.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 clickTermsAndConditionsLink() {
        termsAndConditions.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 fill() {
        setContactNameTextField();
        setEmailEmailField();
        setUsernameTextField();
        setPasswordPasswordField();
        setHowDidYouFindOutAbout1DropDownListField();
        setHowDidYouFindOutAbout2DropDownListField();
        setIAcceptTheServiceTermsAndCheckboxField();
        setIdLikeToBeContactedByCheckboxField();
        return this;
    }

    /**
     * Set default value to Contact Name Text field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setContactNameTextField() {
        return setContactNameTextField(data.get("CONTACT_NAME"));
    }

    /**
     * Set value to Contact Name Text field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setContactNameTextField(String contactNameValue) {
        contactName.sendKeys(contactNameValue);
        return this;
    }

    /**
     * Set default value to Email Email field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setEmailEmailField() {
        return setEmailEmailField(data.get("EMAIL"));
    }

    /**
     * Set value to Email Email field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setEmailEmailField(String emailValue) {
        email.sendKeys(emailValue);
        return this;
    }

    /**
     * Set default value to How Did You Find Out About Edunext Drop Down List field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setHowDidYouFindOutAbout1DropDownListField() {
        return setHowDidYouFindOutAbout1DropDownListField(data.get("HOW_DID_YOU_FIND_OUT_ABOUT_1"));
    }

    /**
     * Set value to How Did You Find Out About Edunext Drop Down List field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setHowDidYouFindOutAbout1DropDownListField(String howDidYouFindOutAbout1Value) {
        howDidYouFindOutAbout1.sendKeys(howDidYouFindOutAbout1Value);
        return this;
    }

    /**
     * Set default value to How Did You Find Out About Edunext Drop Down List field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setHowDidYouFindOutAbout2DropDownListField() {
        return setHowDidYouFindOutAbout2DropDownListField(data.get("HOW_DID_YOU_FIND_OUT_ABOUT_2"));
    }

    /**
     * Set value to How Did You Find Out About Edunext Drop Down List field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setHowDidYouFindOutAbout2DropDownListField(String howDidYouFindOutAbout2Value) {
        new Select(howDidYouFindOutAbout2).selectByVisibleText(howDidYouFindOutAbout2Value);
        return this;
    }

    /**
     * Set I Accept The Service Terms And Conditions. Checkbox field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setIAcceptTheServiceTermsAndCheckboxField() {
        if (!iAcceptTheServiceTermsAnd.isSelected()) {
            iAcceptTheServiceTermsAnd.click();
        }
        return this;
    }

    /**
     * Set Id Like To Be Contacted By A Sales Representative To Assist Me Along The Way Checkbox field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setIdLikeToBeContactedByCheckboxField() {
        if (!idLikeToBeContactedBy.isSelected()) {
            idLikeToBeContactedBy.click();
        }
        return this;
    }

    /**
     * Set default value to Password Password field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setPasswordPasswordField() {
        return setPasswordPasswordField(data.get("PASSWORD"));
    }

    /**
     * Set value to Password Password field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setPasswordPasswordField(String passwordValue) {
        password.sendKeys(passwordValue);
        return this;
    }

    /**
     * Set default value to Username Text field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setUsernameTextField() {
        return setUsernameTextField(data.get("USERNAME"));
    }

    /**
     * Set value to Username Text field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 setUsernameTextField(String usernameValue) {
        username.sendKeys(usernameValue);
        return this;
    }

    /**
     * Unset default value from How Did You Find Out About Edunext Drop Down List field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 unsetHowDidYouFindOutAbout2DropDownListField() {
        return unsetHowDidYouFindOutAbout2DropDownListField(data.get("HOW_DID_YOU_FIND_OUT_ABOUT_2"));
    }

    /**
     * Unset value from How Did You Find Out About Edunext Drop Down List field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 unsetHowDidYouFindOutAbout2DropDownListField(String howDidYouFindOutAbout2Value) {
        new Select(howDidYouFindOutAbout2).deselectByVisibleText(howDidYouFindOutAbout2Value);
        return this;
    }

    /**
     * Unset I Accept The Service Terms And Conditions. Checkbox field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 unsetIAcceptTheServiceTermsAndCheckboxField() {
        if (iAcceptTheServiceTermsAnd.isSelected()) {
            iAcceptTheServiceTermsAnd.click();
        }
        return this;
    }

    /**
     * Unset Id Like To Be Contacted By A Sales Representative To Assist Me Along The Way Checkbox field.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 unsetIdLikeToBeContactedByCheckboxField() {
        if (idLikeToBeContactedBy.isSelected()) {
            idLikeToBeContactedBy.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EdXSignUp4 class instance.
     */
    public EdXSignUp4 verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
