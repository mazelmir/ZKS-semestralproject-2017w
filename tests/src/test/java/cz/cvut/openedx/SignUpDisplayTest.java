package cz.cvut.openedx;

import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.pages.EdXHP;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SignUpDisplayTest {
    static RemoteWebDriver driver;
    static EdXHP edXHP = new EdXHP(driver);

    static String BASE_URL = "https://www.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);

        PageFactory.initElements(driver, edXHP);

        Dimension dimension = new Dimension(1440, 900);
        driver.manage().window().setSize(dimension);
    }

    @Test
    public void step1_visitHPTest() {
        driver.get(BASE_URL);

        // edXHP.verifyPageLoaded();

        assertTrue("Not throwing an error!", driver.getTitle().contains("eduNEXT"));
    }

    @Test
    public void step2_startSignUpTest() {
        edXHP.clickStartTrialNowLink();

        driver.switchTo().frame(1);

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.presenceOfElementLocated(By.className("lite")));

        assertTrue("Not throwing an error!", driver.findElement(By.className("lite")).isDisplayed());
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
