package cz.cvut.openedx;

import cz.cvut.openedx.helpers.CSVReader;
import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.pages.EdXHP;
import cz.cvut.openedx.pages.EdXSignUp2Enterprise;
import cz.cvut.openedx.pages.EdXSignUp2Lite;
import cz.cvut.openedx.pages.EdXSignUp3;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SignUpEnterpriseSandboxTest {
    static RemoteWebDriver driver;
    static EdXHP edXHP = new EdXHP(driver);

    static String BASE_URL = "https://www.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);

        PageFactory.initElements(driver, edXHP);

        Dimension dimension = new Dimension(1440, 900);
        driver.manage().window().setSize(dimension);
    }

    @Test
    public void step0_pageLoaded() {
        driver.get("https://manage.edunext.co/embed/register/#site-about");

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.presenceOfElementLocated(By.className("enterprise")));

        assertTrue("Not throwing an error!", driver.findElement(By.className("enterprise")).isDisplayed());
    }

    private void step1CommonCompletion(WebElement button) {
        ClickHelper.advancedClick(driver, button);

        WebElement nextBtn = driver.findElement(By.id("finish-site-about"));

        nextBtn.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-initiative"), "class", "active"));
    }

    /* private void returnStep() {
        WebElement homeBtn = driver.findElement(By.id("navigation-site-about")).findElement(By.className("register-navigation__option-button"));

        homeBtn.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-about"), "class", "active"));
    } */

    @Test
    public void step1_chooseTest() {
        WebElement enterprise = driver.findElement(By.className("enterprise"));

        step1CommonCompletion(enterprise);

        assertTrue("Not throwing an error!", driver.findElement(By.id("navigation-site-initiative")).getAttribute("class").contains("active")); //todo replace with sthing more specific
    }

    @Test
    public void step2_goToSandbox() throws IOException {
        EdXSignUp2Enterprise edXSignUp2Enterprise = new EdXSignUp2Enterprise(driver);
        PageFactory.initElements(driver, edXSignUp2Enterprise);

        edXSignUp2Enterprise.verifyPageLoaded();

        edXSignUp2Enterprise.clickGoToTheSandboxSite2Button();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.titleContains("Sandbox"));

        assertTrue("Not throwing an error!", driver.getTitle().contains("Sandbox"));
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
