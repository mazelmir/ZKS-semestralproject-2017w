package cz.cvut.openedx;

import com.google.common.collect.Iterables;
import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.helpers.StudioLoginHelper;
import cz.cvut.openedx.pages.EdXCOutline;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class SubsectionCreateTest {
    static RemoteWebDriver driver;
    static EdXCOutline edXOutline1 = new EdXCOutline(driver);

    static String BASE_URL = "https://studio.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);
    }

    @Test
    public void subsection_CreateTest() {
        StudioLoginHelper.logIn(driver);
        driver.get(BASE_URL + "course/course-v1:sandbox+Demo+01");

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.elementToBeClickable(By.className("outline-section")));

        WebElement section = Iterables.getLast(driver.findElements(By.className("outline-section")));
        WebElement subsectionCreate = section.findElement(By.xpath("//a[@data-default-name=\"Subsection\"]"));

        ClickHelper.advancedClick(driver, subsectionCreate);



        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.presenceOfElementLocated(By.className("is-editing")));

        assertTrue("Not throwing an error!", driver.findElement(By.className("is-editing")).isDisplayed()); //todo replace with sthing more specific
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
