package cz.cvut.openedx;

import com.google.common.collect.Iterables;
import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.helpers.StudioLoginHelper;
import cz.cvut.openedx.pages.EdXCOutline;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class UnitDuplicateTest {
    static RemoteWebDriver driver;
    static EdXCOutline edXOutline1 = new EdXCOutline(driver);

    static String BASE_URL = "https://studio.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);
    }

    @Test
    public void unit_duplicateTest() {
        StudioLoginHelper.logIn(driver);
        driver.get(BASE_URL + "course/course-v1:sandbox+Demo+01");
        WebDriverWait wait0 = new WebDriverWait(driver, 60);
        wait0.until(ExpectedConditions.presenceOfElementLocated((By.className("outline-unit"))));

        List<WebElement> units = driver.findElements(By.className("outline-unit"));
        int prevCount = units.size();

        WebElement unit = Iterables.getLast(units);
        WebElement unitDuplicate = unit.findElement(By.xpath("//div[contains(@class, \'unit-header\')]//a[contains(@class, \'duplicate-button\')]"));

        ClickHelper.advancedClick(driver, unitDuplicate);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int newCount = driver.findElements(By.className("outline-unit")).size();

        assertTrue("Not throwing an error!", newCount > prevCount); //todo replace with sthing more specific
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}