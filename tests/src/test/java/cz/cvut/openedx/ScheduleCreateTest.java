package cz.cvut.openedx;

import cz.cvut.openedx.helpers.CSVReader;
import cz.cvut.openedx.helpers.StudioLoginHelper;
import cz.cvut.openedx.objects.ScheduleObject;
import cz.cvut.openedx.pages.EdXScheduleDetails;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScheduleCreateTest {
    static RemoteWebDriver driver;

    static String BASE_URL = "https://studio.edunext.co/";

    @Parameterized.Parameters
    public static Collection pairs() throws IOException {
        return CSVReader.readCSV("src/test/resources/pairs-valid.csv", "\t");
    }

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);

        StudioLoginHelper.logIn(driver);

        driver.get(BASE_URL + "settings/details/course-v1:sandbox+Demo+01");
        EdXScheduleDetails edXScheduleDetailsPage = new EdXScheduleDetails(driver);
        PageFactory.initElements(driver, edXScheduleDetailsPage);
        edXScheduleDetailsPage.verifyPageLoaded();
    }

    private ScheduleObject so;

    public ScheduleCreateTest(String id, String sd, String st, String ed, String et, String matches) throws ParseException {
        so = new ScheduleObject(sd,st,ed,et);
    }

    @Test
    public void testOutCorrectAllpairs() {
        EdXScheduleDetails page = new EdXScheduleDetails(driver);
        PageFactory.initElements(driver, page);

        page.verifyPageLoaded();

        page.setCourseStartDateTextField(so.courseStartDate);
        page.setCourseStartTimeTextField(so.courseStartTime);
        page.setCourseEndDateTextField(so.courseEndDate);
        page.setCourseEndTimeTextField(so.courseEndTime);

        assertTrue("no error shown", driver.findElementsByClassName("message-error").size() == 0);
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
