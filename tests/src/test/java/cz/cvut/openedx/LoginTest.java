package cz.cvut.openedx;

import cz.cvut.openedx.pages.EdXCOutline;
import cz.cvut.openedx.pages.EdXSignIn;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginTest {
    static RemoteWebDriver driver;
    static EdXCOutline edXOutline;
    static EdXSignIn edXSignIn;

    static String BASE_URL = "https://studio.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);
        edXSignIn = new EdXSignIn(driver);
    }

    @Test
    public void step1_incorrect_loginTest() {
        driver.get(BASE_URL + "signin");
        PageFactory.initElements(driver, edXSignIn);

        edXSignIn
                .verifyPageLoaded()
                .setEmailEmailField("author@example.com")
                .setPassword1CheckboxField("incorrectpassword");

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.elementToBeClickable(By.id("submit")));

        edXSignIn.clickSignInToStudioButton(); //todo this is a hack, should wait for it to be enabled

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.className("error")));

        assertTrue("Not throwing an error!", driver.findElement(By.id("login_error")).isDisplayed());
    }

    @Test
    public void step2_correct_loginTest() {
        driver.get(BASE_URL + "signin");
        PageFactory.initElements(driver, edXSignIn);

        edXSignIn
                .verifyPageLoaded()
                .setEmailEmailField("author@example.com")
                .setPassword1CheckboxField("author");

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.elementToBeClickable(By.id("submit")));

        edXSignIn.clickSignInToStudioButton(); //todo this is a hack, should wait for it to be enabled

        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("course-index-tabs")));

        assertTrue("Not throwing an error!", driver.findElement(By.id("course-index-tabs")).isDisplayed());
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}