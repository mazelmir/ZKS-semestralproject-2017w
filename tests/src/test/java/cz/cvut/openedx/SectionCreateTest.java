package cz.cvut.openedx;

import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.helpers.StudioLoginHelper;
import cz.cvut.openedx.pages.EdXCOutline;
import cz.cvut.openedx.pages.EdXOverview;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SectionCreateTest {
    static RemoteWebDriver driver;
    static EdXCOutline edXOutline1 = new EdXCOutline(driver);

    static String BASE_URL = "https://studio.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);
    }

    @Test
    public void step1_visitS1OutlineTest() {
        StudioLoginHelper.logIn(driver);

        EdXOverview edXOverview = new EdXOverview(driver);
        PageFactory.initElements(driver, edXOverview);

        edXOverview
                .verifyPageLoaded()
                .clickEdxDemonstrationCourseOrganizationSandbox2Link();

        //edXOutline1.verifyPageLoaded();

        assertTrue("Not throwing an error!", driver.getTitle().contains("Course Outline"));

        PageFactory.initElements(driver, edXOutline1); //todo is this the right place for it?
    }

    @Test
    public void step2_createSectionTest() {
        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.elementToBeClickable(By.className("button-new")));

        WebElement sectionCreate = driver.findElement(By.xpath("//a[@data-default-name=\"Section\"]"));
        sectionCreate.click();

        // new Actions(driver).moveToElement(sectionCreate).click().perform();

        // workaround pro klik níže z https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/2556

        ClickHelper.advancedClick(driver, sectionCreate);

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.presenceOfElementLocated(By.className("is-editing"))); //todo also replace with sthing better; same with clasees below

        assertTrue("Not throwing an error!", driver.findElement(By.className("is-editing")).isDisplayed()); //todo replace with sthing more specific
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
