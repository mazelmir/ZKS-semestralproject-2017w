package cz.cvut.openedx;

import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.helpers.StudioLoginHelper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UnitCreateTest {
    static RemoteWebDriver driver;

    static String BASE_URL = "https://studio.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);
    }

    @Test
    public void createUnitTest() {
        StudioLoginHelper.logIn(driver);

        driver.get(BASE_URL + "course/course-v1:sandbox+Demo+01");

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@data-default-name=\"Unit\"]")));

        WebElement unitCreate = driver.findElement(By.xpath("//a[@data-default-name=\"Unit\"]"));
        unitCreate.click();

        // new Actions(driver).moveToElement(unitCreate).click().perform();

        // workaround pro klik níže z https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/2556

        ClickHelper.advancedClick(driver, unitCreate);

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.titleContains("Unit")); //todo also replace with sthing better; same with clasees below

        WebDriverWait wait3 = new WebDriverWait(driver, 60);
        wait3.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-type=\"video\"]")));

        WebElement videoCreate = driver.findElement(By.xpath("//button[@data-type=\"video\"]"));
        videoCreate.click();

        assertTrue("Not throwing an error!", driver.getTitle().contains("Unit")); //todo replace with sthing more specific
    }

    /* @Test
    public void step3_renameElement() {
        WebElement element = driver.findElement(By.className("is-editing"));
        element.sendKeys(SECTION_TITLE + Keys.ENTER);

        assertTrue("Not throwing an error!", element.isDisplayed());
    } */


    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
