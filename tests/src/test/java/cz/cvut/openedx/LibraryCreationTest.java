package cz.cvut.openedx;

import cz.cvut.openedx.helpers.CSVReader;
import cz.cvut.openedx.helpers.StudioLoginHelper;
import cz.cvut.openedx.pages.EdXOverview;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import static org.junit.Assert.assertTrue;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LibraryCreationTest {
    static RemoteWebDriver driver;

    static String BASE_URL = "https://studio.edunext.co/";

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);
    }

    @Test
    public void step1_visitS1OutlineTest() throws IOException {
        StudioLoginHelper.logIn(driver);

        EdXOverview edXOverview = new EdXOverview(driver);
        PageFactory.initElements(driver, edXOverview);

        edXOverview
                .verifyPageLoaded()
                .clickNewLibraryLink();

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("new-library-name")));

        File signupFile = new File("src/test/resources/librarydata.csv"); //todo use getClass().getResource(), but "src/test/resources//signupdata-lite.csv" and "/signupdata-lite.csv" don't work

        String[] line = CSVReader.readFirstCSVLine(signupFile.toString(), ",");
        Random random = new Random();

        driver.findElementById("new-library-name").sendKeys(line[0]);
        driver.findElementById("new-library-org").sendKeys(line[1]);
        driver.findElementById("new-library-number").sendKeys(line[2] + random.nextInt(10000));

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.elementToBeClickable(By.className("new-library-save")));

        driver.findElementByClassName("new-library-save").click();

        WebDriverWait wait3 = new WebDriverWait(driver, 60);
        wait3.until(ExpectedConditions.titleContains("Library"));

        assertTrue("Not throwing an error!", driver.getTitle().contains("Library"));
    }


    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
