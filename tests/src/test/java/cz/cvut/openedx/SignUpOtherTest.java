package cz.cvut.openedx;

import cz.cvut.openedx.helpers.CSVReader;
import cz.cvut.openedx.helpers.ClickHelper;
import cz.cvut.openedx.objects.SiteOtherObject;
import cz.cvut.openedx.pages.EdXHP;
import cz.cvut.openedx.pages.EdXSignUp2Lite;
import cz.cvut.openedx.pages.EdXSignUp2Other;
import cz.cvut.openedx.pages.EdXSignUp3;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SignUpOtherTest {
    static RemoteWebDriver driver;
    static EdXHP edXHP = new EdXHP(driver);

    static String BASE_URL = "https://www.edunext.co/";

    @Parameterized.Parameters
    public static Collection params() throws IOException {
        return CSVReader.readCSV("src/test/resources/signupdata-other.csv", ",");
    }

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        FirefoxOptions options = new FirefoxOptions().addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);

        PageFactory.initElements(driver, edXHP);

        Dimension dimension = new Dimension(1440, 900);
        driver.manage().window().setSize(dimension);
    }

    private SiteOtherObject siteOtherObject;

    public SignUpOtherTest(String name, String url, String lang) {
        siteOtherObject = new SiteOtherObject(name, url, lang);
    }

    @Test
    public void step0_pageLoaded() {
        driver.get("https://manage.edunext.co/embed/register/#site-about");

        WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait1.until(ExpectedConditions.presenceOfElementLocated(By.className("other")));

        assertTrue("Not throwing an error!", driver.findElement(By.className("other")).isDisplayed());
    }

    private void step1CommonCompletion(WebElement button) {
        ClickHelper.advancedClick(driver, button);

        WebElement nextBtn = driver.findElement(By.id("finish-site-about"));

        nextBtn.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-initiative"), "class", "active"));
    }

    /*
    private void returnStep() {
        WebElement homeBtn = driver.findElement(By.id("navigation-site-about")).findElement(By.className("register-navigation__option-button"));

        homeBtn.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-about"), "class", "active"));
    } */

    @Test
    public void step1_chooseTest() {
        WebElement otherBtn = driver.findElement(By.className("other"));

        step1CommonCompletion(otherBtn);

        assertTrue("Not throwing an error!", driver.findElement(By.id("navigation-site-initiative")).getAttribute("class").contains("active")); //todo replace with sthing more specific
    }

    @Test
    public void step2_skipReason() throws IOException {
        EdXSignUp2Other edXSignUp2Other = new EdXSignUp2Other(driver);

        PageFactory.initElements(driver, edXSignUp2Other);
        edXSignUp2Other.verifyPageLoaded();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        edXSignUp2Other.clickNext2Button();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-site-info"), "class", "active"));

        assertTrue(driver.findElement(By.id("navigation-site-info")).getAttribute("class").contains("active"));
    }

    @Test
    public void step3_fillInfo() throws IOException {
        EdXSignUp3 edXSignUp3 = new EdXSignUp3(driver);

        PageFactory.initElements(driver, edXSignUp3);
        edXSignUp3.verifyPageLoaded();

        File signupFile = new File("src/test/resources/signupdata-other.csv"); //todo use getClass().getResource(), but "src/test/resources//signupdata-lite.csv" and "/signupdata-lite.csv" don't work

        String[] line = CSVReader.readFirstCSVLine(signupFile.toString(), ",");

        edXSignUp3.setInitiativeNameTextField(siteOtherObject.name);
        edXSignUp3.setUrlForYourOpenEdxSiteTextField(siteOtherObject.url);
        edXSignUp3.clickLanguage1DropDownListField();
        WebElement choice = driver.findElementByXPath("//span[contains(text(), \"" + siteOtherObject.lang + "\")]");
        choice.click();

        edXSignUp3.clickNext2Button();

        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.attributeContains(By.id("navigation-account"), "class", "active"));

        assertTrue(driver.findElement(By.id("navigation-account")).getAttribute("class").contains("active"));
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
