package cz.cvut.openedx.objects;

public class PreviewQuestionsObject {
    public String color,furniture,i1,i2,i3,i4,answer;

    public PreviewQuestionsObject(String color, String furniture, String i1, String i2, String i3, String i4, String answer) {
        this.color = color;
        this.furniture = furniture;
        this.i1 = i1;
        this.i2 = i2;
        this.i3 = i3;
        this.i4 = i4;
        this.answer = answer;
    }
}
