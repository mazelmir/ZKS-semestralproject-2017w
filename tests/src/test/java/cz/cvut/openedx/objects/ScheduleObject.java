package cz.cvut.openedx.objects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ScheduleObject {
    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    private String minDate = "01/01/100";
    private String minDateP = "01/02/100";
    private String minBelowRangeDate = "12/31/99";
    private String minTime = "00:00";
    private String minTimeP = "00:01";
    private String minBelowRangeTime = "-00:01";
    private String maxAboveRangeDate = "1/1/10000";
    private String maxDate = "12/31/9999";
    private String maxDateM = "12/30/9999";
    private String maxTimeM = "23:58";
    private String maxTime = "23:59";
    private String maxAboveRangeTime = "24:00";

    private Date endLimit = sdf.parse(maxDate + " " + maxTime);

    private String midDate = "2/29/2016";
    private String invalidDate = "X/YY/2016";
    private String midTime = "12:00";
    private String invalidTime = "13:II";

    public String courseStartDate, courseStartTime, courseEndDate, courseEndTime;

    public ScheduleObject(String sd, String st, String ed, String et) throws ParseException {
        switch (sd) {
            case "ValidStart":
                courseStartDate = minDate;
                break;
            case "ValidStartP":
                courseStartDate = minDateP;
                break;
            case "ValidEndM":
                courseStartDate = maxDateM;
                break;
            case "ValidEnd":
                courseStartDate = maxDate;
                break;
            case "ValidMid":
                courseStartDate = midDate;
                break;
            case "BelowRange":
                courseStartDate = minBelowRangeDate;
                break;
            case "AboveRange":
                courseStartDate = maxAboveRangeDate;
                break;
            case "Empty":
                courseStartDate = "";
                break;
            case "Invalid":
                courseStartDate = invalidDate;
                break;
        }

        switch (st) {
            case "ValidStart":
                courseStartTime = minTime;
                break;
            case "ValidStartP":
                courseStartTime = minTimeP;
                break;
            case "ValidEndM":
                courseStartTime = maxTimeM;
                break;
            case "ValidEnd":
                courseStartTime = (courseStartDate.equals(maxDate)) ? maxTimeM : maxTime;
                break;
            case "ValidMid":
                courseStartTime = midTime;
                break;
            case "BelowRange":
                courseStartTime = minBelowRangeTime;
                break;
            case "AboveRange":
                courseStartTime = maxAboveRangeTime;
                break;
            case "Empty":
                courseStartTime = "";
                break;
            case "Invalid":
                courseStartTime = invalidTime;
                break;
        }

        Calendar startCal = Calendar.getInstance();
        Date startTime = sdf.parse(courseStartDate + " " + courseStartTime);
        startCal.setTime(startTime);
        startCal.add(Calendar.MINUTE, 1);
        Date endMin = startCal.getTime();

        if (endMin.compareTo(endLimit) > 0) endMin = endLimit;

        Date endMinDP = getModifiedDate(endMin, 1);
        if (endMinDP.compareTo(endLimit) > 0) endMinDP = endLimit;

        Date endMinTP = getModifiedTime(endMin, 1);
        if (endMinTP.compareTo(endLimit) > 0) endMinTP = endLimit;

        switch (ed) {
            case "ValidStart":
                courseEndDate = dateFormat.format(endMin);
                break;
            case "ValidStartP":
                courseEndDate = dateFormat.format(endMinDP);
                break;
            case "ValidEndM":
                courseEndDate = (courseStartDate.equals(maxDate)) ? maxDate : maxDateM;
                break;
            case "ValidEnd":
                courseEndDate = maxDate;
                break;
            case "ValidMid":
                courseEndDate = dateFormat.format(getModifiedDate(dateFormat.parse(courseStartDate), 31));
                break;
            case "BelowRange":
                courseEndDate = dateFormat.format(getModifiedDate(dateFormat.parse(courseStartDate), -1));
                break;
            case "AboveRange":
                courseEndDate = maxAboveRangeDate;
                break;
            case "Empty":
                courseEndDate = "";
                break;
            case "Invalid":
                courseEndDate = invalidDate;
                break;
        }

        switch (et) {
            case "ValidStart":
                courseEndTime = timeFormat.format(endMin);
                break;
            case "ValidStartP":
                courseEndTime = timeFormat.format(endMinTP);
                break;
            case "ValidEndM":
                courseEndTime = courseStartTime.equals(maxTime) ? maxTime : maxTimeM;
                break;
            case "ValidEnd":
                courseEndTime = maxTime;
                break;
            case "ValidMid":
                courseEndTime = timeFormat.format(getModifiedTime(endMin, 2048));
                break;
            case "BelowRange":
                courseEndTime = minBelowRangeTime;
                break;
            case "AboveRange":
                courseEndTime = maxAboveRangeTime;
                break;
            case "Empty":
                courseEndTime = "";
                break;
            case "Invalid":
                courseEndTime = invalidTime;
                break;
        }
    }


    private Date getModifiedDate(Date date, int daysToAdd) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, daysToAdd);

        if (c.getTime().compareTo(endLimit) > 0)
            return endLimit;
        else
            return c.getTime();
    }

    private Date getModifiedTime(Date date, int minsToAdd) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, minsToAdd);

        if (c.getTime().compareTo(endLimit) > 0)
            return endLimit;
        else
            return c.getTime();
    }
}
