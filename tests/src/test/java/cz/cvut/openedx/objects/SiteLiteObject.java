package cz.cvut.openedx.objects;

public class SiteLiteObject {
    public String type, purpose, userCount, feature1, feature2, feature3, timeframe, strategy,name, url, lang;

    public SiteLiteObject(String type, String purpose, String userCount, String feature1, String feature2, String feature3, String timeframe, String strategy, String name, String url, String lang) {
        this.type = type;
        this.purpose = purpose;
        this.userCount = userCount;
        this.feature1 = feature1;
        this.feature2 = feature2;
        this.feature3 = feature3;
        this.timeframe = timeframe;
        this.strategy = strategy;
        this.name = name;
        this.url = url;
        this.lang = lang;
    }
}
