package cz.cvut.openedx.objects;

public class SiteOtherObject {
    public String name, url, lang;

    public SiteOtherObject(String name, String url, String lang) {
        this.name = name;
        this.url = url;
        this.lang = lang;
    }
}
